import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS, HttpBackend } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app.routes';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { appSettings } from '@app/shared/constants/app-settings';
import { HttpModule } from '@angular/http';
import { AuthenticationService, MenuService, RequestCacheService } from '@app/shared/services';
import { AuthInterceptor, ErrorInterceptor, RefreshTokenInterceptor } from '@shared/interceptors/index';
import { DataStorage } from '@app/shared/providers/data.storage';
import { NbThemeModule } from '@nebular/theme';
import localeRo from '@angular/common/locales/ro';

// fakeBackendProvider : used to create fake backend
import { fakeBackendProvider } from '@app/shared/helper';


// Docs: https://www.npmjs.com/package/@auth0/angular-jwt
import { JwtModule } from '@auth0/angular-jwt';
import { HttpErrorHandler } from './shared/services/http-error-handler.service';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NotifierModule, NotifierOptions } from 'angular-notifier';

// AoT requires an exported function for factories
export const createTranslateLoader = (handler: HttpBackend) => {
    const http = new HttpClient(handler);
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

export function tokenGetter() {
    return localStorage.getItem(appSettings.token);
}

registerLocaleData(localeRo, 'ro');

/**
 * Custom angular notifier options
 */
const customNotifierOptions: NotifierOptions = {
    position: {
        horizontal: {
            position: 'right',
            distance: 12
        },
        vertical: {
            position: 'top',
            distance: 12,
            gap: 10
        }
    },
    theme: 'material',
    behaviour: {
        autoHide: 5000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 4
    },
    animations: {
        enabled: true,
        show: {
            preset: 'slide',
            speed: 300,
            easing: 'ease'
        },
        hide: {
            preset: 'fade',
            speed: 300,
            easing: 'ease',
            offset: 50
        },
        shift: {
            speed: 300,
            easing: 'ease'
        },
        overlap: 150
    }
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HttpModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpBackend]
            }
        }),
        JwtModule.forRoot({
            config: {
                tokenGetter: tokenGetter,
                whitelistedDomains: ['localhost:3001', 'localhost:4200', 'iwmobiledev-srv/internalportalapi'],
                blacklistedRoutes: ['localhost:3001/auth/']
            }
        }),
        NbThemeModule.forRoot({ name: 'default' }), // this will enable the default theme
        AppRoutingModule,
        NgxPermissionsModule.forRoot(),
        NotifierModule.withConfig(customNotifierOptions)
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        // {
        //     provide: HTTP_INTERCEPTORS,
        //     useClass: ErrorInterceptor,
        //     multi: true
        // },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: RefreshTokenInterceptor,
            multi: true
        },
        MenuService,
        DataStorage,
        RequestCacheService,
        // provider used to create fake backend
        fakeBackendProvider,
        HttpErrorHandler
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }
