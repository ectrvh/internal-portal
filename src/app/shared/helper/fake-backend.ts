import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const testUser = { id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' };

        const testUsers = [
            {
                'id': '2cb55048-bbd3-4ac8-a14d-2e2c0da154a9',
                'firstName': 'Claudiu',
                'lastName': 'Chiorescu',
                'fullName': 'Claudiu Chiorescu',
                'userName': null,
                'password': null,
                'name': null,
                'emailAddress': null,
                'phone': null,
                'address': null,
                'email': null,
                'teamsLink': null,
                'phoneNumber': null,
            }];

        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // console.log(request.url, 'request.url');

            // authenticate
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                if (request.body.username === testUser.username && request.body.password === testUser.password) {
                    // if login details are valid return 200 OK with a fake jwt token
                    const body = {
                        id: testUser.id,
                        username: testUser.username,
                        firstName: testUser.firstName,
                        lastName: testUser.lastName,
                        access_token: 'fake-jwt-token'
                    };
                    return of(new HttpResponse({ status: 200, body }));
                } else {
                    // else return 400 bad request
                    return throwError({ error: { message: 'Username or password is incorrect' } });
                }
            }

            // get users
            if (request.url.endsWith('/test_api/account/GetUsers') && request.method === 'GET') {
                // check for fake auth token in header and return users if valid,
                // this security is implemented server side in a real application
                return of(new HttpResponse({ status: 200, body: testUsers }));

                // if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
                //     return of(new HttpResponse({ status: 200, body: [testUsers] }));
                // } else {
                //     // return 401 not authorised if token is null or invalid
                //     return throwError({ error: { message: 'Unauthorised He HE' } });
                // }
            }

            // pass through any requests not handled above
            return next.handle(request);

        }))

            // call materialize and dematerialize to ensure delay even if an error is thrown
            // (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
