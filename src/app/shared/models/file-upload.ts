export class FileUpload {
  id: string;
  name?: string;
  defaultPath?: string;
  contentType?: string;
  length?: number;  
  documentId?: string;  
  newsId?: string;
  newsletterId?: string;
  contactId? : string;
  userId?: string;
  fileBytes?: string;
  downloadName?: string;
}
