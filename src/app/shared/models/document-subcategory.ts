import { DocumentCategory } from './document-category';

export class DocumentSubCategory {
  id?: string;
  name?: string;
  description?: string;
  enabled?: boolean;
  active?: boolean;
  createdAt: string;
  updatedAt: string;
  documentCategoryId?: string;
  documentCategory: DocumentCategory;
  documents?: any;
}
