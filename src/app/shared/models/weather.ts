export class Weather {
    id: string;
    locationKey: string;
    locationName: string;
    locationCountry: string;
    locationLatitude: number;
    locationLongitude: number;
    data?: string;
}
