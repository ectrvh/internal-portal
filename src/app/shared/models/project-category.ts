import { Project } from './project';

export class ProjectCategory {
  id?: string;
  name?: string;
  description?: string;
  enabled?: boolean;
  active?: boolean;
  createdAt: string;
  updatedAt: string;
  projects?: Project[];
}
