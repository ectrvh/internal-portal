import { News, NewsCategory } from '.';

export class NewsSubCategory {
    id?: string;
    name?: string;
    description?: string;
    newsCategoryId?: string;
    newsCategory?: NewsCategory;
    news?: News[];
    enabled?: boolean;
    active?: boolean;
}
