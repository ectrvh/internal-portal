export class MenuItem {
  id?: string;
  label?: string;
  description?: string;
  routerLink?: any;
  permissions?: string[];
  items?: MenuItem[];
  cssClass?: string;
  icon?: string;
  expanded?: boolean;
  disabled?: boolean;
  visible?: boolean;
  active?: boolean;
}
