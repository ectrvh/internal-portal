export class JobType {
  id: string;
  name?: string;
  description?: string;
  enabled: boolean;
  active: boolean;
}
