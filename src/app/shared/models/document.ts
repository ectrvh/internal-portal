import { DocumentSubCategory } from './document-subcategory';
import { FileUpload } from './file-upload';

export class Document {
  id: string;
  title?: string;
  date: string;
  link: string;
  enabled: boolean;
  showInDashboard: boolean;
  active: boolean;
  createdAt: string;
  updatedAt: string;
  documentSubcategoryId?: string;
  documentSubcategory?: DocumentSubCategory;
  fileUploads?: FileUpload;
}
