import { ProjectCategory } from './project-category';
import { FileUpload } from './file-upload';

export class Project {
  id?: string;
  title?: string;
  details?: string;
  startDate: string;
  endDate?: string;
  projectCategoryId?: string;
  projectCategory?: ProjectCategory;
  enabled: boolean;
  active: boolean;
  usersLink?: any;
  fileUploads?: FileUpload[];
  projectDocuments?: FileUpload[];
  image: any;
}
