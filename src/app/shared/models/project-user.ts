import { Project, User } from '.';

export class ProjectUser {
  projectId: string;
  userId: string;
  project: Project;
  user: User;
}
