import { Function, JobType, TeamUser } from '.';
import { FileUpload } from './file-upload';

export class User {
  id: string;
  userName: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  email?: string;
  location?: string;
  mobile?: string;
  pager?: string;
  phoneNumber?: string;
  fileUploads?: FileUpload;
  photoLink?: string;
  jobTypeId?: string;
  jobType?: JobType;
  functionId?: string;
  function?: Function;
  projectsLink?: any;
  teamsLink?: TeamUser[];
  photoBytes?: string;
  description?: string;
  havePhoto?: boolean = false;
}
