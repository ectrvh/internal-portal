import { FileUpload } from "./file-upload";

export class Newsletter {
  id: string;
  title: string;
  date: string;
  fileUploads: FileUpload[];
  content?: string;
  preview?: string;
  active: boolean;
  enabled: boolean;
  url:any;
}
