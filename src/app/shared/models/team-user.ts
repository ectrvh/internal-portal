import { Team, User } from '.';

export class TeamUser {
    teamId: string;
    userId: string;
}
