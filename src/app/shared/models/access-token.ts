export class AccessToken {
  access_token: string;
  expires_in: number;
  expires_at: number;
  token_type: string;
}
