import { NewsSubCategory } from './news-subcategory';

export class News {
    id: string;
    title: string;
    body?: string;
    newsSubcategoryId: string;
    newsSubcategories: NewsSubCategory[];
    showInDashboard: boolean;
    date: string;
    active: boolean;
    enabled: boolean;
    updatedAt: string;
}
