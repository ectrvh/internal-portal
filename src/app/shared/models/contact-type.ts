export class ContactType {
  id?: string;
  name?: string;
  description?: string;
  enabled: boolean;
  active?: boolean;
}
