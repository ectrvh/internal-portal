export class Department {
    id: string;
    name: string;
    enabled: boolean;
    active: boolean;
}
