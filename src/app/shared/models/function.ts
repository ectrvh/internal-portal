export class Function {
  id: string;
  name?: string;
  description?: string;
  enabled?: boolean;
  active?: boolean;  
}
