import { TeamUser, Department } from '.';
export class Team {
    id: string;
    name: string;
    enabled: boolean;
    active: boolean;
    usersLink?: TeamUser[];
    departmentId?: string;
    department: Department;
}
