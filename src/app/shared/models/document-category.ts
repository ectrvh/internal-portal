export class DocumentCategory {
  id?: string;
  name?: string;
  description?: string;
  enabled: boolean;
  active: boolean;
  createdAt: string;
  updatedAt: string;
  departmentId: string;
}
