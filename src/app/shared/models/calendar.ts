export class Calendar {
  id?: string;
  title?: string;
  start?: string;
  end?: string;
  editable?: boolean;
  allDay?: boolean;
  description?: string;
  organizer?: string;
  location?: string;
  reminder?: string;
  duration?: string;
  isMeeting?: boolean;
  isNew?: boolean;
  isRecurring?: boolean;
  timeZone?: string;
  displayTo?: string;
  displayCc?: string;
  url?: string;
  className?: any;
  color?: string;
  borderColor?: string;
  textColor?: string;
  backgroundColor?: string;
}
