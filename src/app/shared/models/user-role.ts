export class UserRole {
  id: string;
  name: string;
  normalizedName: string;
  concurrencyStamp?: string;
}
