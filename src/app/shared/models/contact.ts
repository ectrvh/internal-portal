import { ContactType } from './contact-type';
import { User } from '../models/user';

export class Contact {
  id: string;
  title: string;
  description?: string;
  location?: string;
  position?: number;
  contactTypeId?: string;
  contactType?: ContactType;
  active: boolean;
  enabled: boolean;
  imageBytes?: string;
  user: User;
}
