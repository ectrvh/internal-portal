import { NewsSubCategory } from './news-subcategory';
import { Department } from './department';

export class NewsCategory {
    id?: string;
    name?: string;
    description?: string;
    enabled?: boolean;
    active?: boolean;
    createdAt: string;
    updatedAt: string;
    newsCategories?: NewsSubCategory[];
    departmentId?: string;
    department?: Department;

}
