import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AccessToken } from '@app/shared/models/access-token';
import { environment } from 'environments/environment';
import { AuthenticationService, RequestCacheService } from '@app/shared/services';
import { apiUrl } from '@app/shared/services/api';
import { requestMethod } from '../constants/request';
import { includes } from 'lodash';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {

        if (request.method === requestMethod.GET && request.url.startsWith(apiUrl.mock)) { // Mock Data            
            request = this.createLocalRequest(request);
        } else if (request.method === requestMethod.GET && request.url.startsWith(environment.accuweatherForecastsUrl)) {
            request = this.createAccuweatherRequest(request);
        } else {
            request = this.createApiRequest(request);

            if (includes(request.url, 'GetFileUpload')) {
                const cachedResponse = this.cache.get(request);                
                return cachedResponse ? Observable.of(cachedResponse) : this.sendRequest(request, next, this.cache);
            }

        }

        return next.handle(request);
    }

    constructor(private authService: AuthenticationService,
        private cache: RequestCacheService,
        public location: Location) { }

    sendRequest(req: HttpRequest<any>, next: HttpHandler, cache: RequestCacheService): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    cache.set(req, event);
                }
            })
        );
    }

    createApiRequest(request) {
        const token: AccessToken = this.authService.getTokenFromLocalStorage();
        const headers: any = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        if (token) {
            headers.Authorization = `${token.token_type} ${token.access_token}`;
        }

        const apiUrlRequest = includes(location.hostname, 'softinfo.ro') ? environment.apiUrlExtern : environment.apiUrl;

        return request.clone({
            setHeaders: headers,
            url: apiUrlRequest + request.url
        });
    }

    createLocalRequest(request) {
        const headers: any = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': ''
        };

        return request.clone({
            setHeaders: headers,
            url: request.url
        });
    }

    createAccuweatherRequest(request) {
        return request.clone({
            url: request.url
        });
    }

}
