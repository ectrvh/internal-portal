import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpClient } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';
import { AuthenticationService } from '@app/shared/services';
import { includes } from 'lodash';
import { map, tap, finalize } from 'rxjs/operators';

interface CallerRequest {
    subscriber: Subscriber<any>;
    failedRequest: HttpRequest<any>;
}

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

    private refreshInProgress: boolean;
    private requests: CallerRequest[] = [];

    constructor(private http: HttpClient,
        private authService: AuthenticationService) {

    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const { url } = request;
        const apiRequest = includes(url, '/assets/mocks')
            || (!includes(url, '/Account/RefreshToken'))
            || includes(url, 'api/')
            || includes(url, 'dataservice.accuweather.com/');


        if (!apiRequest) {            
            return next.handle(request);
        }

        const observable = new Observable<HttpEvent<any>>((subscriber) => {
            const originalRequestSubscription = next.handle(request)
                .subscribe((response) => {
                    subscriber.next(response);
                },
                    (err) => {
                        if (err.status === 401) {                            
                            this.handleUnauthorizedError(subscriber, request);
                        } else {
                            subscriber.error(err);
                        }
                    },
                    () => {                        
                        subscriber.complete();
                    });

            return () => {
                originalRequestSubscription.unsubscribe();
            };
        });

        return observable;

    }


    private handleUnauthorizedError(subscriber: Subscriber<any>, request: HttpRequest<any>) {

        this.requests.push({ subscriber, failedRequest: request });
        if (!this.refreshInProgress) {
            this.refreshInProgress = true;
            this.authService.refreshToken().pipe(
                map((token: any) => {
                    return this.authService.toToken(token);
                }),
                tap(token => {
                    this.authService.saveTokenInLocalStorage(token);
                }),
                finalize(() => {
                    this.refreshInProgress = false;
                })
            ).subscribe((token) => {
                const authorization = `${token.token_type} ${token.access_token}`;
                this.repeatFailedRequests(authorization);
            },
                () => {
                    this.authService.logout();
                    location.reload(true);
                });
        }
    }


    private repeatFailedRequests(authHeader) {
        this.requests.forEach((c) => {
            const requestWithNewToken = c.failedRequest.clone({
                headers: c.failedRequest.headers.set('Authorization', authHeader)
            });

            this.repeatRequest(requestWithNewToken, c.subscriber);
        });
        this.requests = [];
    }


    private repeatRequest(requestWithNewToken: HttpRequest<any>, subscriber: Subscriber<any>) {
        this.http.request(requestWithNewToken).subscribe((res) => {
            subscriber.next(res);
        },
            (err) => {
                if (err.status === 401) {
                    this.authService.logout();
                    location.reload(true);
                }
                subscriber.error(err);
            },
            () => {
                subscriber.complete();
            });
    }

}
