import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { AccessToken } from '@app/shared/models/access-token';
import { UserService, AuthenticationService } from '@app/shared/services';
import { User } from '@app/shared/models';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
                private userService: UserService,
                private authService: AuthenticationService) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {

        const user: User = this.userService.getUserFromLocalStorage();
        const token: AccessToken = this.authService.getTokenFromLocalStorage();

        if (user && token) {
            return true;
        }

        // Not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
