import { Injectable } from '@angular/core';
import { appSettings } from '@app/shared/constants/app-settings';

@Injectable()
export class DataStorage {

    public storage: any;

    public constructor() { }

    public saveDataInLocalStorage(data: any) {
        const dataStorage = JSON.stringify(data);
        localStorage.setItem(appSettings.data, dataStorage);
    }

    public removeDataFromLocalStorage() {
        localStorage.removeItem(appSettings.data);
    }

    public getDataFromLocalStorage() {
        return JSON.parse(localStorage.getItem(appSettings.data));
    }

}
