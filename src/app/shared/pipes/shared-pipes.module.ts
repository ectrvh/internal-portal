import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from '@app/theme/pipes/safehtml.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        SafeHtmlPipe
    ],
    exports: [
        SafeHtmlPipe
    ]    
})
export class SharedPipesModule { }
