
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { AccessToken } from '@models/access-token';
import { User } from '@models/user';
import { map, catchError, first } from 'rxjs/operators';
import { Http, Headers } from '@angular/http';
import { apiUrl } from '@app/shared/services/api';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'environments/environment';
import { appSettings } from '@app/shared/constants/app-settings';
import { UserService } from '@app/shared/services/user.service';
import { HttpClient } from '@angular/common/http';
import { has, includes } from 'lodash';
import { ROLE } from '../constants/roles';
import { claimTypesMicrosoft } from '../constants/claimtypes';
import { NgxRolesService, NgxPermissionsService } from 'ngx-permissions';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http,
    private httpClient: HttpClient,
    private userService: UserService,
    private roleService: NgxRolesService,
    private permissionsService: NgxPermissionsService,
    public location: Location,
    public jwtHelper: JwtHelperService) { }


  login(username: string, password: string): Observable<any> {
    // const body = `grant_type=password&username=${username}&password=${password}`;
    const body = null;
    const apiUrlRequest = includes(location.hostname, 'softinfo.ro') ? environment.apiUrlExtern : environment.apiUrl;
    
    const baseUrl: string = apiUrlRequest + apiUrl.auth;
    const url = `${baseUrl}?grant_type=password&username=${username}&password=${btoa(password)}`;

    let contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');

    return this.http.post(url, body, { headers: contentHeaders })
      .pipe(
        map(res => {
          // login successful if there's a jwt token in the response
          const res_token = res.json();
          const res_access_token = (res_token) ? res.json().access_token : null;

          if (res_token && res_access_token) {
            // store username and jwt token in local storage to keep user logged in between page refreshes            
            const token: AccessToken = this.toToken(res_token);
            this.saveTokenInLocalStorage(token);

            const decodedToken = this.jwtHelper.decodeToken(res_access_token);

            let userInfo: User = {
              id: decodedToken.jti,
              userName: decodedToken.sub,
              fullName: '',
              email: ''
            };
            
            this.userService.saveUserInLocalStorage(userInfo);

            return userInfo;
            // console.log(this.jwtHelper.decodeToken(res_access_token), 'decodeToken'); // token
            // console.log(this.jwtHelper.getTokenExpirationDate(res_access_token), 'getTokenExpirationDate'); // date
            // console.log(this.jwtHelper.isTokenExpired(res_access_token), 'isTokenExpired'); // true or false
          }
          return true;

        },
          catchError((err) => {
            return err;
          })

        ));
  }

  logout() {
    this.removeTokenFromLocalStorage();
    this.userService.removeUserFromLocalStorage();
  }

  toToken(token) {
    token.expires_at = this.calculateExpiresAt(token.expires_in);
    token.token_type = 'Bearer';
    return token;
  }

  refreshToken(): Observable<any> {
    const baseUrl: string = environment.apiUrl + '/api/Account/RefreshToken';
    const token: AccessToken = this.getTokenFromLocalStorage();

    return this.httpClient.post(`${baseUrl}?token=${token.access_token}`, {});
  }

  public get loggedIn(): boolean {
    return (localStorage.getItem(appSettings.token) !== null);
  }

  saveTokenInLocalStorage(token: AccessToken) {
    localStorage.setItem(appSettings.token, JSON.stringify(token));
  }

  public getTokenFromLocalStorage(): AccessToken {
    try {
      return JSON.parse(localStorage.getItem(appSettings.token));
    } catch (err) {
      return null;
    }
  }

  private removeTokenFromLocalStorage() {
    localStorage.removeItem(appSettings.token);
  }

  private calculateExpiresAt(expiresIn) {
    return (parseInt(expiresIn, 10) * 1000) + Date.now();
  }

  getRoleFromToken() {
    let role = ROLE.USER;

    const token: AccessToken = this.getTokenFromLocalStorage();
    if (!token) return role;

    const decodedToken = this.jwtHelper.decodeToken(token.access_token);

    if (has(decodedToken, claimTypesMicrosoft.role)) {
      const roleFromToken = decodedToken[claimTypesMicrosoft.role]

      if (roleFromToken === 'SuperAdmin') {
        role = ROLE.SUPERADMIN;
      }

    }
    return role;
  }
  
  setUserRole() {
    const role = this.getRoleFromToken();        

    if (role === ROLE.SUPERADMIN) {
      this.permissionsService.loadPermissions([role]);
      // this.roleService.addRole(role, ['canAccessAdmin', 'canAccessFront']);
    } else {      
      this.permissionsService.loadPermissions([role]);
      // this.roleService.addRole(role, ['canAccessFront']);
    }
  }

  getUserPermission() {
    const role = this.getRoleFromToken();

    if (role === ROLE.SUPERADMIN) {
      this.permissionsService.loadPermissions([role]);
    } else {
      this.permissionsService.loadPermissions([role]);
    }
  }

}
