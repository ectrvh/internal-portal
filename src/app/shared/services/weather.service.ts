import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { toHttpParams } from '@shared/util';
import { Weather } from '../models';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { apiUrl } from '@app/shared/services/api';

const apiUrl1 = '/api/Weathers';

// AccuWeather Params
// const API_KEY = 'Nfbd7KHRabPGqiAk0bGRHRkGZYA5nFtT';
const language = 'ro-ro';
// const details = true;
// const metric = true;

@Injectable({ providedIn: 'root' })

export class WeatherService {

    // City Search ( GET )
    // http://dataservice.accuweather.com/locations/v1/cities/search?apikey=Nfbd7KHRabPGqiAk0bGRHRkGZYA5nFtT&q=Bucuresti&language=ro-ro

    // 5 Days of Daily Forecasts ( GET )
    // http://dataservice.accuweather.com/forecasts/v1/daily/5day/287430?apikey=Nfbd7KHRabPGqiAk0bGRHRkGZYA5nFtT&language=ro-ro&details=true&metric=true

    // Weather Icons
    // https://developer.accuweather.com/weather-icons

    // private forecastsApiUrl = '/forecasts/v1/daily/5day/';

    constructor(private http: HttpClient) { }

    getWeatherForecast(cityId = 287430, language = 'ro-ro'): Observable<any> {
        // return this.http.get<Weather>(`assets/mocks/getWeather.json`);
        // return this.http.get(this.forecastsApiUrl + cityId + '?apikey=' + API_KEY + '&language=' + language + '&details=true&metric=true');

        return this.http.get(
            environment.accuweatherForecastsUrl + cityId + `?apikey=` + environment.accuweatherApiKey +
            `&language=` + language + `&details=` + environment.accuweatherDetails + `&metric=` + environment.accuweatherMetric)
            .pipe(
                map(response => {
                    return response;
                })
            );
    }

    // Params: locationKey, date

    getWeather(payload?: any): Observable<any> {
        return this.http.get<Weather>(`${apiUrl.weathers}/GetWeather` + `?${toHttpParams(payload)}`)
            .pipe(
                map(res => {
                    return {
                        message: 'OK',
                        response: res
                    }
                }),
                catchError((err) => {
                    // Weather Not Found
                    return this.getWeatherForecast(payload.locationKey)
                        .map(response => {
                            const data = {
                                locationKey: payload.locationKey,
                                locationName: payload.locationName,
                                locationCountry: payload.locationCountry,
                                locationLatitude: payload.locationLatitude,
                                locationLongitude: payload.locationLongitude,
                                data: response
                            };

                            return {
                                message: 'UPDATE',
                                response: data
                            };
                        });

                })
            );

    }

    save(payload: any): Observable<any> {
        return this.http.post<Weather>(`${apiUrl.weathers}/`, payload);
    }

    private extractData(res: any) {
        let body = res.json();
        return body.list || {};
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        // if (error instanceof Response) {
        //   const body = error.json() || '';
        //   const err = body.error || JSON.stringify(body);
        //   errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        // } else {
        errMsg = error.message ? error.message : error.toString();
        // }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
