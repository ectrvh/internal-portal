import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NewsCategory } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class NewsCategoriesService {

    constructor(private http: HttpClient) { }

    // Params: id, categoryId, limit, page
    getNewsCategories(payload?: any): Observable<any> {
        return this.http.get<NewsCategory[]>(`${apiUrl.newsCategories}/GetNewsCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllNewsCategories(payload?: any): Observable<any> {
        return this.http.get<NewsCategory[]>(`${apiUrl.newsCategories}/GetNewsCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id, categoryId, limit, page
    getNewsCategoriesForSelect(payload?: any): Observable<any> {
        return this.http.get<NewsCategory[]>(`${apiUrl.newsCategories}/GetNewsCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const newsCategories = _map(result, newsCategory => {
                        return {
                            id: newsCategory.id,
                            name: newsCategory.name,
                            disabled: !newsCategory.enabled
                        };
                    });                    
                    return newsCategories;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<NewsCategory>(`${apiUrl.newsCategories}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<NewsCategory>(`${apiUrl.newsCategories}/DisableNewsCategory` + `?${toHttpParams(payload)}`);
    }
}
