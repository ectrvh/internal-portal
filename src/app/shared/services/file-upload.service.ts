import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { toHttpParams } from '@shared/util';
import { FileUpload } from '../models';
import { saveAs } from 'file-saver';
import { ResponseContentType } from '@angular/http';
import { catchError, retry } from 'rxjs/operators';
import { HandleError, HttpErrorHandler } from './http-error-handler.service';
import { apiUrl } from '@app/shared/services/api';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({ providedIn: 'root' })

export class FileUploadService {

    private handleError: HandleError;

    constructor(private http: HttpClient,        
        private httpErrorHandler: HttpErrorHandler,
        private domSanitizer: DomSanitizer) {
        this.handleError = httpErrorHandler.createHandleError('CustomerService');
    }

    // Params: id
    getFile(payload?: any): Observable<any> {
        return this.http
            .get(`${apiUrl.fileUploads}/GetFileUpload` + `?${toHttpParams(payload)}`,            
                { responseType: 'blob' }
            )
            // .map(e => this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(e)))
            .pipe(
                // retry(3),
                catchError(this.handleError('getFile', null))
            );
    }

    downloadFile(payload: any, file: FileUpload) {
        // contentType: "application/pdf"       
        // contentType: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"        \
        // const contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';

        this.http.get(`${apiUrl.fileUploads}/GetFileUpload` + `?${toHttpParams(payload)}`,
            {
                responseType: "blob",
                headers: {
                    'Accept': file.contentType
                }
            })
            .subscribe(blob => {
                saveAs(blob, file.downloadName);
            });
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<FileUpload>(`${apiUrl.fileUploads}` + `?${toHttpParams(payload)}`);
    }
}
