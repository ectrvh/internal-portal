import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@app/shared/models';
import { Team } from '@app/shared/models/team';
import { Observable } from 'rxjs/Observable';
import { apiUrl } from '@app/shared/services/api';
import { appSettings } from '@app/shared/constants/app-settings';
import { toHttpParams } from '@shared/util';
import { UserRole } from '../models/user-role';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })

export class UserService {

    constructor(private http: HttpClient) { }

    // Params: userId
    getUsers(payload?: any): Observable<any> {
        return this.http.get<User[]>(`${apiUrl.account}/GetUser` + `?${toHttpParams(payload)}`);
    }

    getAllUsers(payload?: any): Observable<any> {
        return this.http.get<User[]>(`${apiUrl.account}/GetUser` + `?${toHttpParams(payload)}`);
    }

    getUserList(payload?: any): Observable<any> {
        return this.http.get<User[]>(`${apiUrl.account}/GetUserList` + `?${toHttpParams(payload)}`);
    }

    getUserRole(payload?: any): Observable<string> {
        return this.http.get<UserRole>(`${apiUrl.account}/GetUserRole` + `?${toHttpParams(payload)}`)
            .pipe(
                map((role: UserRole) => {
                    return role.normalizedName;
                }),
            );
    }

    getUserRoles(): Observable<any> {
        return this.http.get<UserRole[]>(`${apiUrl.account}/GetUserRoles`);
    }

    populateUsers(): Observable<any> {
        return this.http.get<any>(`${apiUrl.account}/PopulateUsers`);
    }

    save(payload: any): Observable<any> {
        return this.http.post<User>(`${apiUrl.account}/PostUserJobFunction` + `?${toHttpParams(payload)}`, payload);
    }

    saveUserTeams(payload: any, userId: string): Observable<any> {
        // console.log(payload, 'saveUserTeams payload');
        return this.http.post<User>(`${apiUrl.account}/PostUserTeams?userId=` + userId, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<User>(`${apiUrl.account}/DisableUser` + `?${toHttpParams(payload)}`);
    }

    getUserTeams(userId: string): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.account}/GetUserTeams?userId=` + userId);
    }

    public saveUserInLocalStorage(user: User) {
        const userData = JSON.stringify(user);
        localStorage.setItem(appSettings.user, userData);
    }

    public getUserFromLocalStorage() {
        return JSON.parse(localStorage.getItem(appSettings.user));
    }

    public removeUserFromLocalStorage() {
        localStorage.removeItem(appSettings.user);
    }
}
