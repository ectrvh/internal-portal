import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ContactType } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';

const apiUrl = '/api/ContactTypes';

@Injectable({ providedIn: 'root' })

export class ContactTypeService {

    constructor(private http: HttpClient) { }

    getContactTypes(payload?: any): Observable<any> {
        return this.http.get<ContactType[]>(`${apiUrl}/GetContactType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllContactTypes(payload?: any): Observable<any> {
        return this.http.get<ContactType[]>(`${apiUrl}/GetContactType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getAllContactTypesForSelected(payload?: any): Observable<any> {
        return this.http.get<ContactType[]>(`${apiUrl}/GetContactType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const contactTypes = _map(result, contactType => {
                        return {
                            id: contactType.id,
                            name: contactType.name,
                            disabled: !contactType.enabled
                        };
                    });
                    return contactTypes;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<ContactType>(`${apiUrl}/`, payload);
    }


    delete(payload: any): Observable<any> {
        return this.http.delete<ContactType>(`${apiUrl}/DisableContactType` + `?${toHttpParams(payload)}`);
    }

}
