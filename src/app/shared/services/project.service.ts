import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Project } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class ProjectService {

    constructor(private http: HttpClient) { }

    // Params: id, typeId, limit, page
    getProjects(payload?: any): Observable<any> {
        return this.http.get<Project[]>(`${apiUrl.projects}/GetProject` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllProjects(payload?: any): Observable<any> {
        return this.http.get<Project[]>(`${apiUrl.projects}/GetProject` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: projectId
    getProjectUsers(payload?: any): Observable<any> {
        return this.http.get(`${apiUrl.projects}/GetProjectUsers` + `?${toHttpParams(payload)}`);
    }

    save(payload: any): Observable<any> {
        return this.http.post<Project>(`${apiUrl.projects}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Project>(`${apiUrl.projects}/DisableProject` + `?${toHttpParams(payload)}`);
    }

}
