import { Injectable } from '@angular/core';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Injectable()

export class ToastService {

    constructor(private toastrService: NbToastrService) { }

    showToastSuccess(message) {
        const title = 'SUCCESS';
        const position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
        const duration = 3000;
        const hasIcon = true;
        const icon = 'nb-checkmark';

        this.toastrService.success(message, title, { position, duration, hasIcon, icon });
      }

      showToastError(message, duration = 3000) {
        const title = 'ERROR';
        const position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;        
        const hasIcon = true;
        const icon = 'nb-alert';

        this.toastrService.danger(message, title, { position, duration, hasIcon, icon });
      }

}
