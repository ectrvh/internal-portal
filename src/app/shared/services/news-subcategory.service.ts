import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NewsSubCategory } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class NewsSubcategoriesService {

    constructor(private http: HttpClient) { }

    // Params: id, categoryId, limit, page
    getNewsSubcategories(payload?: any): Observable<any> {
        return this.http.get<NewsSubCategory[]>(`${apiUrl.newsSubcategories}/GetNewsSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllNewsSubcategories(payload?: any): Observable<any> {
        return this.http.get<NewsSubCategory[]>(`${apiUrl.newsSubcategories}/GetNewsSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id, categoryId, limit, page
    getNewsSubcategoriesForSelect(payload?: any): Observable<any> {
        return this.http.get<NewsSubCategory[]>(`${apiUrl.newsSubcategories}/GetNewsSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const newsSubcategories = _map(result, newsCategorie => {
                        return {
                            id: newsCategorie.id,
                            name: newsCategorie.name,
                            disabled: !newsCategorie.enabled
                        };
                    });                    
                    return newsSubcategories;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<NewsSubCategory>(`${apiUrl.newsSubcategories}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<NewsSubCategory>(`${apiUrl.newsSubcategories}/DisableNewsSubcategory` + `?${toHttpParams(payload)}`);
    }

}
