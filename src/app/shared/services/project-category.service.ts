import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ProjectCategory } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class ProjectCategoriesService {

    constructor(private http: HttpClient) { }

    // Params: id, typeId, limit, page
    getProjectsCategories(payload?: any): Observable<any> {
        return this.http.get<ProjectCategory[]>(`${apiUrl.projectsCategories}/GetProjectCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllProjectsCategories(payload?: any): Observable<any> {
        return this.http.get<ProjectCategory[]>(`${apiUrl.projectsCategories}/GetProjectCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getProjectsCategoriesForSelect(payload?: any): Observable<any> {
        return this.http.get<ProjectCategory[]>(`${apiUrl.projectsCategories}/GetProjectCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const projectCategorys = _map(result, projectCategory => {
                        return {
                            id: projectCategory.id,
                            name: projectCategory.name,
                            disabled: !projectCategory.enabled
                        };
                    });
                    return projectCategorys;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<ProjectCategory>(`${apiUrl.projectsCategories}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<ProjectCategory>(`${apiUrl.projectsCategories}/DisableProjectCategory` + `?${toHttpParams(payload)}`);
    }

}
