import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Team } from '@app/shared/models/team';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class TeamService {

    constructor(private http: HttpClient) { }

    // Params: id
    getTeams(payload?: any): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.teams}/GetTeam` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllTeams(payload?: any): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.teams}/GetTeam` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getAllTeamsForSelect(payload?: any): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.teams}/GetTeam` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const teams = _map(result, team => {
                        return {
                            id: team.id,
                            name: team.name,
                            disabled: !team.enabled
                        };
                    });
                    return teams;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<Team>(`${apiUrl.teams}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Team>(`${apiUrl.teams}/DisableTeam` + `?${toHttpParams(payload)}`);
    }

}
