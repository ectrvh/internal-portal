import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { toHttpParams } from '@shared/util';
import { Ng2FileInputService } from 'ng2-file-input';
import { AccessToken } from '../models';
import { AuthenticationService } from './auth.service';
import { environment } from 'environments/environment';
import { Observable, Subject } from 'rxjs/Rx';
import { Location } from '@angular/common';
import { includes } from 'lodash';
@Injectable({ providedIn: 'root' })

export class FileInputService {

    constructor(private http: HttpClient,
        private authService: AuthenticationService,
        public location: Location,
        private ng2FileInputService: Ng2FileInputService) {
    }


    // ownerType : user, document, project, contact, newsletter
    uploadFile(modelId: string, fileInputId: string, ownerType: string) {

        const files = this.ng2FileInputService.getCurrentFiles(fileInputId);

        if (files.length === 0) {
            return Observable.of(null);
        }

        const formData = new FormData();
        formData.append('formFile', files[0]);
        // for (let file of files) {
        //   formData.append('formFile', file);
        // }

        const payload = {
            id: modelId,
            ownerType: ownerType
        };
        let authorization = '';

        const token: AccessToken = this.authService.getTokenFromLocalStorage();
        if (token) {
            authorization = `${token.token_type} ${token.access_token}`;
        }

        const apiUrlRequest = includes(location.hostname, 'softinfo.ro') ? environment.apiUrlExtern : environment.apiUrl;

        const uploadReq = new HttpRequest('POST', `${apiUrlRequest}/api/FileUploads` + `?${toHttpParams(payload)}`, formData, {
            reportProgress: true,
            headers: new HttpHeaders().set('Authorization', `${authorization}`)
        });






        return this.http.request(uploadReq);

    }

}
