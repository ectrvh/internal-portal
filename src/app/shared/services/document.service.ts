import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Document } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class DocumentService {

    constructor(private http: HttpClient) { }


    // Params: id, categoryId, limit, page
    getDocuments(payload?: any): Observable<any> {
        return this.http.get<Document[]>(`${apiUrl.documents}/GetDocument` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item =>
                        item.active === true &&
                        item.enabled === true &&
                        item.fileUploads && item.fileUploads.length > 0);
                    return result;
                })
            );
    }

    getAllDocuments(payload?: any): Observable<any> {
        return this.http.get<Document[]>(`${apiUrl.documents}/GetDocument` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getDocumentsForDashboard(payload?: any): Observable<any> {
        return this.http.get<Document[]>(`${apiUrl.documents}/GetDocument` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item =>
                        item.active === true &&
                        item.enabled === true &&
                        item.showInDashboard === true);
                    return result;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<Document>(`${apiUrl.documents}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Document>(`${apiUrl.documents}/DisableDocument` + `?${toHttpParams(payload)}`);
    }

}
