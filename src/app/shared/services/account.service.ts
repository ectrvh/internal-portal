import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { toHttpParams } from '@shared/util';
import { User, Team, Project } from '../models';
import { map } from 'rxjs/operators/map';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class AccountService {

    constructor(private http: HttpClient) { }

    // Params: userId
    getUsers(payload?: any): Observable<any> {
        return this.http.get<User[]>(`${apiUrl.account}/GetUser` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllUsers(payload?: any): Observable<any> {
        return this.http.get<User[]>(`${apiUrl.account}/GetUser` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: userId
    getUserTeams(payload?: any): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.account}/GetUserTeams` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllUserTeams(payload?: any): Observable<any> {
        return this.http.get<Team[]>(`${apiUrl.account}/GetUserTeams` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: userId
    getUserProjects(payload?: any): Observable<any> {
        return this.http.get<Project[]>(`${apiUrl.account}/GetUserProjects` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllUserProjects(payload?: any): Observable<any> {
        return this.http.get<Project[]>(`${apiUrl.account}/GetUserProjects` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // id, description
    saveUserDescription(payload?: any): Observable<any> {        
        return this.http.post<User>(`${apiUrl.account}/PostUserDescription` + `?${toHttpParams(payload)}`, null);        
    }

}
