import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DocumentSubCategory } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class DocumentSubCategoriesService {

    constructor(private http: HttpClient) { }

    // Params: id
    getDocumentSubCategories(payload?: any): Observable<any> {
        return this.http.get<DocumentSubCategory[]>(`${apiUrl.documentsSubcategories}/GetDocumentSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {                                        
                    return result.filter(item => item.active === true && item.enabled === true);
                })
            );
    }

    // Params: subcategoryId
    getCategoryDocuments(payload?: any): Observable<any> {
        return this.http.get<DocumentSubCategory[]>(`${apiUrl.documentsSubcategories}/GetCategoryDocuments` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {                                        
                    return result.filter(item => item.active === true && item.enabled === true);
                })
            );
    }

    
    getAllDocumentSubCategories(payload?: any): Observable<any> {
        return this.http.get<DocumentSubCategory[]>(`${apiUrl.documentsSubcategories}/GetDocumentSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id
    getDocumentSubCategoriesForSelect(payload?: any): Observable<any> {
        return this.http.get<DocumentSubCategory[]>(`${apiUrl.documentsSubcategories}/GetDocumentSubcategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const documentSubCategories = _map(result, documentSubCategorie => {
                        return {
                            id: documentSubCategorie.id,
                            name: documentSubCategorie.name,
                            disabled: !documentSubCategorie.enabled
                        };
                    });
                    return documentSubCategories;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<DocumentSubCategory>(`${apiUrl.documentsSubcategories}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<DocumentSubCategory>(`${apiUrl.documentsSubcategories}/DisableDocumentSubcategory` + `?${toHttpParams(payload)}`);
    }

}
