import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { News } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class NewsService {

    constructor(private http: HttpClient) { }


    // Params: id, categoryId, limit, page
    getNews(payload?: any): Observable<any> {
        return this.http.get<News[]>(`${apiUrl.news}/GetNews` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllNews(payload?: any): Observable<any> {
        return this.http.get<News[]>(`${apiUrl.news}/GetNews` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getNewsForDashboard(payload?: any): Observable<any> {
        return this.http.get<News[]>(`${apiUrl.news}/GetNews` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true && item.showInDashboard === true);
                    return result;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<News>(`${apiUrl.news}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<News>(`${apiUrl.news}/DisableNews` + `?${toHttpParams(payload)}`);
    }

}
