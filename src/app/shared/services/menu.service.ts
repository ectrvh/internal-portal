import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MenuItem } from '@app/shared/models';
import { NewsService } from '@app/shared/services/news.service';
import { NbMenuItem } from '@nebular/theme';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { apiUrl } from './api';

export const HEADER_MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'employees',
        link: '/employees',
        icon: ''
    },
    {
        title: 'documents',
        link: '/documents',
        icon: ''
    },
    {
        title: 'projects',
        link: '/projects',
        icon: ''
    },
    {
        title: 'news',
        link: '/news',
        icon: ''
    },
    {
        title: 'newsletters',
        link: '/newsletters',
        icon: ''
    }
];

export const USER_MENU_ITEMS = [
    {
        title: 'profile',
        data: 'profile'
    },
    {
        title: 'logOut',
        data: 'logout'
    }    
];

export const SIDEBAR_DEFAULT_MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Dashboard',
        icon: 'nb-home',
        link: '/dashboard',
        home: true,
        selected: true
    }
];

export const DASHBOARD_MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Dashboard',
        icon: 'nb-home',
        link: '/dashboard',
        selected: true,
        home: true
    },
    {
        title: 'employees',
        icon: 'nb-person',
        link: '/employees'
    },
    {
        title: 'documents',
        icon: 'nb-compose',
        link: '/documents'
    },
    {
        title: 'projects',
        icon: 'nb-layout-default',
        link: '/projects'
    },
    {
        title: 'news',
        icon: 'nb-title',
        link: '/news'
    },
    {
        title: 'newsletters',
        icon: 'nb-email',
        link: '/newsletters'
    }
];

export const ADMIN_MENU_ITEMS: NbMenuItem[] = [
    {
        title: 'Admin Dashboard',
        icon: 'nb-home',
        link: '/admin/dashboard',
        home: true
    },
    {
        title: 'employees',
        icon: 'nb-person',
        children: [
            {
                title: 'departments',
                link: '/admin/departments',
            },
            {
                title: 'teams',
                link: '/admin/teams',
            },
            {
                title: 'employees',
                link: '/admin/employees',
            }
        ],
    },
    {
        title: 'projects',
        icon: 'nb-layout-default',
        children: [
            {
                title: 'projectCategory',
                link: '/admin/projects-categories',
            },
            {
                title: 'projects',
                link: '/admin/projects',
            }
        ],
    },
    {
        title: 'news',
        icon: 'nb-title',
        children: [
            {
                title: 'newsCategory',
                link: '/admin/news-categories',
            },
            {
                title: 'newsSubcategory',
                link: '/admin/news-subcategories',
            },
            {
                title: 'news',
                link: '/admin/news',
            },
        ],
    },
    {
        title: 'documents',
        icon: 'nb-compose',
        children: [
            {
                title: 'documentCategory',
                link: '/admin/documents-categories',
            },
            {
                title: 'documentSubcategory',
                link: '/admin/documents-subcategories',
            },
            {
                title: 'documents',
                link: '/admin/documents',
            }
        ],
    },
    {
        title: 'contacts',
        icon: 'nb-person',
        link: '/admin/contacts',
    },
    {
        title: 'newsletters',
        icon: 'nb-email',
        link: '/admin/newsletter',
    }
];

@Injectable()
export class MenuService {

    private topMenuItems = [
        {
            title: 'topMenu.employees',
            link: '/employees',
            cssClass: '',
            icon: '',
            permissions: ['all'],
            items: []
        },
        {
            title: 'topMenu.documents',
            link: '/documents',
            cssClass: '',
            icon: '',
            permissions: ['all'],
            items: []
        },
        {
            title: 'topMenu.projects',
            link: '/projects',
            cssClass: '',
            icon: '',
            permissions: ['all'],
            items: []
        },
        {
            title: 'topMenu.news',
            link: '/news',
            cssClass: '',
            icon: '',
            permissions: ['all'],
            items: []
        }
    ];

    private sidebarDefaultMenuItems: MenuItem[] = [
        {
            id: 'dashboard',
            label: 'dashboard',
            routerLink: '/dashboard',
            permissions: ['all']
        },
        {
            id: 'all',
            label: 'all',
            routerLink: '',
            permissions: ['all']
        }
    ];

    private sidebarDefaultSelectedMenuItem: MenuItem = {
        id: 'all',
        label: 'all',
        routerLink: '',
        permissions: ['all']
    };

    private sidebarMenuItemsDashboard: MenuItem[] = [
        {
            label: 'sideMenu.dashboard',
            routerLink: '/dashboard',
            permissions: ['all']
        }
    ];

    private sidebarAdminMenuItems: MenuItem[] = [
        {
            id: 'dashboard',
            label: 'Dashboard',
            icon: 'nb-home',
            routerLink: '/admin/dashboard'
        },
        {
            id: 'employees',
            label: 'Employees',
            items: [
                {
                    id: 'employees.departments',
                    label: 'Departments',
                    routerLink: '/admin/departments',
                    permissions: ['all']
                },
                {
                    id: 'employees.teams',
                    label: 'Teams',
                    routerLink: '/admin/teams',
                    permissions: ['all']
                },
                {
                    id: 'employees.employees',
                    label: 'Employees',
                    routerLink: '/admin/employees',
                    permissions: ['all']
                }
            ]
        },
        {
            id: 'projects',
            label: 'Projects',
            items: [
                {
                    id: 'projects.projects',
                    label: 'Projects',
                    routerLink: '/admin/projects',
                    permissions: ['all']
                },
                {
                    id: 'projects.types',
                    label: 'Projects Types',
                    routerLink: '/admin/projects-types',
                    permissions: ['all']
                },
            ]
        },
        {
            id: 'news',
            label: 'News',
            items: [
                {
                    id: 'news.news',
                    label: 'News',
                    routerLink: '/admin/news',
                    permissions: ['all']
                },
                {
                    id: 'news.types',
                    label: 'News Types',
                    routerLink: '/admin/news-types',
                    permissions: ['all']
                },
                {
                    id: 'news.categories',
                    label: 'News Categories',
                    routerLink: '/admin/news-categories',
                    permissions: ['all']
                }
            ]
        },
        {
            id: 'documents',
            label: 'Documents',
            items: [
                {
                    id: 'documents.documents',
                    label: 'Documents',
                    routerLink: '/admin/documents',
                    permissions: ['all']
                },
                {
                    id: 'documents.types',
                    label: 'Documents Types',
                    routerLink: '/admin/documents-types',
                    permissions: ['all']
                },
                {
                    id: 'documents.categories',
                    label: 'Documents Categories',
                    routerLink: '/admin/documents-categories',
                    permissions: ['all']
                },
            ]
        },
        {
            id: 'contacts',
            label: 'Contacts',
            items: [
                {
                    id: 'contacts.contacts',
                    label: 'Contacts',
                    routerLink: '/admin/contacts',
                    permissions: ['all']
                },
                {
                    id: 'contacts.types',
                    label: 'Contacts Types',
                    routerLink: '/admin/contacts-types',
                    permissions: ['all']
                },
            ]
        },
        {
            id: 'contacts.newsletter',
            label: 'Newsletters',
            routerLink: '/admin/newsletter',
            permissions: ['all']
        },
    ];

    constructor(private http: HttpClient) {
    }

    getTopMenuItems() {
        return this.topMenuItems;
        // return Observable.of(this.allTopMenuItems);
    }

    getDashboardMenuItems() {
        return this.sidebarMenuItemsDashboard;
    }

    getRouteMenuItems() {
        return this.sidebarAdminMenuItems;
    }

    getAdminMenuItems(): Observable<any> {
        return of(ADMIN_MENU_ITEMS);
    }    

    getDefaultMenuItems() {
        return this.sidebarDefaultMenuItems;
    }

    getDefaultSelectedMenuItem() {
        return this.sidebarDefaultSelectedMenuItem;
    }

}
