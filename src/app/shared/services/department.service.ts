import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Department } from '@app/shared/models/department';
import { toHttpParams } from '../util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class DepartmentService {

    constructor(private http: HttpClient) { }

    // Params: id
    getDepartments(payload?: any): Observable<any> {
        return this.http.get<Department[]>(`${apiUrl.departments}/GetDepartment` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllDepartments(payload?: any): Observable<any> {
        return this.http.get<Department[]>(`${apiUrl.departments}/GetDepartment` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: departmentId
    getDepartmentTeams(payload?: any): Observable<any> {
        return this.http.get<Department[]>(`${apiUrl.departments}/GetDepartmentTeams` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    // Params: id
    getDepartmentsForSelect(payload?: any): Observable<any> {
        return this.http.get<Department[]>(`${apiUrl.departments}/GetDepartment` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const departments = _map(result, department => {
                        return {
                            id: department.id,
                            name: department.name,
                            disabled: !department.enabled
                        };
                    });
                    return departments;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<Department>(`${apiUrl.departments}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Department>(`${apiUrl.departments}/DisableDepartment` + `?${toHttpParams(payload)}`);
    }

}
