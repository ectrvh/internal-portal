import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Newsletter } from '@app/shared/models/newsletter';
import { map } from 'rxjs/operators';
import { sortBy } from 'lodash';
import { toHttpParams } from '../util';
import * as moment from 'moment';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class NewsletterService {

    constructor(private http: HttpClient) { }

    getAllNewsletters(payload?: any): Observable<any> {
        return this.http.get<Newsletter[]>(`${apiUrl.newsletters}/GetNewsletter` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id, limit, page
    getNewsletters(payload?: any): Observable<any> {
        return this.http.get<Newsletter[]>(`${apiUrl.newsletters}/GetNewsletter` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    // Sort by date desc                    
                    return sortBy(result, function (o) { return moment(o.date); }).reverse();
                })
            );
    }

    getLastNewsletter(payload?: any): Observable<any> {
        return this.http.get<Newsletter[]>(`${apiUrl.newsletters}/GetNewsletter` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true && item.fileUploads.length > 0);
                    // Sort by date desc
                    result = sortBy(result, function (o) { return moment(o.date); }).reverse();
                    return result[0];
                })
            );
    }

    getNewsletter(newsletterId: string): Observable<any> {
        return this.http.get<Newsletter>(`${apiUrl.newsletters}/getNewsletter?Id=` + newsletterId);
    }

    create(payload: any) {
        return this.http.post<Newsletter>(`${apiUrl.newsletters}/`, payload);
    }

    update(payload: any) {
        return this.http.post<Newsletter>(`${apiUrl.newsletters}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Newsletter>(`${apiUrl.newsletters}/DisableNewsletter` + `?${toHttpParams(payload)}`);
    }

    // This will delete the record from DB
    deleteNewsletter(id: string): Observable<any> {
        return this.http.delete<Newsletter>(`${apiUrl.newsletters}/DeleteNewsletter?Id=` + id);
    }

}
