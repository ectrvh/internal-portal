import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DocumentCategory } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class DocumentCategoriesService {

    constructor(private http: HttpClient) { }

    // Params: id
    getDocumentsCategories(payload?: any): Observable<any> {
        return this.http.get<DocumentCategory[]>(`${apiUrl.documentsCategories}/GetDocumentCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllDocumentsCategories(payload?: any): Observable<any> {
        return this.http.get<DocumentCategory[]>(`${apiUrl.documentsCategories}/GetDocumentCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id
    getDocumentsCategoriesForSelect(payload?: any): Observable<any> {
        return this.http.get<DocumentCategory[]>(`${apiUrl.documentsCategories}/GetDocumentCategory` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const documentCategorys = _map(result, documentCategory => {
                        return {
                            id: documentCategory.id,
                            name: documentCategory.name,
                            disabled: !documentCategory.enabled
                        };
                    });
                    return documentCategorys;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<DocumentCategory>(`${apiUrl.documentsCategories}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<DocumentCategory>(`${apiUrl.documentsCategories}/DisableDocumentCategory` + `?${toHttpParams(payload)}`);
    }

}
