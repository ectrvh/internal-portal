import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { toHttpParams } from '@shared/util';
import { JobType } from '../models';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class JobTypeService {

    constructor(private http: HttpClient) { }

    // Params: id
    getJobTypes(payload?: any): Observable<any> {
        return this.http.get<JobType[]>(`${apiUrl.jobTypes}/GetJobType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllJobTypes(payload?: any): Observable<any> {
        return this.http.get<JobType[]>(`${apiUrl.jobTypes}/GetJobType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    // Params: id
    getJobTypesForSelect(payload?: any): Observable<any> {
        return this.http.get<JobType[]>(`${apiUrl.jobTypes}/GetJobType` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const jobTypes = _map(result, jobtype => {
                        return {
                            id: jobtype.id,
                            name: jobtype.name,
                            disabled: !jobtype.enabled
                        };
                    });
                    return jobTypes;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<JobType>(`${apiUrl.jobTypes}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<JobType>(`${apiUrl.jobTypes}/DisableJobType` + `?${toHttpParams(payload)}`);
    }

}
