import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { toHttpParams } from '@shared/util';
import { Function } from '../models';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class FunctionService {

    constructor(private http: HttpClient) { }

    // Params: id
    getFunctions(payload?: any): Observable<any> {
        return this.http.get<Function[]>(`${apiUrl.functions}/GetFunction` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllFunctions(payload?: any): Observable<any> {
        return this.http.get<Function[]>(`${apiUrl.functions}/GetFunction` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    getFunctionsForSelect(payload?: any): Observable<any> {
        return this.http.get<Function[]>(`${apiUrl.functions}/GetFunction` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    const functions = _map(result, functionSelect => {
                        return {
                            id: functionSelect.id,
                            name: functionSelect.name,
                            disabled: !functionSelect.enabled
                        };
                    });
                    return functions;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<Function>(`${apiUrl.functions}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Function>(`${apiUrl.functions}/DisableFunction` + `?${toHttpParams(payload)}`);
    }

}
