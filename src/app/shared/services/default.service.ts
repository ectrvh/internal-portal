import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ModelCount } from '@app/shared/models';
import { map } from 'rxjs/operators/map';
import { map as _map } from 'lodash';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class DefaultService {

    constructor(private http: HttpClient) { }

    getModelCount(): Observable<any> {
        return this.http.get<ModelCount>(`${apiUrl.default}/GetModelCount`)
            .pipe(
                map((result: any) => {
                    if (result.citiesNameCount) {
                        const cityNamesCount = _map(result.citiesNameCount, city => {
                            return {
                                name: city.cityName,
                                value: city.count
                            };
                        });
                        result.citiesNameCount = cityNamesCount;
                    }
                    if (result.projectsNameCount) {
                        const projectNamesCount = _map(result.projectsNameCount, project => {
                            return {
                                name: project.projectName,
                                value: project.count
                            };
                        });
                        result.projectsNameCount = projectNamesCount;
                    }
                    if (result.teamsNameCount) {
                        const teamNamesCount = _map(result.teamsNameCount, team => {
                            return {
                                name: team.teamName,
                                value: team.count
                            };
                        });
                        result.teamsNameCount = teamNamesCount;
                    }

                    return result;
                })
            );
    }

}
