import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { apiUrl } from '@app/shared/services/api';
import { Calendar } from '@app/shared/models/calendar';
import { toHttpParams } from '@shared/util';
import { omit, map, fromPairs } from 'lodash';

@Injectable({ providedIn: 'root' })

export class CalendarService {

    constructor(private http: HttpClient) { }

    getDemoEvents(payload?): Observable<any> {
        return this.http.get<Calendar>(`${apiUrl.mock}/getCalendarEvents.json`)
            .map((res: any) => {
                // map to Calendar Event
                const result = map(res, event => {
                    return {
                        id: event.id.changeKey,
                        title: event.subject,
                        start: event.start,
                        end: event.end,
                        organizer: event.organizer,
                        location: event.location,
                        reminder: event.reminderMinutesBeforeStart,
                        duration: event.duration,
                        isMeeting: event.isMeeting,
                        isNew: event.isNew,
                        isRecurring: event.isRecurring,
                        timeZone: event.timeZone,
                        displayTo: event.displayTo,
                        displayCc: event.displayCc
                    };
                });

                const totals = result.length;
                return { events: result, totals: totals };
            });
    }

    getEvents(payload?): Observable<any> {
        return this.http.get<any>(`${apiUrl.exchange}/GetAppointment` + `?${toHttpParams(payload)}`)
            .map((res: any) => {
                // map to Calendar Event
                const result = map(res, event => {
                    return {
                        id: event.id.changeKey,
                        title: event.subject,
                        start: event.start,
                        end: event.end,
                        allDay: event.isAllDayEvent,
                        organizer: event.organizer,
                        location: event.location,
                        reminder: event.reminderMinutesBeforeStart,
                        duration: event.duration,
                        isMeeting: event.isMeeting,
                        isNew: event.isNew,
                        isRecurring: event.isRecurring,
                        timeZone: event.timeZone,
                        displayTo: event.displayTo,
                        displayCc: event.displayCc,
                        textColor: '#ffffff',
                        backgroundColor: '#73a1ff'
                    };
                });

                const totals = result.length;
                return { events: result, totals: totals };
            });
    }    

}
