import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Contact } from '@app/shared/models';
import { toHttpParams } from '@shared/util';
import { map } from 'rxjs/operators/map';
import { apiUrl } from '@app/shared/services/api';

@Injectable({ providedIn: 'root' })

export class ContactService {

    constructor(private http: HttpClient) { }

    // Params: id, typeId, limit, page
    getContacts(payload?: any): Observable<any> {
        return this.http.get<Contact[]>(`${apiUrl.contacts}/GetContact` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getContactsForDashboard(payload?: any): Observable<any> {
        return this.http.get<Contact[]>(`${apiUrl.contacts}/GetContact` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true && item.enabled === true);
                    return result;
                })
            );
    }

    getAllContacts(payload?: any): Observable<any> {
        return this.http.get<Contact[]>(`${apiUrl.contacts}/GetContact` + `?${toHttpParams(payload)}`)
            .pipe(
                map((result: any) => {
                    result = result.filter(item => item.active === true);
                    return result;
                })
            );
    }

    save(payload: any): Observable<any> {
        return this.http.post<Contact>(`${apiUrl.contacts}/`, payload);
    }

    delete(payload: any): Observable<any> {
        return this.http.delete<Contact>(`${apiUrl.contacts}/DisableContact` + `?${toHttpParams(payload)}`);
    }

}
