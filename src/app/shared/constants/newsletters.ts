export const newsletters = [{
    title: 'Nr. 4 : Septembrie 2018',
    url: 'assets/newsletter/04-iw-newsletter-2018-09.pdf'
},
{
    title: 'Nr. 3 : Iulie 2018',
    url: 'assets/newsletter/03-iw-newsletter-2018-07.pdf'
},
{
    title: 'Nr. 2 : Iunie 2018',
    url: 'assets/newsletter/02-iw-newsletter-2018-06.pdf'
},
{
    title: 'Nr. 1 : Mai 2018',
    url: 'assets/newsletter/01-iw-newsletter-2018-05.pdf'
}];
