import { HttpParams } from '@angular/common/http';
import { isString, isNil, omitBy } from 'lodash';
import { isNullOrUndefined } from 'util';
import { NbMenuItem } from '@nebular/theme';

export interface IMenuItem {
  id: string;
  title: string;
  icon: string;
  link: string;
  permissions: any;
  children: any;
  active: boolean;
}


export function toHttpParams(props): HttpParams {
  return new HttpParams({
    fromObject: normalizeParams(props)
  });
}

export function normalizeParams(props) {
  return omitBy(props, (val) => isNil(val) || (isString(val) && !val));
}

export function stringToArray(str: string, separator: string) {
  return !str ? [] : str.split(separator).filter(item => item !== '');
}

export function arrayToString(arr: any[], separator: string) {
  return !arr ? '' : arr.join(separator);
}

export function toMenuItems(data: any[],
  route: string = '',
  includeDashboardRoure: boolean = true,
  includeDefaultRoute: boolean = true,
  defaultRouteLabel: string = 'all') {

  if (!data) { return []; }

  let menuItems: any[] = [];

  menuItems = mapMenuItems(data, route);

  if (includeDefaultRoute) {
    menuItems.unshift(<NbMenuItem>
      {
        title: defaultRouteLabel,
        icon: 'nb-layout-default',
        link: '',
        permissions: ['all'],
        selected: true,
        active: true,
        data: {
          id: 'all',
          route: route
        }
      }
    );
  }

  if (includeDashboardRoure) {
    menuItems.unshift(<NbMenuItem>
      {
        title: 'Dashboard',
        icon: 'nb-home',
        link: '/dashboard',
        home: true
      }
    );
  }

  return menuItems;
}

export function mapMenuItems(data: any, route: string) {

  const menuItems: NbMenuItem[] = [];

  data.forEach(obj => {

    const menuItem: NbMenuItem = <NbMenuItem>{
      title: '',
      icon: 'nb-layout-sidebar-left',
      link: '',
      data: {
        id: null,
        route: null
      }
    };

    // Map Object id to MenuItem Data
    if (!isNullOrUndefined(obj.id)) {
      menuItem.data.id = obj.id;
      menuItem.data.route = route;
    }

    // Map Object name to MenuItem label
    if (!isNullOrUndefined(obj.label)) {
      menuItem.title = obj.label;
    } else if (!isNullOrUndefined(obj.name)) {
      menuItem.title = obj.name;
    }

    // Map Object icon to MenuItem Icon
    if (!isNullOrUndefined(obj.icon)) { menuItem.icon = obj.icon; }

    // Map Object RouterLink to MenuItem Router Link
    if (!isNullOrUndefined(obj.routerLink)) { menuItem.link = obj.routerLink; }

    // Map Object Active to MenuItem Active
    // if (!isNullOrUndefined(obj.active)) { menuItem.selected = obj.active; }

    // Map Object Items to MenuItem Items
    if (!isNullOrUndefined(obj.items)) {
      menuItem.children = mapMenuItems(obj.items, route);
    }

    menuItems.push(menuItem);
  });

  return menuItems;
}
