import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FullCalendarModule } from 'ng-fullcalendar';
import { IwCalendarComponent } from '@app/shared/components/iw-calendar/components/iw-calendar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { NbSpinnerModule, NbPopoverModule, NbCardModule, NbDatepickerModule } from '@nebular/theme';
import { IwCalendarDetailPopoverComponent } from './components/calendar-detail-popover/iw-calendar-detail-popover.component';
import { IwCalendarModalFormComponent } from './components/calendar-modal-form/iw-calendar-modal-form.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        FullCalendarModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        CalendarModule,
        DropdownModule,
        InputSwitchModule,
        OverlayPanelModule,
        NbSpinnerModule,
        NbPopoverModule,
        NbCardModule,
        NbDatepickerModule.forRoot()        
    ],
    declarations: [
        IwCalendarComponent,
        IwCalendarModalFormComponent,
        IwCalendarDetailPopoverComponent
    ],
    entryComponents: [
        IwCalendarModalFormComponent
      ],    
    exports: [
        IwCalendarComponent
    ]
})

export class IwCalendarModule { }
