import { Component, OnInit, ViewChild, Output, ChangeDetectorRef, EventEmitter, SimpleChanges, OnChanges, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { CalendarService } from '@app/shared/services/calendar.service';
import * as moment from 'moment';
import { map, omit } from 'lodash';
import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'iw-calendar-detail-popover',
  templateUrl: './iw-calendar-detail-popover.component.html',
  styleUrls: ['./iw-calendar-detail-popover.component.scss']
})
export class IwCalendarDetailPopoverComponent implements OnInit {

  
  // Name of any of the available views: month, agendaWeek, agendaDay, listMonth
  @Input() defaultView: string = 'listMonth';

  // header = { left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay,listMonth'};
  @Input() header = { left: '', center: 'title', right: ''};

  loading = false;


  constructor( public calendarService: CalendarService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {

  }

 

}
