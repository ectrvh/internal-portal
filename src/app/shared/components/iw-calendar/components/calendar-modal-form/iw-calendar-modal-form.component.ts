import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from '@app/shared/services';
import { map, omit } from 'lodash';
import * as moment from 'moment';
import { Calendar } from '@app/shared/models';

@Component({
  selector: 'iw-calendar-modal-form',
  templateUrl: './iw-calendar-modal-form.component.html',
  styleUrls: ['./iw-calendar-modal-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class IwCalendarModalFormComponent implements OnInit, OnDestroy {

  event: Calendar;
  eventForm: FormGroup;  
  isUpdate: boolean;  

  startDate = new Date();
  endDate = new Date();

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,        
    private projectService: ProjectService,
    private cdr: ChangeDetectorRef) {

    // Init FormGroup From Display Dialog
    this.eventForm = this.formBuilder.group({
      id: ['', Validators.required],
      title: ['', Validators.required],      
      start: ['', Validators.required],
      end: ['', Validators.required],
      organizer: ['', Validators.required],
      location: ['', Validators.required],
      reminder: ['', Validators.required]
    });

    this.eventForm.patchValue({
      id: '',
      title: '',      
      start: new Date(),
      end: new Date(),
      organizer: '',
      location: '',
      reminder: ''
    });

    this.isUpdate = false;    
  }

  ngOnInit() {

    if (this.event) {

      this.eventForm.patchValue({
        id: this.event.id,
        title: this.event.title,        
        start: this.event.start,
        end: this.event.end,
        allDay: this.event.allDay,
        organizer: this.event.organizer,
        location: this.event.location,
        reminder: this.event.reminder
      });

      this.startDate = new Date(this.event.start);
      this.endDate = new Date(this.event.end);

      // if (this.project.usersLink) {
      //   this.project.usersLink.forEach(element => {
      //     if (element.user) {
      //       this.selectedUsersIds.push(element.user.id);
      //     }
      //   });
      // }

      this.isUpdate = true;
    } else {
      this.event = new Calendar;
    }
  }


  saveForm(details) {
    let formData = this.eventForm.getRawValue();

    if (formData) {
      let startDate = (this.event && this.event.start) ? this.event.start : new Date();
      let endDate = (this.event && this.event.end) ? this.event.end : new Date();

      if (formData.startDate) {
        startDate = moment(formData.startDate).format('YYYY-MM-DD');
      }
      if (formData.endDate) {
        endDate = moment(formData.endDate).format('YYYY-MM-DD');
      }

      formData.startDate = startDate;
      formData.endDate = endDate;      
    }

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['projectType']);

      // this.projectService.save(formData).subscribe(
      //   res => {
      //     this.closeModal({ status: 'NEW', data: res });
      //   },
      //   err => {
      //     this.closeModal({ status: 'ERROR', data: err });
      //   }
      // );
    } else { // UPDATE
      // this.projectService.save(formData).subscribe(
      //   res => {          
      //     this.closeModal({ status: 'UPDATE', data: res });                                        
      //   },
      //   err => {
      //     this.closeModal({ status: 'ERROR', data: err });
      //   }
      // );

    }

  }

  ngOnDestroy() {
    // this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
