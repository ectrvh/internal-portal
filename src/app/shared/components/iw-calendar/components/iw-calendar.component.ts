import { Component, OnInit, ViewChild, Output, ChangeDetectorRef, EventEmitter, SimpleChanges, OnChanges, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { CalendarService } from '@app/shared/services/calendar.service';
import * as moment from 'moment';
import { map, omit } from 'lodash';
import 'rxjs/add/operator/map';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IwCalendarModalFormComponent } from './calendar-modal-form/iw-calendar-modal-form.component';

@Component({
  selector: 'iw-calendar',
  templateUrl: './iw-calendar.component.html',
  styleUrls: ['./iw-calendar.component.scss']
})
export class IwCalendarComponent implements OnInit, OnChanges {

  // https://fullcalendar.io/docs
  // https://github.com/Jamaks/ng-fullcalendar

  // Name of any of the available views: month, agendaWeek, agendaDay, listMonth
  @Input() defaultView: string = 'listMonth';
  @Input() showOnCard = false;
  // header = { left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay,listMonth'};
  @Input() header = { left: '', center: 'title', right: '' };

  events = [];
  editable = true;
  eventLimit = false;
  selectable = true;
  loading = false;

  buttonText = {
    today: 'Astazi',
    month: 'Luna',
    week: 'Saptamana',
    day: 'Zi',
    list: 'Lista'
  };
  timeFormat = 'H:mm'; // 24h format
  locale: string;

  @Output() initialized = new EventEmitter<any>();

  @ViewChild(CalendarComponent) iwCalendar: CalendarComponent;
  calendarOptions: Options;
  displayEvent: any;

  popOverEventInfo = {
    title: '',
    start: '',
    end: '',
    organizer: '',
    location: '',
    reminder: ''
  };

  displayEventDialogForm = false;

  startDate: Date;
  endDate: Date;

  constructor(
    public calendarService: CalendarService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.locale = this.translate.currentLang;

    this.calendarOptions = {
      locale: this.locale,
      editable: this.editable,
      eventLimit: this.eventLimit,
      header: this.header,
      buttonText: this.buttonText,
      selectable: this.selectable,
      timeFormat: this.timeFormat,
      defaultView: this.defaultView,
      events: [],
    };

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      if (event.lang !== this.locale) {
        this.locale = event.lang;
        // console.log(buttonTextTranslated);
        const buttonText = {
          today: 'Today',
          month: 'Month',
          week: 'Week',
          day: 'Day',
          list: 'List'
        };
        this.iwCalendar.fullCalendar('option', 'locale', this.locale);
        this.iwCalendar.fullCalendar('option', 'buttonText', buttonText);
      }
    });

  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes, 'changes');
    const {
      events
    } = changes;

    const eventsChanged = !!events;

    if (eventsChanged) {
      this.events = events.currentValue;
    }
  }

  onCalendarInit() {
    this.loadEvents();
  }

  loadEvents() {
    // const calendarCurrentDate = this.iwCalendar.fullCalendar('getDate');
    const calendarCurrentView = this.iwCalendar.fullCalendar('getView');

    const payload = {
      startDate: (this.showOnCard) ? moment(new Date()).format('DD.MM.YYYY') : moment(calendarCurrentView.start).format('DD.MM.YYYY'),
      endDate: (this.showOnCard) ? moment(new Date()).add(7, 'day').format('DD.MM.YYYY') : moment(calendarCurrentView.end).format('DD.MM.YYYY')
    };

    this.loading = true;
    this.calendarService.getEvents(payload)
      .map((res: any) => {
        return (res && res.events) ? res.events : [];
      })
      .subscribe(data => {
        // this.events = data;
        this.loading = false;
        this.iwCalendar.fullCalendar('removeEvents');
        this.iwCalendar.fullCalendar('addEventSource', data);
      });
  }

  onClickButton(model: any) {
    // console.log(model);
    // Click on Button Header : Next, Prev, Month, ...
    // model.buttonType : 'prev', 'next', 'today', 'month', 'agendaWeek', 'agendaDay', 'listMonth'
    // model.data = '2018-07-01T00:00:00' as moment
    // moment(model.created).format('YYYY-MM-DD HH:mm:ss')
    this.loadEvents();
  }

  onEventMouseOver(model, op) {
    this.popOverEventInfo.title = model.event.title;
    this.popOverEventInfo.start = moment(model.event.start).format('DD/MM/YYYY hh:mm');
    this.popOverEventInfo.end = moment(model.event.end).format('DD/MM/YYYY hh:mm');
    this.popOverEventInfo.organizer = model.event.organizer;
    this.popOverEventInfo.location = model.event.location;
    this.popOverEventInfo.reminder = model.event.reminder + ' minutes'; // reminderMinutesBeforeStart
    op.show(model, model.jsEvent.target);
  }

  onEventMouseOut(model, op) {
    op.hide(model, model.jsEvent.target);
  }

  onEventRender(model: any) {
    // let element = model.element;
    // if (element.className === 'red') {
    //   element.css({
    //     'background-color': '#333333',
    //     'border-color': '#333333'
    //   });
    // }
  }


  onEventClick(model: any) {
    // console.log(model.event);

    const activeModal = this.modalService.open(IwCalendarModalFormComponent, { size: 'lg', centered: true, container: 'nb-layout' });
    activeModal.componentInstance.event = model.event;

    activeModal.result.then((res) => {
      // on close
      if (res && res.status) {

        if (res.status === 'NEW') {
          //  this.toastService.showToastSuccess('Project successfuly created');
          //  this.getProjects();
        } else if (res.status === 'UPDATE') {
          //  this.toastService.showToastSuccess('Project successfuly updated');
          //  this.getProjects();
        } else if (res.status === 'ERROR') {
          // this.toastService.showToastError(res.data);
        }

      }
    }, (reason) => {
      // on dismiss
    });


  }

  clearEvents() {
    this.events = [];
  }

}
