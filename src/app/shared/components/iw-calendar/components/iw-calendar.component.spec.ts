import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IwCalendarComponent } from './iw-calendar.component';

describe('IwCalendarComponent', () => {
  let component: IwCalendarComponent;
  let fixture: ComponentFixture<IwCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IwCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IwCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
