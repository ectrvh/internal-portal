import {
  Component,
  Input,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  ChangeDetectorRef,
  Output,
  EventEmitter
} from '@angular/core';
import { NbMenuService, NbMenuItem } from '@nebular/theme';
import { SIDEBAR_DEFAULT_MENU_ITEMS, MenuService, DASHBOARD_MENU_ITEMS } from '@app/shared/services/menu.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { NbMenuBag } from '@nebular/theme/components/menu/menu.service';
import { takeWhile } from 'rxjs/operators';
import { toMenuItems } from '@app/shared/util';

// https://akveo.github.io/nebular/docs/components/sidebar/api#nbsidebarservice

@Component({
  selector: 'app-sidebar-menu',
  styleUrls: ['./sidebar-menu.component.scss'],
  templateUrl: './sidebar-menu.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SidebarMenuComponent implements OnInit, OnDestroy {

  @Input() menuItems: NbMenuItem[];
  @Output() onHandleAction = new EventEmitter<any>(); // Click on Menu Item

  selectedMenu: NbMenuBag;
  alive = true;

  constructor(private nbMenuService: NbMenuService,
    private menuService: MenuService,
    private cdr: ChangeDetectorRef,
    private router: Router) {

    // this.nbMenuService.onItemClick()
    //   .pipe(
    //     takeWhile(() => this.alive)
    //   )
    //   .subscribe(res => {
    //     if (this.selectedMenu && this.selectedMenu.item && this.selectedMenu.item.data && this.selectedMenu.item.data === res.item.data) {
    //       return;
    //     }

    //     // console.log(this.selectedMenu, 'SIDEBAR MENU : selectedMenu');
    //     this.selectedMenu = res;
    //     this.onHandleAction.emit(this.selectedMenu);
    //   });

  }

  ngOnInit() {
    // this.selectedMenu = null;
    // const route = this.router.url;
    console.log(this.menuItems, 'menuItems');
    this.setSidebarMenuItems(this.menuItems);
  }

  goToHome() {
    this.nbMenuService.navigateHome();
  }

  setSidebarMenuItems(menuItems) {
    this.menuItems = menuItems;
    this.cdr.detectChanges();
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
