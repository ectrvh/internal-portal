import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbSidebarModule, NbMenuModule } from '@nebular/theme';
import { SidebarMenuComponent } from './sidebar-menu.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbSidebarModule,
        NbMenuModule,
    ],
    declarations: [
        SidebarMenuComponent
    ],
    exports: [
        SidebarMenuComponent
    ]
})

export class SidebarMenuModule { }
