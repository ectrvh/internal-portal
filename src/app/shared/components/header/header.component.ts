import { Component, Input, OnInit, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { NbMenuService, NbSidebarService, NbPopoverDirective } from '@nebular/theme';
import { Router } from '@angular/router';
import { HEADER_MENU_ITEMS, USER_MENU_ITEMS } from '@app/shared/services/menu.service';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { filter, map } from 'rxjs/operators';
import { AuthenticationService, UserService, FileUploadService } from '@app/shared/services';
import { User } from '@app/shared/models';
// import { AnalyticsService } from '../../../@core/utils/analytics.service';
// import { LayoutService } from '../../../@core/data/layout.service';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HeaderComponent implements OnInit {

  @ViewChild(NbPopoverDirective) popover: NbPopoverDirective;
  @Input() position = 'normal';
  currentLanguage: string = 'RO';
  topMenuItems = [];
  user: User = new User();

  userMenu = [];

  languages = [
    {
      title: 'RO',
      key: 'ro',
    },
    {
      title: 'EN',
      key: 'en',
    }
  ];

  constructor(private sidebarService: NbSidebarService,
    private userService: UserService,
    private nbMenuService: NbMenuService,
    private router: Router,
    public translate: TranslateService,
    private auth: AuthenticationService,
    private fileUploadService: FileUploadService,
    private cdr: ChangeDetectorRef
    // private analyticsService: AnalyticsService,
  ) {
    this.translate.setDefaultLang('ro');
    this.translate.use('ro');
    this.currentLanguage = this.translate.getDefaultLang().toUpperCase();

    this.topMenuItems = HEADER_MENU_ITEMS;

    this.userMenu = USER_MENU_ITEMS;
    this.translateUserMenuItems();

    this.nbMenuService.onItemClick()
      .pipe(
        filter(({ tag }) => tag === 'user-context-menu'),
        // map(({ item: { title, data } }) => menuItem),
      )
      .subscribe(menuItem => {
        if (menuItem.item.data === 'profile') {
          this.router.navigate(['user-profile']);
        } else if (menuItem.item.data === 'logout') {
          this.auth.logout();
          this.router.navigate(['login']);
        }
      });
  }

  onChangeLang(language: string) {
    this.translate.use(language);
    this.currentLanguage = language.toUpperCase();
    this.translateUserMenuItems();
  }

  ngOnInit() {
    this.user = this.userService.getUserFromLocalStorage();

    if (this.user) {
      if (this.user.havePhoto) {
        const payload = { id: this.user.id };
        this.fileUploadService.getFile(payload).subscribe(
          data => {
            this.getUserImage(this.user, data);
          },
          error => {
          }
        );
      } else {
        this.user.photoLink = 'assets/images/person-default.png';
      }
    }

  }

  translateUserMenuItems() {
    this.userMenu.forEach(item => {
      item.title = this.translate.instant(item.data);
    });
    this.cdr.markForCheck();
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');

    return false;
  }

  goToHome() {
    this.nbMenuService.navigateHome();
  }

  goToAdmin() {
    this.router.navigate(['/admin']);
  }

  startSearch() {
    // this.analyticsService.trackEvent('startSearch');
  }

  getUserImage(user, image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      user.photoLink = reader.result;
      this.cdr.markForCheck();
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

}
