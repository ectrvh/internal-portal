import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbActionsModule, NbContextMenuModule, NbUserModule, NbPopoverModule } from '@nebular/theme';
import { HeaderComponent } from './header.component';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbActionsModule,
        NbContextMenuModule,
        NbUserModule,
        NbPopoverModule,
        NgxPermissionsModule.forChild()
    ],
    declarations: [
        HeaderComponent
    ],
    exports: [
        HeaderComponent
    ],
    entryComponents: [
    ]
})

export class HeaderModule { }
