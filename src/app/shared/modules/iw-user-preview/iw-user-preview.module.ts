import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbSpinnerModule } from '@nebular/theme';
import { IwUserPreviewComponent } from './iw-user-preview.component';
import { SharedPipesModule } from '@app/shared';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbSpinnerModule,
        SharedPipesModule,
    ],
    declarations: [
        IwUserPreviewComponent
    ],
    entryComponents: [
    ],
    exports: [
        IwUserPreviewComponent
    ],
    providers: [
    ]
})

export class IwUserPreviewModule { }
