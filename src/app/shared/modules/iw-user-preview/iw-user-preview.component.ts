import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input } from '@angular/core';
import { UserService } from '@app/shared/services';
import { User } from '@app/shared/models';
import 'rxjs/add/operator/map';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'iw-user-preview',
  templateUrl: './iw-user-preview.component.html',
  styleUrls: ['./iw-user-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class IwUserPreviewComponent implements OnInit, OnDestroy {

  @Input() contact: any;
  @Input() user: any;
  @Input() userImageData: any;

  alive = true;
  loading = false;

  constructor(
    private activeModal: NgbActiveModal,
    private userService: UserService) { }


  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal() {
    this.activeModal.close();
  }

}
