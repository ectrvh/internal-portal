import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbSpinnerModule} from '@nebular/theme';
import { IwNewsletterPreviewComponent } from './iw-newsletter-preview.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbSpinnerModule,
        PdfViewerModule    
    ],
    declarations: [
        IwNewsletterPreviewComponent,        
    ],
    entryComponents: [        
      ],    
    exports: [
        IwNewsletterPreviewComponent
    ],
    providers: [        
      ]
})

export class IwNewsletterPreviewModule { }
