import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { Newsletter } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileUploadService } from '@app/shared/services';

@Component({
  selector: 'iw-newsletter-preview',
  templateUrl: './iw-newsletter-preview.component.html',
  styleUrls: ['./iw-newsletter-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class IwNewsletterPreviewComponent implements OnInit, OnDestroy {

  @Input() newsletter: Newsletter;
  @Input() fileData: any;

  alive = true;
  loading = false;

  constructor(private activeModal: NgbActiveModal,
              private fileUploadService: FileUploadService,
              private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {

    if (!this.fileData) {

      if (this.newsletter && this.newsletter.fileUploads && this.newsletter.fileUploads.length > 0) {

        this.loading = true;
        const payload = { id: this.newsletter.id };
        this.fileUploadService.getFile(payload).subscribe(
          data => {
            this.createFileFromBlob(data);
            this.loading = false;
          },
          error => {
          }
        );

      }
    }
  }


  ngOnDestroy() {
    this.alive = false;
  }

  closeModal() {
    this.activeModal.close();
  }

  createFileFromBlob(file: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.fileData = reader.result;
      this.cdr.markForCheck();
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

}
