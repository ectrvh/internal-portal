import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbSpinnerModule, NbCardModule } from '@nebular/theme';
import { IwNewsPreviewComponent } from './iw-news-preview.component';
import { SharedPipesModule } from '@app/shared';
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbCardModule,
        NbSpinnerModule,
        SharedPipesModule
    ],
    declarations: [
        IwNewsPreviewComponent
    ],
    entryComponents: [
    ],
    exports: [
        IwNewsPreviewComponent
    ],
    providers: [
    ]
})

export class IwNewsPreviewModule { }
