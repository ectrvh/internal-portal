import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input } from '@angular/core';
import { News } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'iw-news-preview',
  templateUrl: './iw-news-preview.component.html',
  styleUrls: ['./iw-news-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class IwNewsPreviewComponent implements OnInit, OnDestroy {

  @Input() news: News;

  alive = true;
  loading = false;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal() {
    this.activeModal.close();
  }

}
