import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, ChangeDetectorRef } from '@angular/core';
import { Project, ProjectCategory } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectService, ProjectCategoriesService } from '@app/shared/services';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'iw-project-preview',
  templateUrl: './iw-project-preview.component.html',
  styleUrls: ['./iw-project-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class IwProjectPreviewComponent implements OnInit, OnDestroy {

  @Input() project: Project;
  @Input() projectCategories: ProjectCategory[];

  projectCategory: any;
  projectUsers: any;
  projectUsersNameList = '';

  alive = true;
  loading = false;

  constructor(private projectService: ProjectService,
    private cdr: ChangeDetectorRef,
    private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    this.loading = true;

    this.projectCategory = this.projectCategories.find(result => result.id === this.project.projectCategoryId);
    const projectUsers = this.projectService.getProjectUsers({ projectId: this.project.id });

    forkJoin([projectUsers])
      .subscribe(result => {
        this.projectUsers = result[0];
        this.toProjectUsersNameList(this.projectUsers);
        this.loading = false;
        this.cdr.markForCheck();
      });

  }

  toProjectUsersNameList(projectUsers) {
    const usersList = [];
    if (projectUsers) {
      projectUsers.map(user => {
        usersList.push(user.fullName);
      });

      this.projectUsersNameList = usersList.join(', ');
    }
  }


  ngOnDestroy() {
    this.alive = false;
  }

  closeModal() {
    this.activeModal.close();
  }

}
