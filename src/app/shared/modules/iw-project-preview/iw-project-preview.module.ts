import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NbSpinnerModule, NbCardModule, NbButtonModule } from '@nebular/theme';
import { SharedPipesModule } from '@app/shared';
import { IwProjectPreviewComponent } from './iw-project-preview.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        NbCardModule,
        NbSpinnerModule,
        NbButtonModule,
        SharedPipesModule
    ],
    declarations: [
        IwProjectPreviewComponent        
    ],
    entryComponents: [
    ],
    exports: [
        IwProjectPreviewComponent
    ],
    providers: [
    ]
})

export class IwProjectPreviewModule { }
