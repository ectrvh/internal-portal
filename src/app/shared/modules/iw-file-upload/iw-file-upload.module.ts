import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NbSpinnerModule, NbAlertModule } from '@nebular/theme';
import { IwFileUploadComponent } from './iw-file-upload.component';
import { Ng2FileInputModule } from 'ng2-file-input';
import { FileInputService } from '@app/shared/services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        NbAlertModule,
        NbSpinnerModule,
        Ng2FileInputModule.forRoot({
            invalidFileTimeout: 8000
        })
    ],
    declarations: [
        IwFileUploadComponent,
    ],
    entryComponents: [
    ],
    exports: [
        IwFileUploadComponent
    ],
    providers: [
        FileInputService
    ]
})

export class IwFileUploadModule { }
