import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { FileInputService } from '@app/shared/services';
import { HttpEventType } from '@angular/common/http';
import { Ng2FileInputService } from 'ng2-file-input';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { RESPONSE } from '@app/shared/constants/response';
import { FileUpload } from '@app/shared/models';
class FileModel {
  name = null;
  size = 0;
}
@Component({
  selector: 'iw-file-upload',
  templateUrl: './iw-file-upload.component.html',
  styleUrls: ['./iw-file-upload.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

// https://www.npmjs.com/package/ng2-file-input

export class IwFileUploadComponent implements OnInit, OnDestroy {

  @Input() modelId: string = '';
  @Input() ownerType: string = '';
  @Input() allowedExtensions: string[];

  // here we extract informations if a document was uploaded
  @Input() fileInfo: FileUpload = new FileUpload();

  @Output() onFileUploadComplete = new EventEmitter<any>();

  multiple: boolean = false;
  showPreviews: boolean = false;
  removable: boolean = true;

  dropText: string = ' ';
  browseText: string = '';
  removeText: string = '';
  invalidFileText: string = '';

  alive = true;
  loading = false;

  newFile = false;

  public actionLog: string = "";
  public fileUploadInputId: string = "newsletterFile";
  public fileUploadProgress: number = 0;
  public fileUploadMessage: string = '';
  public saveFileError = null;
  name = '';
  size = 0;

  constructor(private fileInputService: FileInputService,
    public translate: TranslateService,
    private ng2FileInputService: Ng2FileInputService) {

    this.translate.get('Browse').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.browseText = res);

    this.translate.get('remove').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.removeText = res);

    this.translate.get('You have picked an invalid or disallowed file.').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.invalidFileText = res);

    this.translate.get('Drag file to upload').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.dropText = res);

  }

  ngOnInit() {
    // this.allowedExtensions = ['image/jpeg', 'image/png', 'application/pdf'];
    // console.log(this.fileInfo, 'fileInfo');
  }

  ngOnDestroy() {
    this.alive = false;
  }

  uploadFile(formData) {

    const res = {
      response: RESPONSE.SUCCESS,
      message: ''
    };
    this.fileInputService.uploadFile(formData.id, this.fileUploadInputId, this.ownerType).subscribe(
      event => {
        if (event) {
          if (event.type === HttpEventType.UploadProgress) {
            this.fileUploadProgress = Math.round(100 * event.loaded / event.total);
            // console.log(this.testProgress, 'this.testProgress');
          } else if (event.type === HttpEventType.Response) {
            this.fileUploadMessage = event.body.toString();
            this.onFileUploadComplete.emit(res);
          }
        } else {
          res.message = 'No file to upload';
          this.onFileUploadComplete.emit(res);
        }
      }, error => {
        this.saveFileError = RESPONSE.ERROR + ': ' + error.status + ' ' + error.statusText;

        res.response = RESPONSE.ERROR;
        res.message = this.saveFileError;

        this.onFileUploadComplete.emit(res);
      }
    );
  }

  // ng2-file-input  Proprieties
  onAction(event: any) {
    // console.log(event,'onAction');
    // this.actionLog += '\n currentFiles: ' + this.getFileNames(event.currentFiles);
  }

  onAdded(event: any) {
    this.newFile = true;
    // console.log(event, 'ADDED');
    // console.log(this.newFileAdded);
    this.name = event.currentFiles[0].name;
    this.size = event.currentFiles[0].size;
    // console.log(this.name);
    // console.log(this.size);


    // console.log(event.file.size, 'ADDED');
    // this.actionLog += '\n FileInput: ' + event.id;
    // this.actionLog += '\n Action: File added';
  }

  onRemoved(event: any) {
    // this.actionLog += '\n FileInput: ' + event.id;
    // this.actionLog += '\n Action: File removed';
  }

  onInvalidDenied(event: any) {
    // console.log(event,'onInvalidDenied');
    // this.actionLog += '\n FileInput: ' + event.id;
    // this.actionLog += '\n Action: File denied';
  }

  onCouldNotRemove(event: any) {
    // this.actionLog += '\n FileInput: ' + event.id;
    // this.actionLog += '\n Action: Could not remove file';
  }

  resetFileInput(): void {
    this.ng2FileInputService.reset(this.fileUploadInputId);
  }

  public logCurrentFiles(): void {
    // const files = this.ng2FileInputService.getCurrentFiles(this.fileUploadInputId);
    // this.actionLog += '\n The currently added files are: ' + this.getFileNames(files);
  }

  // private getFileNames(files: File[]): string {
  // const names = files.map(file => file.name);
  // return names ? names.join(', ') : 'No files currently added.';
  // }

}
