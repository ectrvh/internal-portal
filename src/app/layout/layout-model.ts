import { Injectable } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { SIDEBAR_DEFAULT_MENU_ITEMS } from '@app/shared/services';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class LayoutModel {

  sidebarMenuItems: NbMenuItem[];
  selectedMenuItem: any;
  private menuItem$ = new Subscription();

  constructor(public translate: TranslateService) {
    this.sidebarMenuItems = SIDEBAR_DEFAULT_MENU_ITEMS;
  }

  updateMenuItems(menuItems) {
    this.sidebarMenuItems = this.translateMenuItems(menuItems);
  }


  getSelectMenuItem() {
    return this.selectedMenuItem;
  }

  translateMenuItems(menuItems: NbMenuItem[]): NbMenuItem[] {
    // deep copy array (without reference)
    const translationMenu = JSON.parse(JSON.stringify(menuItems));

    for (let mainIndex = 0; mainIndex < menuItems.length; mainIndex++) {
      const mainSubscription = this.translate.stream(menuItems[mainIndex].title).subscribe(res => {
        const mainMenu = translationMenu[mainIndex];
        mainMenu.title = res;
      });

      this.menuItem$.add(mainSubscription);

      if (menuItems[mainIndex].children) {
        for (let childIndex = 0; childIndex < menuItems[mainIndex].children.length; childIndex++) {
          const childSubscription = this.translate.stream(menuItems[mainIndex].children[childIndex].title).subscribe(res => {
            const subMenu = translationMenu[mainIndex].children[childIndex];
            subMenu.title = res;
          });

          this.menuItem$.add(childSubscription);
        }
      }
    }

    return translationMenu;
  }

  ngOnDestroy() {
    this.menuItem$.unsubscribe();
  }

}
