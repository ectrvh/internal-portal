import { Component, OnInit, OnDestroy } from '@angular/core';
import { SIDEBAR_DEFAULT_MENU_ITEMS, AuthenticationService, DASHBOARD_MENU_ITEMS } from '@app/shared/services';
import { LayoutModel } from './layout-model';
@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})

export class LayoutComponent implements OnInit, OnDestroy {

    alive = true;

    constructor(
        public layoutModel: LayoutModel,
        private auth: AuthenticationService
    ) {
        this.auth.getUserPermission();
    }

    ngOnInit() {
        this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    }

    // selectSidebarMenuItem(selectedMenuItem) {
    //     this.layoutModel.selectedMenuItem = selectedMenuItem.item;
    // }

    ngOnDestroy() {
        this.alive = false;
    }

}
