import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout.routes';
import { LayoutComponent } from './layout.component';
import { RouterModule } from '@angular/router';

import {
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NbProgressBarModule,
  NbSpinnerModule
} from '@nebular/theme';

import { NbSecurityModule } from '@nebular/security';
import { FooterComponent } from './components';
import { CapitalizePipe, PluralPipe, RoundPipe, TimingPipe, NumberWithCommasPipe } from '@theme/pipes';
import { DEFAULT_THEME } from '@theme/styles/theme.default';
import { HeaderModule } from '@app/shared/components/header/header.module';
import { LayoutModel } from './layout-model';
import { PdfViewerModule } from 'ng2-pdf-viewer';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const ROUTE_MODULES = [RouterModule, LayoutRoutingModule];

const NB_MODULES = [
  NbCardModule,
  NbLayoutModule,
  NbTabsetModule,
  NbRouteTabsetModule,
  NbMenuModule,
  NbUserModule,
  NbActionsModule,
  NbSearchModule,
  NbSidebarModule,
  NbCheckboxModule,
  NbPopoverModule,
  NbContextMenuModule,
  NgbModule,
  NbSecurityModule, // *nbIsGranted directive,
  NbProgressBarModule,
  NbSpinnerModule
];

const EXTRA_MODULES = [
  HeaderModule,
  PdfViewerModule,
  NgbDropdownModule.forRoot()
];

const PIPES = [
  CapitalizePipe,
  PluralPipe,
  RoundPipe,
  TimingPipe,
  NumberWithCommasPipe
];

const COMPONENTS = [
  LayoutComponent,
  FooterComponent
];

const PROVIDERS = [
  LayoutModel
]; 

const EXTRA_PROVIDERS = [{
  provide: LOCALE_ID, useValue: 'ro-ro'
}
];

const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'default',
    },
    [DEFAULT_THEME],
  ).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS, ...PIPES],
  declarations: [...COMPONENTS, ...PIPES],
  providers: [...PROVIDERS, ...EXTRA_PROVIDERS, ...NB_THEME_PROVIDERS],
  entryComponents: [
  ],
})

export class LayoutModule { }
