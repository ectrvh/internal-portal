import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NbSecurityModule } from '@nebular/security';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin.routes';

const BASE_MODULES = [CommonModule, TranslateModule];

const ROUTE_MODULES = [RouterModule, AdminRoutingModule];

const EXTRA_MODULES = [];

const NB_MODULES = [
  NbSecurityModule, // *nbIsGranted directive,
];

const COMPONENTS = [
  AdminComponent
];


@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  exports: [...BASE_MODULES, ...NB_MODULES, ...COMPONENTS],
  declarations: [...COMPONENTS]
})

export class AdminModule { }
