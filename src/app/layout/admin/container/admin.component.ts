import { Component, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { MenuService } from '@app/shared/services';
import { LayoutModel } from '@app/layout/layout-model';
import { takeWhile } from 'rxjs/operators';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminComponent implements OnInit, OnDestroy {

    currentPageName = '';
    alive = true;

    constructor(public layoutModel: LayoutModel,
        private menuService: MenuService,
        private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.menuService
            .getAdminMenuItems()
            .pipe(takeWhile(() => this.alive))
            .subscribe(res => {
                // console.log(res);
                this.layoutModel.updateMenuItems(res);
                // this.cdr.markForCheck();
                this.cdr.detectChanges();
            });
    }

    selectSidebarMenuItem(selectedMenuItem) {
        this.currentPageName = selectedMenuItem.item.title;
    }

    ngOnDestroy() {
        this.alive = false;
    }
}
