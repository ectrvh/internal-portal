import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/shared';
import { AdminComponent } from './admin.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ROLE } from '@app/shared/constants/roles';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: AdminComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'dashboard',
                loadChildren: 'app/layout/admin/components/dashboard/dashboard.module#AdminDashboardModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'departments',
                loadChildren: 'app/layout/admin/components/department/department.module#AdminDepartmentModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'teams',
                loadChildren: 'app/layout/admin/components/team/team.module#AdminTeamModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'employees',
                loadChildren: 'app/layout/admin/components/employee/employee.module#AdminEmployeeModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'projects',
                loadChildren: 'app/layout/admin/components/project/project.module#AdminProjectModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'news',
                loadChildren: 'app/layout/admin/components/news/news.module#AdminNewsModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'news-categories',
                loadChildren: 'app/layout/admin/components/news-category/news-category.module#AdminNewsCategoryModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'news-subcategories',
                loadChildren: 'app/layout/admin/components/news-subcategory/news-subcategory.module#AdminNewsSubCategoryModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'documents',
                loadChildren: 'app/layout/admin/components/document/document.module#AdminDocumentModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'documents-categories',
                loadChildren: 'app/layout/admin/components/document-category/document-category.module#AdminDocumentCategoryModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'documents-subcategories',
                loadChildren: 'app/layout/admin/components/document-subcategory/document-subcategory.module#AdminDocumentSubCategoryModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'projects-categories',
                loadChildren: 'app/layout/admin/components/project-category/project-category.module#AdminProjectCategoryModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'newsletter',
                loadChildren: 'app/layout/admin/components/newsletter/newsletter.module#AdminNewsletterModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            },
            {
                path: 'contacts',
                loadChildren: 'app/layout/admin/components/contact/contact.module#AdminContactModule',
                pathMatch: 'full',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminRoutingModule { }
