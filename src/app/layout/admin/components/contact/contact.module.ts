import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastService } from '@app/shared/services';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminContactRoutingModule } from './contact.routes';
import { AdminContactComponent } from './contact.component';
import { AdminContactFormModalComponent } from './form-modal/form-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule
} from '@nebular/theme';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NgbModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminContactRoutingModule];

const COMPONENTS = [
  AdminContactComponent,
  AdminContactFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminContactFormModalComponent    
  ],
  providers: [
    ToastService
  ]
})

export class AdminContactModule { }
