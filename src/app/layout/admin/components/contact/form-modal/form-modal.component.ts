import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Contact, User } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '@app/shared/services';
import { omit, isEmpty } from 'lodash';
import { RESPONSE } from '@app/shared/constants/response';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-contact-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminContactFormModalComponent implements OnInit, OnDestroy {

  contact: Contact;
  users: User[];
  selectedUserId: string;

  mode = '';
  modeDelete = MODE.DELETE;

  alive = true;
  form: FormGroup;
  isUpdate: boolean;

  selectedPosition = '1';
  submitted = false;

  contactPositions = [
    { id: '1', name: '1' },
    { id: '2', name: '2' },
    { id: '3', name: '3' },
    { id: '4', name: '4' },
    { id: '5', name: '5' },
    { id: '6', name: '6' },
    { id: '7', name: '7' },
    { id: '8', name: '8' },
    { id: '9', name: '9' },
    { id: '10', name: '10' },
    { id: '11', name: '11' },
    { id: '12', name: '12' },
    { id: '13', name: '13' },
    { id: '14', name: '14' },
    { id: '15', name: '15' }
  ];

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private contactService: ContactService
  ) {

    this.form = this.formBuilder.group({
      id: [''],
      title: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]],
      location: ['', [Validators.required, Validators.minLength(3)]],
      position: [''],
      enabled: [''],
      active: [''],
      user: [''],
      userId: ['']
    });

    this.form.patchValue({
      id: '',
      title: '',
      description: '',
      location: '',
      position: '1',
      enabled: true,
      active: true,
      user: null,
      userId: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.contact) {

      this.form.patchValue({
        id: this.contact.id,
        title: this.contact.title,
        description: this.contact.description,
        location: this.contact.location,
        position: this.contact.position,
        enabled: this.contact.enabled,
        active: this.contact.active,
        user: this.contact.user,
        userId: (this.contact.user) ? this.contact.user.id : null
      });

      this.selectedUserId = (this.contact.user) ? this.contact.user.id : null;
      this.selectedPosition = this.contact.position.toString();

      this.isUpdate = true;
    } else {
      this.contact = new Contact;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE      
      formData = omit(formData, ['id']); // Need to remove key=id on Create

      this.contactService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      this.contactService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.contact.id };
    this.contactService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  onChange(model) {
    let form = this.form.getRawValue();

    if (isEmpty(form.title)) {
      this.form.patchValue({ title: model.fullName });
    }

    if (isEmpty(form.location)) {
      this.form.patchValue({ location: model.location });
    }

    if (isEmpty(form.description)) {
      this.form.patchValue({ description: 'INTERIOR ' + model.pager });
    }

  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
