import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ContactService, UserService } from '@app/shared/services';
import { Contact, ContactType, User } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminContactFormModalComponent } from './form-modal/form-modal.component';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';
import { forkJoin } from 'rxjs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-admin-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminContactComponent implements OnInit, OnDestroy {

  contacts: Contact[];
  users: User[] = [];

  contactTypes: ContactType;
  alive = true;
  loading = false;
  languageChanged: string;
  notifier: NotifierService;

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      linkedUser: {
        title: this.translate.instant('employeeLinked'),
        type: 'html',
        width: '5%',
        filter: false
      },
      title: {
        title: this.translate.instant('title'),
        type: 'text',
        filter: true,
        width: '30%'
      },
      description: {
        title: this.translate.instant('description'),
        type: 'text',
        filter: true,
        width: '30%',
      },
      location: {
        title: this.translate.instant('location'),
        type: 'text',
        filter: true,
        width: '30%'
      },
      position: {
        title: this.translate.instant('position'),
        type: 'text',
        filter: true,
        width: '5%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  textSaveSuccess = '';
  textDeleteSuccess = '';

  constructor(private modalService: NgbModal,
    private contactService: ContactService,
    private userService: UserService,
    private notifierService: NotifierService,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Contact successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Contact successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.linkedUser.title = this.translate.instant('employeeLinked');
      this.smartTableSettings.columns.title.title = this.translate.instant('title');
      this.smartTableSettings.columns.description.title = this.translate.instant('description');
      this.smartTableSettings.columns.location.title = this.translate.instant('location');
      this.smartTableSettings.columns.position.title = this.translate.instant('position');
      // this.smartTableData.refresh();
      this.initData();
    });

  }

  ngOnInit() {
    this.initData();
  }

  initData() {
    this.loading = true;
    const contacts = this.contactService.getAllContacts();
    const users = this.userService.getUserList({ limit: 300, page: 1 });

    forkJoin([contacts, users])
      .subscribe(res => {
        this.contacts = res[0];
        this.users = res[1];
        this.formatContactDataForTable(this.contacts);
        this.cdr.markForCheck();
        this.loading = false;
      });
  }

  formatContactDataForTable(contacts) {
    this.loading = true;

    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    this.contacts = contacts.map(contact => {
      contact.statusIcon = (contact.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
      contact.linkedUser = (contact.user != null) ? contact.user.fullName : 'No employee linked';
      return contact;
    });

    this.initSmartTable(this.contacts);
    this.loading = false;
    this.cdr.markForCheck();

  }

  getContacts() {
    this.loading = true;
    this.smartTableData.empty();
    this.contactService.getAllContacts()
      .subscribe(
        contacts => {
          this.formatContactDataForTable(contacts);
        }
      );
  }

  initSmartTable(data) {
    this.smartTableData.empty();
    this.smartTableData.load(data);
    this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);

  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminContactFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );
    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.contact = null;
    activeModal.componentInstance.contactTypes = this.contactTypes;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getContacts();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    const activeModal = this.modalService.open(
      AdminContactFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );
    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.contact = event.data;
    activeModal.componentInstance.contactTypes = this.contactTypes;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getContacts();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    const activeModal = this.modalService.open(
      AdminContactFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );
    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.contact = event.data;
    activeModal.componentInstance.contactTypes = null;
    activeModal.componentInstance.users = null;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getContacts();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  ngOnDestroy() {
    this.alive = false;
    // this.cdr.detach();
  }

}

