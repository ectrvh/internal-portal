import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminContactComponent } from './contact.component';

const routes: Routes = [
    {
        path: '',
        component: AdminContactComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminContactRoutingModule {}
