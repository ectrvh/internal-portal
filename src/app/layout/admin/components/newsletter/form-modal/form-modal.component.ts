import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild } from '@angular/core';
import { Newsletter, FileUpload } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsletterService } from '@app/shared/services';
import { map, omit } from 'lodash';
import * as moment from 'moment';
import { IwFileUploadComponent } from '@app/shared/modules/iw-file-upload/iw-file-upload.component';
import { RESPONSE } from '@app/shared/constants/response';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-newsletter-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsletterFormModalComponent implements OnInit, OnDestroy {

  @ViewChild(IwFileUploadComponent) iwFileUpload: IwFileUploadComponent;

  alive = true;

  newsletter: Newsletter;
  now = new Date();

  dateFormatted = this.now;
  form: FormGroup;
  isUpdate = false;
  fileUploads: FileUpload;

  mode = '';
  modeDelete = MODE.DELETE;
    
  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private newsletterService: NewsletterService) {

    this.form = this.formBuilder.group({
      id: ['', Validators.required],
      title: ['', Validators.required],
      date: ['', Validators.required],
      content: ['', Validators.required],
      enabled: ['', Validators.required],
      active: ['', Validators.required]
    });

    this.form.patchValue({
      id: '',
      title: '',
      date: this.now,
      content: '',
      enabled: true,
      active: true
    });

  }

  ngOnInit() {
    if (this.newsletter) {
      this.isUpdate = true;

      if (this.newsletter.fileUploads.length > 0) {
        this.fileUploads = this.newsletter.fileUploads[0];
        // console.log(this.fileUploads);
      } else {
        this.fileUploads = null;
      }

      if (this.newsletter.date === '' || this.newsletter.date === 'Invalid date') {
        this.newsletter.date = this.now.toString();
      }
      this.form.patchValue({
        id: this.newsletter.id,
        title: this.newsletter.title,
        date: new Date(this.newsletter.date),
        content: this.newsletter.content,
        enabled: this.newsletter.enabled,
        active: this.newsletter.active
      });
      this.dateFormatted = new Date(this.newsletter.date);
    } else {
      this.newsletter = new Newsletter;
      this.fileUploads = null;
      this.newsletter.date = this.now.toString();
    }
  }

  saveForm(details) {
    let formData = this.form.getRawValue();
    if (formData) {
      let date = (this.newsletter && this.newsletter.date) ? this.newsletter.date : this.now;
      if (formData.date) {
        date = moment(formData.date).format('YYYY-MM-DD');
      }
      formData.date = date;
    }
    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      this.newsletterService.create(formData).subscribe(
        res => {
          formData.id = res.id;
          this.iwFileUpload.uploadFile(formData);
        },
        err => {          
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      this.newsletterService.update(formData).subscribe(
        res => {
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.newsletter.id };
    this.newsletterService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }


  fileUploadCompleted(event) {
    if (event && event.response !== RESPONSE.ERROR) {
      this.closeModal({ response: event.response, message: event.message });
    }
  }

}
