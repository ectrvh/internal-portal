import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbSelectModule,
  NbAlertModule,
  NbToastrModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastService, FileInputService } from '@app/shared/services';
import { AdminNewsletterFormModalComponent } from './form-modal/form-modal.component';
import { AdminNewsletterRoutingModule } from './newsletter.routes';
import { AdminNewsletterComponent } from './newsletter.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { IwNewsletterPreviewModule } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.module';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';
import { IwFileUploadModule } from '@app/shared/modules/iw-file-upload/iw-file-upload.module';
import { CalendarModule } from 'primeng/calendar';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NbAlertModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot(),
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  PdfViewerModule,
  IwNewsletterPreviewModule,
  IwFileUploadModule,
  CalendarModule
];

const ROUTE_MODULES = [AdminNewsletterRoutingModule];

const COMPONENTS = [
  AdminNewsletterComponent,
  AdminNewsletterFormModalComponent  
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminNewsletterFormModalComponent,    
    IwNewsletterPreviewComponent
  ],
  providers: [
    ToastService,
    FileInputService
  ]
})


export class AdminNewsletterModule { }
