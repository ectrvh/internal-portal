import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminNewsletterComponent } from './newsletter.component';

const routes: Routes = [
    {
        path: '',
        component: AdminNewsletterComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminNewsletterRoutingModule {}
