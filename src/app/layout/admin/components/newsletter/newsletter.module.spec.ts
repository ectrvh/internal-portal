import { AdminNewsletterModule } from './newsletter.module';

describe('AdminNewsletterModule', () => {
  let adminNewsletterModule: AdminNewsletterModule;

  beforeEach(() => {
    adminNewsletterModule = new AdminNewsletterModule();
  });

  it('should create an instance', () => {
    expect(adminNewsletterModule).toBeTruthy();
  });
});
