import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsletterService, ToastService } from '@app/shared/services';
import { Newsletter } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { AdminNewsletterFormModalComponent } from './form-modal/form-modal.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';
import { RESPONSE } from '@app/shared/constants/response';
import { takeWhile } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsletterComponent implements OnInit, OnDestroy {

  loading = false;
  newsletters: Newsletter[];
  alive = true;
  languageChanged: string;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'preview',
          title: '<i class="nb-search text-warning"></i>',
        },
        {
          name: 'edit',
          title: '<i class="nb-edit text-primary"></i>',
        },
        {
          name: 'delete',
          title: '<i class="nb-trash text-danger"></i>',
        }
      ],
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      title: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '70%'
      },
      dateFormatted: {
        title: this.translate.instant('date'),
        type: 'text',
        filter: true,
        width: '10%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(private modalService: NgbModal,
    private newsletterService: NewsletterService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Newsletter successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Newsletter successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.title.title = this.translate.instant('name');
      this.smartTableSettings.columns.dateFormatted.title = this.translate.instant('date');
      // this.smartTableData.refresh();
      this.getNewsletters();

    });

  }

  ngOnInit() {
    this.getNewsletters();
  }

  ngOnDestroy() {
    this.alive = false;
  }


  getNewsletters() {
    this.loading = true;
    this.smartTableData.empty();
    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';
    const previewDisplayHtml = '<i class="ion-search font-w-bold text-warning text-center display-block"></i>';
    // <a (click)="showNewsDetails(news, $event)" style="cursor: pointer">
    this.newsletterService.getAllNewsletters()
      .subscribe(
        newsletters => {
          this.newsletters = newsletters.filter(res => res.active === true)
            .map(res => {
              res.statusIcon = (res.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
              res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
              return res;
            });

          this.smartTableData.load(this.newsletters);
          this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
          this.loading = false;
          this.cdr.markForCheck();
        }

      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminNewsletterFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.newsletter = null;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNewsletters();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminNewsletterFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.newsletter = event.data;

    activeModal.result.then((res) => {
      // on close

      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNewsletters();
        }

      }

    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminNewsletterFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.newsletter = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getNewsletters();
        }

      }

    }, (reason) => {
      // on dismiss
    });

  }

  onPreview(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      IwNewsletterPreviewComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.newsletter = event.data;

    activeModal.result.then((res) => {
      // on close
    }, (reason) => {
      // on dismiss
    });

  }

  onCustom(event) {
    if (event.action == 'edit') {
      this.onEdit(event);
    } else if (event.action === 'delete') {
      this.onDelete(event);
    } else if (event.action === 'preview') {
      this.onPreview(event);
    }

    // console.log(`Custom event '${event.action}' fired on row №: ${event.data.id}`)
  }


}


