import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DocumentCategoriesService, DepartmentService } from '@app/shared/services';
import { DocumentCategory, Department } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDocumentCategoryFormModalComponent } from './form-modal/form-modal.component';
import { map, omit } from 'lodash';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { takeWhile } from 'rxjs/operators';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-document-category',
  templateUrl: './document-category.component.html',
  styleUrls: ['./document-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentCategoryComponent implements OnInit, OnDestroy {

  documentCategorys: DocumentCategory[];
  alive = true;
  loading = false;
  departments: Department[];
  languageChanged: string;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      name: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '40%'
      },
      description: {
        title: this.translate.instant('description'),
        type: 'text',
        filter: true,
        width: '40%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(private modalService: NgbModal,
    private documentCategoriesService: DocumentCategoriesService,
    private departmentService: DepartmentService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Document Category successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Document Category successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.name.title = this.translate.instant('name');
      this.smartTableSettings.columns.description.title = this.translate.instant('description');
      // this.smartTableData.refresh();
      this.getDocumentsCategories();
      this.getDepartments();
    });

  }

  ngOnInit() {
    this.getDocumentsCategories();
    this.getDepartments();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getDocumentsCategories() {
    this.loading = true;
    this.smartTableData.empty();
    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    this.documentCategoriesService.getAllDocumentsCategories()
      .subscribe(
        documentCategorys => {

          this.documentCategorys = documentCategorys
            .filter(res => res.active === true)
            .map(res => {
              res.statusIcon = (res.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
              return res;
            });

          this.smartTableData.load(this.documentCategorys);
          this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
          this.loading = false;
          this.cdr.markForCheck();
        }

      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  getDepartments() {
    this.departmentService.getAllDepartments().subscribe(department => {
      // console.log(department, 'departmente');
      this.departments = department;
    });
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminDocumentCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.documentCategory = null;
    activeModal.componentInstance.departments = this.departments;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDocumentsCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminDocumentCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.documentCategory = event.data;
    activeModal.componentInstance.departments = this.departments;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDocumentsCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminDocumentCategoryFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.documentCategory = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getDocumentsCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

}


