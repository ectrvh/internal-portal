import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDocumentCategoryComponent } from './document-category.component';

const routes: Routes = [
    {
        path: '',
        component: AdminDocumentCategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminDocumentCategoryRoutingModule {}
