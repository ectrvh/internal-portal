import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbToastrModule,
  NbSelectModule
} from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '@app/shared/services';
import { AdminDocumentCategoryComponent } from './document-category.component';
import { AdminDocumentCategoryFormModalComponent } from './form-modal/form-modal.component';
import { AdminDocumentCategoryRoutingModule } from './document-category.routes';
import { NgSelectModule } from '@ng-select/ng-select';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminDocumentCategoryRoutingModule];

const COMPONENTS = [
  AdminDocumentCategoryComponent,
  AdminDocumentCategoryFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminDocumentCategoryFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminDocumentCategoryModule { }
