import { AdminDocumentCategoryModule } from './document-category.module';

describe('AdminDocumentCategoryModule', () => {
  let adminDocumentCategoryModule: AdminDocumentCategoryModule;

  beforeEach(() => {
    adminDocumentCategoryModule = new AdminDocumentCategoryModule();
  });

  it('should create an instance', () => {
    expect(adminDocumentCategoryModule).toBeTruthy();
  });
});
