import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminDocumentCategoryComponent } from './document-category.component';

describe('AdminDocumentCategoryComponent', () => {
  let component: AdminDocumentCategoryComponent;
  let fixture: ComponentFixture<AdminDocumentCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDocumentCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDocumentCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
