import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DocumentCategory, Department } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentCategoriesService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-document-category-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentCategoryFormModalComponent implements OnInit, OnDestroy {

  documentCategory: DocumentCategory;
  alive = true;
  departments: Department;
  selecteddepartmentId: string;

  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;
  
  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private documentCategoriesService: DocumentCategoriesService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', Validators.required],
      enabled: [''],
      active: [''],
      departmentId: ['']
    });


    this.form.patchValue({
      id: '',
      name: '',
      description: '',
      enabled: true,
      active: true,
      departmentId: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.documentCategory) {
      this.form.patchValue({
        id: this.documentCategory.id,
        name: this.documentCategory.name,
        description: this.documentCategory.description,
        enabled: this.documentCategory.enabled,
        active: this.documentCategory.active,
        departmentId: this.documentCategory.departmentId
      });
      this.selecteddepartmentId = this.documentCategory.departmentId;
      this.isUpdate = true;
    } else {
      this.documentCategory = new DocumentCategory;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;
    console.log(this.form);

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['documents']);
      if (formData.departmentId === undefined) {
        formData.departmentId = null;
      }
      this.documentCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      if (formData.departmentId === undefined) {
        formData.departmentId = null;
        // console.log('a intrat aici?');
      }
      this.documentCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.documentCategory.id };

    this.documentCategoriesService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });        
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });        
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
