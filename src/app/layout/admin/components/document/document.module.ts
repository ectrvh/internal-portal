import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbSelectModule,
  NbToastrModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastService } from '@app/shared/services';
import { AdminDocumentFormModalComponent } from './form-modal/form-modal.component';
import { AdminDocumentRoutingModule } from './document.routes';
import { AdminDocumentComponent } from './document.component';
import { IwFileUploadModule } from '@app/shared/modules/iw-file-upload/iw-file-upload.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { CalendarModule } from 'primeng/calendar';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()

];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  IwFileUploadModule,
  NgSelectModule,
  CalendarModule
];

const ROUTE_MODULES = [AdminDocumentRoutingModule];

const COMPONENTS = [
  AdminDocumentComponent,
  AdminDocumentFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminDocumentFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminDocumentModule { }
