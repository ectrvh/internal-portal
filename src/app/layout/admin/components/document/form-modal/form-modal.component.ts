import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild } from '@angular/core';
import { DocumentSubCategory, Document, FileUpload } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentService } from '@app/shared/services';
import { map, omit } from 'lodash';
import * as moment from 'moment';
import { IwFileUploadComponent } from '@app/shared/modules/iw-file-upload/iw-file-upload.component';
import { RESPONSE } from '@app/shared/constants/response';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-document-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentFormModalComponent implements OnInit, OnDestroy {

  @ViewChild(IwFileUploadComponent) iwFileUpload: IwFileUploadComponent;

  document: Document;
  documentSubCategories: DocumentSubCategory[];
  documentDate = new Date();
  selectedDocumentSubCategoryId: string;
  alive = true;
  fileUploads: FileUpload;
  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;
  
  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private documentService: DocumentService) {

    this.form = this.formBuilder.group({
      id: [''],
      title: [null, [Validators.required, Validators.minLength(3)]],
      date: [''],
      link: [''],
      documentSubCategoryId: ['', Validators.required],
      documentSubCategory: [''],
      enabled: [''],
      showInDashboard: [''],
      active: ['']
    });

    this.form.patchValue({
      id: '',
      title: '',
      date: new Date(),
      link: '',
      documentSubCategoryId: null,
      documentSubCategory: null,
      enabled: true,
      showInDashboard: false,
      active: true
    });

    this.isUpdate = false;

  }

  ngOnInit() {

    if (this.document) {
      if (this.document.fileUploads[0]) {
        this.fileUploads = this.document.fileUploads[0];
      } else {
        this.fileUploads = null;
      }
      if (this.document.date === '' || this.document.date === 'Invalid date') {
        this.document.date = this.documentDate.toString();
      }

      this.form.patchValue({
        id: this.document.id,
        title: this.document.title,
        date: new Date(this.document.date),
        link: this.document.link,
        documentSubCategoryId: this.document.documentSubcategoryId,
        documentSubCategory: this.document.documentSubcategory,
        enabled: this.document.enabled,
        showInDashboard: this.document.showInDashboard,
        active: this.document.active
      });

      this.selectedDocumentSubCategoryId = this.document.documentSubcategoryId;
      this.documentDate = new Date(this.document.date);

      // if (this.document.usersLink) {
      //   this.project.usersLink.forEach(element => {
      //     if (element.user) this.selectedUsersIds.push(element.user.id);
      //   })
      // }

      this.isUpdate = true;

    } else {
      this.fileUploads = null;
      this.document = new Document;
      this.document.date = this.documentDate.toString();
    }

  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {

    this.submitted = true;
    console.log(this.form);

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (formData) {
      let date = (this.document && this.document.date) ? this.document.date : new Date();

      if (formData.date) {
        date = moment(formData.date).format('YYYY-MM-DD');
      }

      formData.date = date;

      // if (formData.usersLink) {
      //   formData.usersLink = formData.usersLink
      //     .map((res: any) => {
      //       return { userId: res, projectId: formData.id };
      //     });
      // }
    }

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['documentSubCategory']);

      this.documentService.save(formData).subscribe(
        res => {
          formData.id = res.id;
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });             
        }
      );
    } else { // UPDATE
      formData = omit(formData, ['documentSubCategory']);
      this.documentService.save(formData).subscribe(
        res => {
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.document.id };
    this.documentService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }


  fileUploadCompleted(event) {
    if (event && event.response !== RESPONSE.ERROR) {
      this.closeModal({ response: event.response, message: event.message });
    }
  }

}
