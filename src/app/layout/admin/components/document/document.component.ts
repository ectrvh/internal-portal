import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DocumentService, ToastService, DocumentSubCategoriesService } from '@app/shared/services';
import { Document, DocumentSubCategory } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDocumentFormModalComponent } from './form-modal/form-modal.component';
import * as moment from 'moment';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/operators';
import { RESPONSE } from '@app/shared/constants/response';
import { NotifierService } from 'angular-notifier';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentComponent implements OnInit, OnDestroy {

  documents: Document[];
  documentSubCategories: DocumentSubCategory[];
  alive = true;
  loading = false;
  languageChanged: string;

  notifier: NotifierService;

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      showInDashboardIcon: {
        title: this.translate.instant('dashboard'),
        type: 'html',
        width: '5%',
        filter: false
      },
      title: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '60%'
      },
      documentSubCategoryName: {
        title: this.translate.instant('documentSubcategory'),
        type: 'text',
        filter: true,
        width: '25%'
      },
      dateFormatted: {
        title: this.translate.instant('date'),
        type: 'text',
        filter: true,
        width: '10%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  documentSaveSuccess = '';
  documentDeleteSuccess = '';

  constructor(private modalService: NgbModal,
    private documentService: DocumentService,
    private documentSubcategoriesService: DocumentSubCategoriesService,
    private toastService: ToastService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Document successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.documentSaveSuccess = res);

    this.translate.get('Document successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.documentDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.showInDashboardIcon.title = this.translate.instant('showInDashboard');
      this.smartTableSettings.columns.title.title = this.translate.instant('name');
      this.smartTableSettings.columns.documentSubCategoryName.title = this.translate.instant('documentSubcategory');
      this.smartTableSettings.columns.dateFormatted.title = this.translate.instant('date');
      // this.smartTableData.refresh();
      this.getDocuments();
      this.getDocumentsSubcategory();
    });

  }

  ngOnInit() {
    this.getDocuments();
    this.getDocumentsSubcategory();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getDocuments() {
    this.loading = true;
    this.smartTableData.empty();
    const statusSuccessIcon = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const statusDangerIcon = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';
    const showInDasboadWarningIcon = '<i class="fas fa-star text-warning display-block text-center"></i>';

    this.documentService.getAllDocuments().subscribe(results => {

      this.documents = results.map(res => {
        res.statusIcon = (res.enabled) ? statusSuccessIcon : statusDangerIcon;
        res.showInDashboardIcon = (res.showInDashboard) ? showInDasboadWarningIcon : ' ';
        res.documentSubCategoryName = res.documentSubcategory.name;
        // res.date = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
        res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
        return res;
      });
      this.smartTableData.load(this.documents);
      this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
      this.loading = false;
      this.cdr.markForCheck();
    });
  }

  getDocumentsSubcategory() {
    this.documentSubcategoriesService.getDocumentSubCategoriesForSelect()
      .subscribe(
        documentSubcategories => {
          this.documentSubCategories = documentSubcategories;
        }
      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminDocumentFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.document = null;
    activeModal.componentInstance.documentSubCategories = this.documentSubCategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {
        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.documentSaveSuccess);
          this.getDocuments();
        }
      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminDocumentFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.document = event.data;
    activeModal.componentInstance.documentSubCategories = this.documentSubCategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {
        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.documentSaveSuccess);
          this.getDocuments();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onDelete(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminDocumentFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.document = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.documentDeleteSuccess);
          this.getDocuments();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }
}

