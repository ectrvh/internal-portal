import { AdminDocumentModule } from './document.module';

describe('AdminDocumentModule', () => {
  let adminDocumentModule: AdminDocumentModule;

  beforeEach(() => {
    adminDocumentModule = new AdminDocumentModule();
  });

  it('should create an instance', () => {
    expect(adminDocumentModule).toBeTruthy();
  });
});
