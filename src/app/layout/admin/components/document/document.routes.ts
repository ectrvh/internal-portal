import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDocumentComponent } from './document.component';

const routes: Routes = [
    {
        path: '',
        component: AdminDocumentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminDocumentRoutingModule {}
