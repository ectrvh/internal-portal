import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ProjectService, UserService, ProjectCategoriesService } from '@app/shared/services';
import { User, Project, ProjectCategory } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminProjectFormModalComponent } from './form-modal/form-modal.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/operators';
import { RESPONSE } from '@app/shared/constants/response';
import * as moment from 'moment';
import { forkJoin } from 'rxjs';
import { MODE } from '@app/shared/constants/mode';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-admin-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminProjectComponent implements OnInit, OnDestroy {

  projects: Project[];
  projectCategories: ProjectCategory[];
  users: User[];
  alive = true;
  loading = false;
  languageChanged: string;
  notifier: NotifierService;

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      title: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '40%'
      },
      projectCategoryName: {
        title: this.translate.instant('projectCategory'),
        type: 'html',
        filter: true,
        width: '25%'
      },
      startDateFormatted: {
        title: this.translate.instant('startDate'),
        type: 'text',
        filter: true,
        width: '10%'
      },
      endDateFormatted: {
        title: this.translate.instant('endDate'),
        type: 'text',
        filter: true,
        width: '10%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  projectSaveSuccess = '';
  projectDeleteSuccess = '';

  constructor(private modalService: NgbModal,
    private projectService: ProjectService,
    private projectCategoriesService: ProjectCategoriesService,
    private userService: UserService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Project successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.projectSaveSuccess = res);

    this.translate.get('Project successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.projectDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.title.title = this.translate.instant('title');
      this.smartTableSettings.columns.projectCategoryName.title = this.translate.instant('projectCategory');
      this.smartTableSettings.columns.startDateFormatted.title = this.translate.instant('startDate');
      this.smartTableSettings.columns.endDateFormatted.title = this.translate.instant('endDate');
      // this.smartTableData.refresh();
      this.getProjects();
      this.getUsers();
    });

  }

  ngOnInit() {
    this.getProjects();
    this.getUsers();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getProjects() {
    this.loading = true;
    this.smartTableData.empty();
    const statusSuccessIcon = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const statusDangerIcon = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';
    const allProjectCategories = this.projectCategoriesService.getProjectsCategoriesForSelect();
    const allProjects = this.projectService.getAllProjects();

    forkJoin([allProjectCategories, allProjects]).subscribe(results => {
      this.projectCategories = results[0];
      this.projects = results[1].map(res => {
        const projectCategory = this.projectCategories.find(result => result.id === res.projectCategoryId);
        if (projectCategory) {
          res.projectCategory = projectCategory;
          res.projectCategoryName = projectCategory.name;
        }
        res.statusIcon = (res.enabled) ? statusSuccessIcon : statusDangerIcon;
        // res.date = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
        res.startDateFormatted = (res.startDate) ? moment(res.startDate).format('DD.MM.YYYY') : '';
        res.endDateFormatted = (res.endDate) ? moment(res.endDate).format('DD.MM.YYYY') : '';
        res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
        return res;
      });
      this.smartTableData.load(this.projects);
      this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
      this.loading = false;
      this.cdr.markForCheck();
    });

    // this.projectService.getAllProjects()
    //   .subscribe(
    //     projects => {

    //       this.projects = projects.map(res => {
    //         res.statusIcon = (res.enabled) ? successDisplayHtml : dangerDisplayHtml;
    //         // res.projectCategoryName = (res.allProjectCategorys && res.projectCategorys.name) 
    //  ? res.projectCategorys.name + ((!res.projectCategorys.enabled) 
    //    ? '    <i class="fas fa-exclamation-circle text-warning"></i>' : '') : '';
    //         res.projectCategoryName = 'TO DO De facut fork cu Project Type';
    //         res.startDateFormatted = (res.startDate) ? moment(res.startDate).format('DD.MM.YYYY') : '';
    //         res.endDateFormatted = (res.endDate) ? moment(res.endDate).format('DD.MM.YYYY') : '';
    //         return res;
    //       });
    //       this.smartTableData.load(this.projects);
    //       this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
    //       this.loading = false;
    //       this.cdr.markForCheck();
    //     }
    //   );
  }

  getUsers() {
    this.userService.getUserList({ limit: 300, page: 1 })
      .subscribe(
        users => {
          this.users = users;
        }
      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminProjectFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });
    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.project = null;
    activeModal.componentInstance.projectCategories = this.projectCategories;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {
        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.projectSaveSuccess);
          this.getProjects();
        }
      }

    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminProjectFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });
    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.project = event.data;
    activeModal.componentInstance.projectCategories = this.projectCategories;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {
        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.projectSaveSuccess);
          this.getProjects();
        }
      }

    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    const activeModal = this.modalService.open(
      AdminProjectFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );
    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.project = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.projectDeleteSuccess);
          this.getProjects();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

}


