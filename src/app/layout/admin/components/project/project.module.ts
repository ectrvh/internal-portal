import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastService } from '@app/shared/services';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminProjectComponent } from './project.component';
import { AdminProjectFormModalComponent } from './form-modal/form-modal.component';
import { AdminProjectRoutingModule } from './project.routes';
import { IwFileUploadModule } from '@app/shared/modules/iw-file-upload/iw-file-upload.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { CalendarModule } from 'primeng/calendar';
import { EditorModule } from 'primeng/editor';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NbAlertModule,
  NbToastrModule
} from '@nebular/theme';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NbAlertModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  CalendarModule,
  EditorModule,
  Ng2SmartTableModule,
  IwFileUploadModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminProjectRoutingModule];

const COMPONENTS = [
  AdminProjectComponent,
  AdminProjectFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminProjectFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminProjectModule { }
