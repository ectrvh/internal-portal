import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild } from '@angular/core';
import { User, ProjectCategory, Project, FileUpload } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectService } from '@app/shared/services';
import { omit } from 'lodash';
import * as moment from 'moment';
import { IwFileUploadComponent } from '@app/shared/modules/iw-file-upload/iw-file-upload.component';
import { RESPONSE } from '@app/shared/constants/response';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-project-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminProjectFormModalComponent implements OnInit, OnDestroy {

  @ViewChild(IwFileUploadComponent) iwFileUpload: IwFileUploadComponent;

  project: Project;
  editor: any;
  projectCategories: ProjectCategory[];
  selectedProjectCategoryId: string;
  users: User[];
  selectedUsersIds = [];

  mode = '';
  modeDelete = MODE.DELETE;

  alive = true;
  form: FormGroup;
  isUpdate: boolean;

  now = new Date();
  submitted = false;
  pictureUpload: FileUpload;


  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private projectService: ProjectService) {

    this.form = this.formBuilder.group({
      id: [''],
      title: ['', [Validators.required, Validators.maxLength(150)]],
      details: ['', [Validators.required, Validators.maxLength(4000)]],
      projectCategoryId: ['', Validators.required],
      projectCategory: [''],
      startDate: [''],
      endDate: [''],
      enabled: [''],
      active: [''],
      usersLink: ['']
    });

    this.form.patchValue({
      id: '',
      title: '',
      details: '',
      projectCategoryId: '',
      projectCategory: '',
      startDate: this.now,
      endDate: this.now,
      enabled: true,
      active: true,
      usersLink: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {
    if (this.project) {
      this.isUpdate = true;

      if (this.project.fileUploads.length > 0) {
        this.pictureUpload = this.project.fileUploads[0];
        // console.log(this.pictureUpload);
      } else {
        this.pictureUpload = null;
      }

      if (this.project.startDate === '' || this.project.startDate === 'Invalid date') {
        this.project.startDate = this.now.toString();
      }

      if (this.project.endDate === '' || this.project.endDate === 'Invalid date') {
        this.project.endDate = this.now.toString();
      }

      this.form.patchValue({
        id: this.project.id,
        title: this.project.title,
        details: this.project.details,
        projectCategoryId: this.project.projectCategoryId,
        projectCategory: this.project.projectCategory,
        startDate: new Date(this.project.startDate),
        endDate: new Date(this.project.endDate),
        enabled: this.project.enabled,
        active: this.project.active
      });

      this.selectedProjectCategoryId = this.project.projectCategoryId;

      if (this.project.usersLink) {
        this.project.usersLink.forEach(element => {
          this.selectedUsersIds.push(element.userId);
        });
      }

    } else {
      this.project = new Project;
      this.project.startDate = this.now.toString();
      this.project.endDate = this.now.toString();
      this.pictureUpload = null;
    }

  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;
    // console.log(this.form);
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    let formData = this.form.getRawValue();

    if (formData) {
      let startDate = (this.project && this.project.startDate) ? this.project.startDate : new Date();
      let endDate = (this.project && this.project.endDate) ? this.project.endDate : new Date();

      if (formData.startDate) {
        startDate = moment(formData.startDate).format('YYYY-MM-DD');
      }
      if (formData.endDate) {
        endDate = moment(formData.endDate).format('YYYY-MM-DD');
      }

      formData.startDate = startDate;
      formData.endDate = endDate;

      if (formData.usersLink) {
        formData.usersLink = formData.usersLink
          .map((res: any) => {
            return { userId: res, projectId: formData.id, isCoordinator: false };
          });
      }
    }

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      // formData = omit(formData, ['projectCategory']);
      formData = omit(formData, ['usersLink']);
      this.projectService.save(formData).subscribe(
        res => {
          formData.id = res.id;
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      // console.log(formData, 'formdata Update');
      this.projectService.save(formData).subscribe(
        res => {
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.project.id };
    this.projectService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

  fileUploadCompleted(event) {
    if (event && event.response !== RESPONSE.ERROR) {
      this.closeModal({ response: event.response, message: event.message });
    }
  }
}
