import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminProjectComponent } from './project.component';

const routes: Routes = [
    {
        path: '',
        component: AdminProjectComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminProjectRoutingModule {}
