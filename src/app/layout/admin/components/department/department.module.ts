import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AdminDepartmentComponent } from './department.component';
import { AdminDepartmentFormModalComponent } from './form-modal/form-modal.component';
import { AdminDepartmentRoutingModule } from './department.routes';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbToastrModule
} from '@nebular/theme';
import { ToastService } from '@app/shared/services';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbToastrModule.forRoot(),
  NgbModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule
];

const ROUTE_MODULES = [AdminDepartmentRoutingModule];

const COMPONENTS = [
  AdminDepartmentComponent,
  AdminDepartmentFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminDepartmentFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminDepartmentModule { }
