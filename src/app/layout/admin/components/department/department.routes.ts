import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDepartmentComponent } from './department.component';

const routes: Routes = [
    {
        path: '',
        component: AdminDepartmentComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminDepartmentRoutingModule {}
