import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Department } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-department-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDepartmentFormModalComponent implements OnInit, OnDestroy {

  department: Department;
  alive = true;
  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private departmentService: DepartmentService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      enabled: [''],
      active: ['', Validators.required]
    });

    this.form.patchValue({
      id: '',
      name: '',
      enabled: true,
      active: true
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.department) {
      this.form.patchValue({
        id: this.department.id,
        name: this.department.name,
        enabled: this.department.enabled,
        active: this.department.active
      });
      this.isUpdate = true;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;
    // console.log(this.form);
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id

      this.departmentService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });          
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });          
        }
      );
    } else { // UPDATE
      this.departmentService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.department.id };
    this.departmentService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
