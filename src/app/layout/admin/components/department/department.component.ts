import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Department } from '@app/shared/models';
import { DepartmentService } from '@app/shared/services';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDepartmentFormModalComponent } from './form-modal/form-modal.component';
import { MODE } from '@app/shared/constants/mode';
import { NotifierService } from 'angular-notifier';
import { RESPONSE } from '@app/shared/constants/response';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-admin-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDepartmentComponent implements OnInit, OnDestroy {

  departments: Department[];
  alive = true;
  loading = false;
  languageChanged: string;
  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  // https://akveo.github.io/ng2-smart-table/#/documentation

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      name: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '90%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();


  constructor(private modalService: NgbModal,
    private departmentService: DepartmentService,    
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;    

    this.translate.get('Department successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Department successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.name.title = this.translate.instant('name');
      // this.smartTableData.refresh();
      this.getDepartments();
    });
  }

  ngOnInit() {
    this.getDepartments();
  }

  ngOnDestroy() {
    this.alive = false;
    // this.cdr.detach();
  }

  getDepartments() {
    this.loading = true;
    this.smartTableData.empty();
    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    this.departmentService.getAllDepartments()
      .subscribe(
        departments => {
          this.departments = departments.map(res => {
            res.statusIcon = (res.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
            return res;
          });
          this.smartTableData.load(this.departments);
          this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
          this.loading = false;
          this.cdr.markForCheck();
        }
      );

  }


  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminDepartmentFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });
    
    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.department = null;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDepartments();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {    
    const activeModal = this.modalService.open(
      AdminDepartmentFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });
      
      activeModal.componentInstance.mode = MODE.UPDATE;
      activeModal.componentInstance.department = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDepartments();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminDepartmentFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.department = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getDepartments();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

}
