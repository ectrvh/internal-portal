import { AdminDepartmentModule } from './department.module';

describe('AdminDepartmentModule', () => {
  let adminDepartmentModule: AdminDepartmentModule;

  beforeEach(() => {
    adminDepartmentModule = new AdminDepartmentModule();
  });

  it('should create an instance', () => {
    expect(adminDepartmentModule).toBeTruthy();
  });
});
