import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsCategoriesService, NewsSubcategoriesService } from '@app/shared/services';
import { NewsSubCategory, NewsCategory } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminNewsSubCategoryFormModalComponent } from './form-modal/form-modal.component';
import * as moment from 'moment';
import { NotifierService } from 'angular-notifier';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/operators';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-news-subcategory',
  templateUrl: './news-subcategory.component.html',
  styleUrls: ['./news-subcategory.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsSubCategoryComponent implements OnInit, OnDestroy {

  newsSubcategories: NewsSubCategory[];
  newsCategories: NewsCategory[];
  alive = true;
  loading = false;
  languageChanged: string;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      name: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '50%'
      },
      newsCategoryName: {
        title: this.translate.instant('newsCategory'),
        type: 'text',
        filter: true,
        width: '40%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(private modalService: NgbModal,
    private newsCategoriesService: NewsCategoriesService,
    private newsSubcategoriesService: NewsSubcategoriesService,
    private notifierService: NotifierService,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('News SubCategory successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('News SubCategory successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.name.title = this.translate.instant('name');
      this.smartTableSettings.columns.newsCategoryName.title = this.translate.instant('newsCategory');
      // this.smartTableData.refresh();
      this.getNewsSubcategories();
      this.getNewsCategories();
    });
  }

  ngOnInit() {
    this.getNewsSubcategories();
    this.getNewsCategories();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getNewsSubcategories() {
    this.loading = true;
    this.smartTableData.empty();
    const statusSuccessIcon = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const statusDangerIcon = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    this.newsSubcategoriesService.getAllNewsSubcategories()
      .subscribe(results => {
        this.newsSubcategories = results.map(res => {
          res.statusIcon = (res.enabled) ? statusSuccessIcon : statusDangerIcon;
          res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
          res.newsCategoryName = res.newsCategory.name;
          // console.log(res);
          return res;
        });
        this.smartTableData.load(this.newsSubcategories);
        this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  getNewsCategories() {
    this.newsCategoriesService.getNewsCategoriesForSelect().subscribe(
      newscategory => {
        this.newsCategories = newscategory;
      }
    );
  }
  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminNewsSubCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.newsSubCategory = null;
    activeModal.componentInstance.newsCategories = this.newsCategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNewsSubcategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminNewsSubCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.newsSubCategory = event.data;
    activeModal.componentInstance.newsCategories = this.newsCategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNewsSubcategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    // console.log(event.data, 'ROW DATA');
    const activeModal = this.modalService.open(
      AdminNewsSubCategoryFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.newsSubCategory = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getNewsSubcategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

}




