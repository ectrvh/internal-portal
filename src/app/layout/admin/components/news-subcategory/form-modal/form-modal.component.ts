import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsSubCategory, NewsCategory } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsSubcategoriesService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-news-subcategory-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsSubCategoryFormModalComponent implements OnInit, OnDestroy {

  newsSubCategory: NewsSubCategory;
  newsCategories: NewsCategory[];
  selectedNewsCategoryId: string;

  alive = true;

  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private newsSubcategoriesService: NewsSubcategoriesService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: [''],
      active: [''],
      enabled: [''],
      newsCategoryId: ['', Validators.required],
      newsCategory: ['']
    });


    this.form.patchValue({
      id: '',
      name: '',
      description: '',
      enabled: true,
      active: true,
      newsCategoryId: null,
      newsCategory: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.newsSubCategory) {
      this.form.patchValue({
        id: this.newsSubCategory.id,
        name: this.newsSubCategory.name,
        enabled: this.newsSubCategory.enabled,
        active: this.newsSubCategory.active,
        newsCategoryId: this.newsSubCategory.newsCategoryId,
        newsCategory: this.newsSubCategory.newsCategory
      });

      this.selectedNewsCategoryId = this.newsSubCategory.newsCategoryId;

      this.isUpdate = true;
    } else {
      this.newsSubCategory = new NewsSubCategory;
    }
  }


  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;
    console.log(this.form);

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['newsCategory']);

      this.newsSubcategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      this.newsSubcategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.newsSubCategory.id };

    this.newsSubcategoriesService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
