import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminNewsSubCategoryComponent } from './news-subcategory.component';

const routes: Routes = [
    {
        path: '',
        component: AdminNewsSubCategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminNewsSubCategoryRoutingModule {}
