import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbSelectModule,
  NbToastrModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastService } from '@app/shared/services';
import { AdminNewsSubCategoryFormModalComponent } from './form-modal/form-modal.component';
import { AdminNewsSubCategoryRoutingModule } from './news-subcategory.routes';
import { AdminNewsSubCategoryComponent } from './news-subcategory.component';
import { NgSelectModule } from '@ng-select/ng-select';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminNewsSubCategoryRoutingModule];

const COMPONENTS = [
  AdminNewsSubCategoryComponent,
  AdminNewsSubCategoryFormModalComponent  
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminNewsSubCategoryFormModalComponent    
  ],
  providers: [
    ToastService
  ]
})

export class AdminNewsSubCategoryModule { }
