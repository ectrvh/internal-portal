import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminNewsSubCategoryComponent } from './news-subcategory.component';

describe('AdminNewsSubCategoryComponent', () => {
  let component: AdminNewsSubCategoryComponent;
  let fixture: ComponentFixture<AdminNewsSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNewsSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNewsSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
