import { AdminProjectCategoryModule } from './project-category.module';

describe('AdminProjectCategoryModule', () => {
  let adminProjectCategoryModule: AdminProjectCategoryModule;

  beforeEach(() => {
    adminProjectCategoryModule = new AdminProjectCategoryModule();
  });

  it('should create an instance', () => {
    expect(adminProjectCategoryModule).toBeTruthy();
  });
});
