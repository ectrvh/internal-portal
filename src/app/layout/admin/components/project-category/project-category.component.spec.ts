import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AdminProjectCategoryComponent } from './project-category.component';

describe('AdminProjectCategoryComponent', () => {
  let component: AdminProjectCategoryComponent;
  let fixture: ComponentFixture<AdminProjectCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminProjectCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
