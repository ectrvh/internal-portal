import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminProjectCategoryComponent } from './project-category.component';

const routes: Routes = [
    {
        path: '',
        component: AdminProjectCategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminProjectCategoryRoutingModule {}
