import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbToastrModule,
  NbSelectModule
} from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from '@app/shared/services';
import { AdminProjectCategoryComponent } from './project-category.component';
import { AdminProjectCategoryFormModalComponent } from './form-modal/form-modal.component';
import { AdminProjectCategoryRoutingModule } from './project-category.routes';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule
];

const ROUTE_MODULES = [AdminProjectCategoryRoutingModule];

const COMPONENTS = [
  AdminProjectCategoryComponent,
  AdminProjectCategoryFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminProjectCategoryFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminProjectCategoryModule { }
