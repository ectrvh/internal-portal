import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ProjectCategory } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProjectCategoriesService } from '@app/shared/services';
import { omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-project-category-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminProjectCategoryFormModalComponent implements OnInit, OnDestroy {

  projectCategory: ProjectCategory;
  alive = true;

  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private projectCategoriesService: ProjectCategoriesService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', Validators.required],
      enabled: [''],
      active: ['']
    });


    this.form.patchValue({
      id: '',
      name: '',
      description: '',
      enabled: true,
      active: true
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.projectCategory) {
      this.form.patchValue({
        id: this.projectCategory.id,
        name: this.projectCategory.name,
        description: this.projectCategory.description,
        enabled: this.projectCategory.enabled,
        active: this.projectCategory.active
      });

      this.isUpdate = true;
    } else {
      this.projectCategory = new ProjectCategory;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {

    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['projects']);

      this.projectCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      this.projectCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.projectCategory.id };
    this.projectCategoriesService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
