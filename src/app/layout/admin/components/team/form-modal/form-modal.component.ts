import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Department, Team, User } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TeamService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-team-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminTeamFormModalComponent implements OnInit, OnDestroy {

  team: Team;
  departments: Department[];
  users: User[];
  selectedDepartmentId: string;
  selectedUsersId = [];
  usersLink = [];
  alive = true;

  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private teamService: TeamService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      active: ['', Validators.required],
      enabled: [''],
      departmentId: [null, Validators.required],
      department: [''],
      usersId: ['', Validators.required],
    });

    this.form.patchValue({
      id: '',
      name: '',
      enabled: true,
      active: true,
      departmentId: null,
      department: null,
      usersId: null,
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.team) {// Update
      this.form.patchValue({
        id: this.team.id,
        name: this.team.name,
        enabled: this.team.enabled,
        active: this.team.active,
        departmentId: this.team.departmentId,
        department: this.team.department,
        usersId: this.team.usersLink
      });

      this.selectedDepartmentId = this.team.departmentId;
      for (const i of this.team.usersLink) {
        this.selectedUsersId.push(i.userId);
      }
      this.isUpdate = true;

    } else {// Create
      this.team = new Team;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE      
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['department']);
      for (const i of formData.usersId) {
        this.usersLink.push({ teamId: formData.id, userId: i });
      }
      formData = omit(formData, ['usersId']);
      formData.usersLink = this.usersLink;

      this.teamService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      formData = omit(formData, ['department']);
      for (const i of formData.usersId) {
        this.usersLink.push({ teamId: formData.id, userId: i });
      }
      formData = omit(formData, ['usersId']);
      formData.usersLink = this.usersLink;

      this.teamService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.team.id };
    this.teamService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
