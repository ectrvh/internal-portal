import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbToastrModule
} from '@nebular/theme';
import { ToastService } from '@app/shared/services';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AdminTeamComponent } from './team.component';
import { AdminTeamFormModalComponent } from './form-modal/form-modal.component';
import { AdminTeamRoutingModule } from './team.routes';
import { NgSelectModule } from '@ng-select/ng-select';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminTeamRoutingModule];

const COMPONENTS = [
  AdminTeamComponent,
  AdminTeamFormModalComponent  
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminTeamFormModalComponent    
  ],
  providers: [
    ToastService
  ]
})

export class AdminTeamModule { }
