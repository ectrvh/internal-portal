import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Team, Department, User } from '@app/shared/models';
import { TeamService, DepartmentService, UserService } from '@app/shared/services';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminTeamFormModalComponent } from './form-modal/form-modal.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { takeWhile } from 'rxjs/operators';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminTeamComponent implements OnInit, OnDestroy {

  teams: Team[];
  departments: Department[];
  alive = true;
  loading = false;
  users: User;
  languageChanged: string;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      name: {
        title: this.translate.instant('team'),
        type: 'text',
        filter: true,
        width: '45%'
      },
      departmentName: {
        title: this.translate.instant('department'),
        type: 'text',
        filter: true,
        width: '45%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(
    private modalService: NgbModal,
    private departmentsService: DepartmentService,
    private teamService: TeamService,    
    private userService: UserService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef
  ) {

    this.notifier = this.notifierService;

    this.translate.get('Team successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Team successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.name.title = this.translate.instant('name');
      this.smartTableSettings.columns.departmentName.title = this.translate.instant('department');
      // this.smartTableData.refresh();
      this.getTeams();
      this.getDepartments();
      this.getUsers();
    });
  }

  ngOnInit() {
    this.getTeams();
    this.getDepartments();
    this.getUsers();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getDepartments() {
    this.departmentsService.getDepartmentsForSelect()
      .subscribe(
        departments => {
          this.departments = departments;
        }
      );
  }

  getTeams() {
    this.loading = true;
    this.smartTableData.empty();
    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    this.teamService.getAllTeams()
      .subscribe(
        teams => {
          this.teams = teams
            .map(res => {
              res.statusIcon = (res.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
              res.departmentName = (res.department) ? res.department.name : '';
              return res;
            });

          this.smartTableData.load(this.teams);
          this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
          this.loading = false;
          this.cdr.markForCheck();
        }
      );
  }

  getUsers() {
    this.userService.getUserList({ limit: 300, page: 1 })
      .subscribe(
        users => {
          this.users = users;
          this.cdr.markForCheck();
        }
      );
  }
  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminTeamFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.team = null;
    activeModal.componentInstance.departments = this.departments;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getTeams();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    const activeModal = this.modalService.open(
      AdminTeamFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.team = event.data;
    activeModal.componentInstance.departments = this.departments;
    activeModal.componentInstance.users = this.users;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getTeams();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {    
    const activeModal = this.modalService.open(
      AdminTeamFormModalComponent, 
      { size: 'sm', centered: true, container: 'nb-layout' });

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.team = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getTeams();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

}
