import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminTeamComponent } from './team.component';

const routes: Routes = [
    {
        path: '',
        component: AdminTeamComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminTeamRoutingModule {}
