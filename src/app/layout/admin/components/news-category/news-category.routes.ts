import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminNewsCategoryComponent } from './news-category.component';

const routes: Routes = [
    {
        path: '',
        component: AdminNewsCategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminNewsCategoryRoutingModule {}
