import { AdminNewsCategoryModule } from './news-category.module';

describe('AdminNewsCategoryModule', () => {
  let adminNewsCategoryModule: AdminNewsCategoryModule;

  beforeEach(() => {
    adminNewsCategoryModule = new AdminNewsCategoryModule();
  });

  it('should create an instance', () => {
    expect(adminNewsCategoryModule).toBeTruthy();
  });
});
