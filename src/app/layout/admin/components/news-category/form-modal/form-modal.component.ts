import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsCategory, Department } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsCategoriesService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-news-category-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsCategoryFormModalComponent implements OnInit, OnDestroy {

  newsCategory: NewsCategory;
  alive = true;

  departments: Department;
  form: FormGroup;
  isUpdate: boolean;
  submitted = false;
  selecteddepartmentId: string;

  mode = '';
  modeDelete = MODE.DELETE;
  
  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private newsCategoriesService: NewsCategoriesService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', Validators.required],
      enabled: [''],
      active: [''],
      department: [''],
      departmentId: [''],
    });

    this.form.patchValue({
      id: '',
      name: '',
      description: '',
      enabled: true,
      active: true,
      department: null,
      departmentId: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.newsCategory) {
      this.form.patchValue({
        id: this.newsCategory.id,
        name: this.newsCategory.name,
        description: this.newsCategory.description,
        enabled: this.newsCategory.enabled,
        active: this.newsCategory.active,
        department: this.newsCategory.department,
        departmentId: this.newsCategory.departmentId,
      });
      this.selecteddepartmentId = this.newsCategory.departmentId;
      this.isUpdate = true;
    } else {
      this.newsCategory = new NewsCategory;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {

    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['news']);
      if (formData.departmentId === undefined) {
        formData.departmentId = null;
      }
      this.newsCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    } else { // UPDATE
      if (formData.departmentId === undefined) {
        formData.departmentId = null;
      }
      this.newsCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.newsCategory.id };

    this.newsCategoriesService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }
  
  
  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
