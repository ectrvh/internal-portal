import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsSubCategory, NewsCategory, News } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsService } from '@app/shared/services';
import { map, omit } from 'lodash';
import * as moment from 'moment';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-news-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsFormModalComponent implements OnInit, OnDestroy {

  news: News;
  newsSubcategories: NewsSubCategory[];
  newsCategories: NewsCategory[];
  selectedNewsSubcategoryId: string;
  newsDate = new Date();
  alive = true;
  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;
    
  now = new Date();

  configEditor = {
    editable: true,
    spellcheck: true,
    height: '800',
    minHeight: '500',
    // width: string;
    // minWidth: string;
    // translate: string;
    // enableToolbar: boolean;
    showToolbar: true,
    // placeholder: string;
    // imageEndPoint: string;
    // toolbar: string[][];
  };


  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private newsService: NewsService) {

    this.form = this.formBuilder.group({
      id: [''],
      title: ['', [Validators.required, Validators.maxLength(150)]],
      date: ['', Validators.required],
      // link: ['', Validators.required],
      body: ['', [Validators.required, Validators.maxLength(4000)]],
      active: ['', Validators.required],
      enabled: [''],
      showInDashboard: [''],
      newsSubcategoryId: ['', Validators.required],
      // newsSubCategory: ['', Validators.required]
    });

    this.form.patchValue({
      id: '',
      title: '',
      date: this.now,
      body: '',
      enabled: true,
      showInDashboard: false,
      active: true,
      newsSubcategoryId: null,
      // newsSubCategory: null
    });
    this.isUpdate = false;
  }

  ngOnInit() {

    if (this.news) {
      this.isUpdate = true;

      if (this.news.date === '' || this.news.date === 'Invalid date') {
        this.news.date = this.now.toString();
      }

      this.form.patchValue({
        id: this.news.id,
        title: this.news.title,
        date: new Date(this.news.date),
        enabled: this.news.enabled,
        showInDashboard: this.news.showInDashboard,
        active: this.news.active,
        newsSubcategoryId: this.news.newsSubcategoryId,
        newsSubcategory: this.news.newsSubcategories
      });

      this.selectedNewsSubcategoryId = this.news.newsSubcategoryId;
    } else {

      this.news = new News;
      this.news.date = this.now.toString();
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();
    // console.log(formData, 'formData');
    if (formData) {
      let date = (this.news && this.news.date) ? this.news.date : new Date();

      if (formData.date) {
        date = moment(formData.date).format('YYYY-MM-DD');
      }
      formData.date = date;
      // if (formData.usersLink) {
      //   formData.usersLink = formData.usersLink
      //     .map((res: any) => {
      //       return { userId: res, projectId: formData.id };
      //     });
      // }
    }
    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['newsCategory']);
      formData = omit(formData, ['newsSubcategory']);
      this.newsService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });          
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });          
        }
      );
    } else { // UPDATE
      this.newsService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });          
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });          
        }
      );
    }
  }

  delete(event) {    
    const payload = { id: this.news.id };

    this.newsService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
