import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NewsService, ToastService, NewsSubcategoriesService } from '@app/shared/services';
import { News, NewsCategory, NewsSubCategory } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminNewsFormModalComponent } from './form-modal/form-modal.component';
import * as moment from 'moment';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { takeWhile } from 'rxjs/operators';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminNewsComponent implements OnInit, OnDestroy {

  news: News[];
  newsSubcategories: NewsSubCategory[];
  newsCategories: NewsCategory[];
  languageChanged: string;
  alive = true;
  loading = false;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      showInDashboardIcon: {
        title: this.translate.instant('showInDashboard'),
        type: 'html',
        width: '5%',
        filter: false
      },
      title: {
        title: this.translate.instant('title'),
        type: 'text',
        filter: true,
        width: '40%'
      },
      newsSubCategoryName: {
        title: this.translate.instant('newsSubcategory'),
        type: 'text',
        filter: true,
        width: '40%'
      },
      dateFormatted: {
        title: this.translate.instant('date'),
        type: 'text',
        filter: true,
        width: '10%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(
    private modalService: NgbModal,
    private newsService: NewsService,
    private newsSubcategoriesService: NewsSubcategoriesService,    
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('News successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('News successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.showInDashboardIcon.title = this.translate.instant('showInDashboard');
      this.smartTableSettings.columns.title.title = this.translate.instant('title');
      this.smartTableSettings.columns.newsSubCategoryName.title = this.translate.instant('newsSubcategory');
      this.smartTableSettings.columns.dateFormatted.title = this.translate.instant('date');
      // this.smartTableData.refresh();
      this.getNews();
      this.getNewsSubCategory();
    });
  }

  ngOnInit() {
    this.getNews();
    this.getNewsSubCategory();
    // this.getNewsCategory();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getNews() {
    this.loading = true;
    this.smartTableData.empty();
    const statusSuccessIcon = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const statusDangerIcon = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';
    const showInDasboadWarningIcon = '<i class="fas fa-star text-warning display-block text-center"></i>';

    this.newsService.getAllNews().subscribe(
      results => {
        this.news = results.map(res => {
          res.statusIcon = (res.enabled) ? statusSuccessIcon : statusDangerIcon;
          res.showInDashboardIcon = (res.showInDashboard) ? showInDasboadWarningIcon : ' ';
          res.newsSubCategoryName = res.newsSubcategories.name;
          // res.date = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
          res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
          return res;
        });
        this.smartTableData.load(this.news);
        this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
        this.loading = false;
        this.cdr.markForCheck();
      }
    );
  }

  getNewsSubCategory() {
    this.newsSubcategoriesService.getNewsSubcategoriesForSelect()
      .subscribe(
        newsSubcategories => {
          this.newsSubcategories = newsSubcategories;
        }
      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
    const activeModal = this.modalService.open(
      AdminNewsFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.news = null;
    activeModal.componentInstance.newsSubcategories = this.newsSubcategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNews();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onEdit(event) {
    const activeModal = this.modalService.open(
      AdminNewsFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.news = event.data;
    activeModal.componentInstance.newsSubcategories = this.newsSubcategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getNews();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }

  onDelete(event) {
    const activeModal = this.modalService.open(
      AdminNewsFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' });

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.news = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getNews();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }
}


