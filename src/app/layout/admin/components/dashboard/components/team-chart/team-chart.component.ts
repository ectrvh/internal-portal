import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { ModelCount } from '@app/shared/models';

@Component({
  selector: 'app-admin-dashboard-team-chart',
  templateUrl: './team-chart.component.html',
  styleUrls: ['./team-chart.component.scss']
})

// Demo: https://swimlane.github.io/ngx-charts/#/ngx-charts/bar-vertical-2d
// Documentation: https://swimlane.gitbook.io/ngx-charts


export class AdminDashboardTeamChartComponent implements OnInit, OnDestroy {

  @Input() model: ModelCount = new ModelCount;

  // single = [
  //   {
  //     name: 'Bucuresti',
  //     value: 85,
  //   },
  //   {
  //     name: 'Chisinau',
  //     value: 40,
  //   },
  //   {
  //     name: 'Cluj',
  //     value: 66,
  //   },
  // ];

  colorScheme: any;
  themeSubscription: any;
  alive = true;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }

  ngOnInit() { }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.alive = false;
  }

}
