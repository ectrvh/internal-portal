import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { ModelCount } from '@app/shared/models';

@Component({
  selector: 'app-admin-dashboard-project-chart',
  templateUrl: './project-chart.component.html',
  styleUrls: ['./project-chart.component.scss']
})

// Demo: https://swimlane.github.io/ngx-charts/#/ngx-charts/bar-vertical-2d
// Documentation: https://swimlane.gitbook.io/ngx-charts

export class AdminDashboardProjectChartComponent implements OnInit, OnDestroy {

  @Input() model: ModelCount = new ModelCount;

  alive = true;

  showLegend = true;
  xAxisLabel = 'Projects';
  yAxisLabel = 'Employees';
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  colorScheme: any;
  themeSubscription: any;

  constructor(private theme: NbThemeService) {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.colorScheme = {
        domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
      };
    });
  }

  ngOnInit() { }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
    this.alive = false;
  }
}


