import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ModelCount } from '@app/shared/models';
import { DefaultService } from '@app/shared/services/default.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDashboardComponent implements OnInit, OnDestroy {

  alive = true;
  model: ModelCount = new ModelCount;
  loading = false;

  constructor(
    private defaultService: DefaultService,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.getModelCount();
  }

  ngOnDestroy() {
    this.alive = false;
    // this.cdr.detach();
  }

  getModelCount() {
    this.loading = true;
    this.defaultService.getModelCount()
      .subscribe(
        modelCount => {
          this.model = modelCount;
          this.loading = false;
          this.cdr.detectChanges();
        }
      );
  }

}


