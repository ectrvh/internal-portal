import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbButtonModule,
  NbCheckboxModule,
  NbToastrModule
} from '@nebular/theme';
import { ToastService } from '@app/shared/services';
import { AdminDashboardRoutingModule } from './dashboard.routes';
import { AdminDashboardComponent } from './dashboard.component';
import { AdminStatusCardComponent } from './status-card/status-card.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-chartjs';
import { AdminDashboardEmployeeChartComponent } from './components/employee-chart/employee-chart.component';
import { AdminDashboardProjectChartComponent } from './components/project-chart/project-chart.component';
import { AdminDashboardTeamChartComponent } from './components/team-chart/team-chart.component';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NgxChartsModule,
  ChartModule,
  NbToastrModule.forRoot(),
  NgbModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule
];

const ROUTE_MODULES = [AdminDashboardRoutingModule];

const COMPONENTS = [
  AdminDashboardComponent,
  AdminStatusCardComponent,
  AdminDashboardEmployeeChartComponent,
  AdminDashboardProjectChartComponent,
  AdminDashboardTeamChartComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
  ],
  providers: [
    ToastService
  ]
})

export class AdminDashboardModule { }
