import { Component, ChangeDetectionStrategy, OnInit, OnDestroy, Input } from '@angular/core';
import { ModelCount } from '@app/shared/models';

@Component({
  selector: 'app-admin-status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminStatusCardComponent implements OnInit, OnDestroy {

  @Input() model: ModelCount = new ModelCount;
  @Input() loading = false;

  alive = true;

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.alive = false;
  }

}


