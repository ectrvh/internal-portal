import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild } from '@angular/core';
import { User, Function, JobType, Team, FileUpload } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { IwFileUploadComponent } from '@app/shared/modules/iw-file-upload/iw-file-upload.component';
import { RESPONSE } from '@app/shared/constants/response';
import { forkJoin } from 'rxjs';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-employee-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminEmployeeFormModalComponent implements OnInit, OnDestroy {

  @ViewChild(IwFileUploadComponent) iwFileUpload: IwFileUploadComponent;

  user: User;
  alive = true;

  functions: Function[];
  selectedFunctionId: string;

  jobTypes: JobType[];
  selectedJobTypeId: string;

  teams: Team[];
  selectedTeamsId = [];

  form: FormGroup;
  isUpdate: boolean;

  payloadUserTeams = [];
  loading = false;

  fileUploads: FileUpload;

  mode = '';
  modeDelete = MODE.DELETE;

  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private userService: UserService) {

    this.form = this.formBuilder.group({
      id: [''],
      jobId: [null, Validators.required],
      jobType: [null, Validators.required],
      functionId: [null, Validators.required],
      function: [null, Validators.required],
      teamsId: [null, Validators.required]
    });

    this.form.patchValue({
      id: '',
      jobId: null,
      jobType: null,
      functionId: null,
      function: null,
      teamsId: null
    });

    this.isUpdate = true;
  }

  ngOnInit() {
    this.loading = true;
    // console.log(this.teams, 'echipele');
    this.userService.getUsers({ userId: this.user.id })
      .subscribe(
        user => {
          if (user) {
            // console.log(user);
            if (user.fileUploads[0]) {
              this.fileUploads = user.fileUploads[0];
            } else {
              this.fileUploads = null;
            }
            this.form.patchValue({
              id: this.user.id,
              jobId: this.user.jobTypeId,
              jobType: this.user.jobType,
              functionId: this.user.functionId,
              function: this.user.function,
              teamsId: this.user.teamsLink
            });
            if (this.user.teamsLink && this.user.teamsLink.length > 0) {
              this.selectedTeamsId = [];
              this.user.teamsLink.forEach(resource => {
                this.selectedTeamsId.push(resource.teamId);
              });
            }
            this.selectedFunctionId = (this.user.functionId !== '00000000-0000-0000-0000-000000000000') ? this.user.functionId : null;
            this.selectedJobTypeId = (this.user.jobTypeId !== '00000000-0000-0000-0000-000000000000') ? this.user.jobTypeId : null;
            this.isUpdate = true;
          }
          this.loading = false;
        }
      );
  }

  saveForm(details) {
    let formData = this.form.getRawValue();
    if (this.isUpdate) { // Update
      formData = omit(formData, ['function']);
      formData = omit(formData, ['jobType']);
      // recover teamId and push it to payloadUserTeams
      if (formData.teamsId != null) {
        for (const data of formData.teamsId) {
          this.payloadUserTeams.push({ teamId: data, userId: formData.id });
        }
      }
      const saveUserTeams = this.userService.saveUserTeams(this.payloadUserTeams, formData.id);
      formData = omit(formData, ['teamsId']);
      const saveUserJobFunction = this.userService.save(formData);

      forkJoin([saveUserJobFunction, saveUserTeams]).subscribe(
        res => {
          this.iwFileUpload.uploadFile(formData);
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });
        }
      );
    }

  }

  delete(event) {
    const payload = { id: this.user.id };
    this.userService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

  fileUploadCompleted(event) {
    if (event && event.response !== RESPONSE.ERROR) {
      this.closeModal({ response: event.response, message: event.message });
    }
  }

}
