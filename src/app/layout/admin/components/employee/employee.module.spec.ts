import { AdminEmployeeModule } from './employee.module';

describe('AdminEmployeeModule', () => {
  let adminEmployeeModule: AdminEmployeeModule;

  beforeEach(() => {
    adminEmployeeModule = new AdminEmployeeModule();
  });

  it('should create an instance', () => {
    expect(adminEmployeeModule).toBeTruthy();
  });
});
