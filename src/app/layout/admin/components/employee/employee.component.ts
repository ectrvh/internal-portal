import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Team, User, JobType, Function } from '@app/shared/models';
import { UserService, TeamService, FunctionService, JobTypeService } from '@app/shared/services';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminEmployeeFormModalComponent } from './form-modal/form-modal.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeWhile } from 'rxjs/operators';
import { RESPONSE } from '@app/shared/constants/response';
import { forkJoin } from 'rxjs';
import { find } from 'lodash';
import { UserRole } from '@app/shared/models/user-role';
import { NotifierService } from 'angular-notifier';
import { MODE } from '@app/shared/constants/mode';

@Component({
  selector: 'app-admin-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminEmployeeComponent implements OnInit, OnDestroy {

  users: User[];
  functions: Function[];
  jobTypes: JobType[];
  teams: Team[];
  userRoles: UserRole[];
  alive = true;
  loading = false;
  languageChanged: string;

  syncUsersLoading = false;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: false,
      position: 'right'
    },
    columns: {
      fullName: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '25%'
      },
      email: {
        title: this.translate.instant('email'),
        type: 'text',
        filter: true,
        width: '30%'
      },
      functionName: {
        title: this.translate.instant('function'),
        type: 'text',
        filter: true,
        width: '15%'
      },
      jobTypeName: {
        title: this.translate.instant('jobType'),
        type: 'text',
        filter: true,
        width: '15%'
      },
      roleName: {
        title: this.translate.instant('role'),
        type: 'text',
        filter: true,
        width: '10%'
      },
      location: {
        title: this.translate.instant('location'),
        type: 'text',
        filter: true,
        width: '10%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(private modalService: NgbModal,
    private userService: UserService,
    private functionService: FunctionService,
    private jobTypeService: JobTypeService,
    private teamService: TeamService,    
    private notifierService: NotifierService,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('User successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('User successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.fullName.title = this.translate.instant('name');
      this.smartTableSettings.columns.email.title = this.translate.instant('email');
      this.smartTableSettings.columns.functionName.title = this.translate.instant('function');
      this.smartTableSettings.columns.jobTypeName.title = this.translate.instant('jobType');
      this.smartTableSettings.columns.location.title = this.translate.instant('location');
      this.smartTableSettings.columns.roleName.title = this.translate.instant('role');
      // this.smartTableData.refresh();
      this.initData();
    });

  }

  ngOnInit() {
    this.initData();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  initData() {
    this.loading = true;
    const users = this.userService.getUserList({ limit: 300, page: 1 });
    const teams = this.teamService.getAllTeamsForSelect();
    const jobTypes = this.jobTypeService.getJobTypesForSelect();
    const functions = this.functionService.getFunctionsForSelect();
    const userRoles = this.userService.getUserRoles();

    forkJoin([users, teams, jobTypes, functions, userRoles])
      .subscribe(res => {
        this.users = res[0];
        this.teams = res[1];
        this.jobTypes = res[2];
        this.functions = res[3];
        this.userRoles = res[4];
        this.formatUserData(this.users);
        this.cdr.markForCheck();
        this.loading = false;
      });
  }

  formatUserData(users) {
    this.smartTableData.empty();

    this.loading = true;

    this.users = users.map(user => {
      const userFunction = find(this.functions, result => result.id === user.functionId);
      if (userFunction) {
        user.functionName = userFunction.name;
      }

      const userJobType = find(this.jobTypes, result => result.id === user.jobTypeId);
      if (userJobType) {
        user.jobTypeName = userJobType.name;
      }

      // TODO :: check !!!!
      const userRole = find(this.userRoles, result => result.id === user.userRoleId);
      if (userRole) {
        user.roleName = userRole.name;
      }

      return user;
    });

    this.smartTableData.load(this.users);
    this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
    this.loading = false;
    this.cdr.markForCheck();

  }

  getUsers() {
    this.userService.getUserList({ limit: 300, page: 1 })
      .subscribe(
        users => {
          this.formatUserData(users);
        }
      );
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }

  onCreate() {
  }

  onEdit(event) {
    const activeModal = this.modalService.open(
      AdminEmployeeFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false });

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.user = event.data;
    activeModal.componentInstance.teams = this.teams;
    activeModal.componentInstance.functions = this.functions;
    activeModal.componentInstance.jobTypes = this.jobTypes;
    activeModal.componentInstance.userRoles = this.userRoles;

    activeModal.result.then((res) => {
      // on close

      if (res && res.response) {
        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getUsers();
        }

      }

    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
  }

  syncUsers() {
    this.syncUsersLoading = true;
    this.userService.populateUsers()
      .subscribe(
        users => {
          this.syncUsersLoading = false;
          this.initData();
        }
      );
  }
}

