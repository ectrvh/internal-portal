import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbToastrModule,
  NbSelectModule,
  NbAlertModule
} from '@nebular/theme';
import { ToastService } from '@app/shared/services';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminEmployeeRoutingModule } from './employee.routes';
import { AdminEmployeeComponent } from './employee.component';
import { AdminEmployeeFormModalComponent } from './form-modal/form-modal.component';
import { IwFileUploadModule } from '@app/shared/modules/iw-file-upload/iw-file-upload.module';
import { NgSelectModule } from '@ng-select/ng-select';


const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbSpinnerModule,
  NbCheckboxModule,
  NbSelectModule,
  NbAlertModule,
  NgbModule.forRoot(),
  NbToastrModule.forRoot()
];

const EXTRA_MODULES = [
  Ng2SmartTableModule,
  IwFileUploadModule,
  NgSelectModule
];

const ROUTE_MODULES = [AdminEmployeeRoutingModule];

const COMPONENTS = [
  AdminEmployeeComponent,
  AdminEmployeeFormModalComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    AdminEmployeeFormModalComponent
  ],
  providers: [
    ToastService
  ]
})

export class AdminEmployeeModule { }
