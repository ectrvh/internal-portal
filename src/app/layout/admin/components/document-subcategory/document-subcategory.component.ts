import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DocumentSubCategoriesService, DocumentCategoriesService } from '@app/shared/services';
import { DocumentCategory, DocumentSubCategory } from '@app/shared/models';
import { LocalDataSource } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminDocumentSubCategoryFormModalComponent } from './form-modal/form-modal.component';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { takeWhile } from 'rxjs/operators';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-document-subcategory',
  templateUrl: './document-subcategory.component.html',
  styleUrls: ['./document-subcategory.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentSubCategoryComponent implements OnInit, OnDestroy {

  documentsSubCategories: DocumentSubCategory[];
  documentCategories: DocumentCategory[];
  alive = true;
  loading = false;
  languageChanged: string;

  notifier: NotifierService;

  textSaveSuccess = '';
  textDeleteSuccess = '';

  smartTableSettings = {
    mode: 'external',
    hideSubHeader: false,
    pager: {
      display: true,
      perPage: 10
    },
    actions: {
      columnTitle: this.translate.instant('actions'),
      editable: false,
      add: false,
      edit: true,
      delete: true,
      position: 'right'
    },
    columns: {
      statusIcon: {
        title: this.translate.instant('active'),
        type: 'html',
        width: '5%',
        filter: false
      },
      name: {
        title: this.translate.instant('name'),
        type: 'text',
        filter: true,
        width: '40%'
      },
      documentCategoryName: {
        title: this.translate.instant('documentCategory'),
        type: 'text',
        filter: true,
        width: '40%'
      }
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit text-primary"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash text-danger"></i>',
      confirmDelete: true,
    }
  };

  smartTableData: LocalDataSource = new LocalDataSource();

  constructor(
    private modalService: NgbModal,
    private documentSubCategoriesService: DocumentSubCategoriesService,
    private documentCategoriesService: DocumentCategoriesService,
    public translate: TranslateService,
    private notifierService: NotifierService,
    private cdr: ChangeDetectorRef) {

    this.notifier = this.notifierService;

    this.translate.get('Document SubCategory successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);

    this.translate.get('Document SubCategory successfuly deleted').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textDeleteSuccess = res);

    this.languageChanged = this.translate.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.languageChanged = event.lang;
      // Update your settings here with the same data
      this.smartTableSettings.actions.columnTitle = this.translate.instant('actions');
      this.smartTableSettings.columns.statusIcon.title = this.translate.instant('active');
      this.smartTableSettings.columns.name.title = this.translate.instant('name');
      this.smartTableSettings.columns.documentCategoryName.title = this.translate.instant('documentCategory');
      // this.smartTableData.refresh();
      this.getDocumentsSubCategories();
      this.getDocumentsCategoriesForSelect();
    });

  }

  ngOnInit() {
    this.getDocumentsSubCategories();
    this.getDocumentsCategoriesForSelect();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getDocumentsSubCategories() {
    this.loading = true;
    this.smartTableData.empty();
    const successDisplayHtml = '<i class="ion-checkmark-round font-w-bold text-success text-center display-block"></i>';
    const dangerDisplayHtml = '<i class="ion-close-round font-w-bold text-danger text-center display-block"></i>';

    // const allDocumentsCategories = this.documentCategoriesService.getDocumentsCategoriesForSelect();
    // const allDocumentsSubCategories = this.documentSubCategoriesService.getAllDocumentSubCategories();

    // forkJoin([allDocumentsCategories, allDocumentsSubCategories]).subscribe(results => {
    //   console.log(results[0]);
    //   this.documentCategories = results[0];
    //   this.documentsSubCategories = results[1];
    //   this.documentsSubCategories = results[1].map(res => {
    //     const documentCategory = this.documentCategories.find(result => result.id === res.documentCategoryId);
    //     if (documentCategory) {
    //       res.documentCategory = documentCategory;
    //       res.documentCategoryName = documentCategory.name;
    //     }
    //     res.statusIcon = (res.enabled) ? successDisplayHtml : dangerDisplayHtml;
    //     // res.date = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
    //     res.dateFormatted = (res.date) ? moment(res.date).format('DD.MM.YYYY') : '';
    //     // console.log(res);
    //     return res;
    //   });
    //   this.smartTableData.load(this.documentsSubCategories);
    //   this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
    //   this.loading = false;
    //   this.cdr.markForCheck();
    // });

    this.documentSubCategoriesService.getAllDocumentSubCategories()
      .subscribe(
        documentsSubCategories => {
          this.documentsSubCategories = documentsSubCategories
            .map(res => {
              res.statusIcon = (res.enabled === true) ? successDisplayHtml : dangerDisplayHtml;
              res.documentCategoryName = (res.documentCategory) ? res.documentCategory.name : '';
              return res;
            });
          this.smartTableData.load(this.documentsSubCategories);
          this.smartTableData.setSort([{ field: 'name', direction: 'asc' }]);
          this.loading = false;
          this.cdr.markForCheck();
        }

      );
  }

  getDocumentsCategoriesForSelect() {
    this.documentCategoriesService.getDocumentsCategoriesForSelect().subscribe(res => {
      this.documentCategories = res;
      this.cdr.markForCheck();
    });
  }

  // SMART TABLE ACTIONS
  onRowSelect(event) {
    // console.log(event.data, 'ROW DATA');
  }


  onCreate() {
    const activeModal = this.modalService.open(
      AdminDocumentSubCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.NEW;
    activeModal.componentInstance.documentSubCategory = null;
    activeModal.componentInstance.documentCategories = this.documentCategories;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDocumentsSubCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });
  }


  onEdit(event) {
    const activeModal = this.modalService.open(
      AdminDocumentSubCategoryFormModalComponent,
      { size: 'lg', centered: true, container: 'nb-layout', backdrop: 'static', keyboard: false }
    );

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.documentSubCategory = event.data;
    activeModal.componentInstance.documentCategories = this.documentCategories;
    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.getDocumentsSubCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }

  onDelete(event) {
    const activeModal = this.modalService.open(
      AdminDocumentSubCategoryFormModalComponent,
      { size: 'sm', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.DELETE;
    activeModal.componentInstance.documentSubCategory = event.data;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textDeleteSuccess);
          this.getDocumentsSubCategories();
        }

      }
    }, (reason) => {
      // on dismiss
    });

  }


}



