import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DocumentSubCategory, DocumentCategory } from '@app/shared/models';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentSubCategoriesService } from '@app/shared/services';
import { map, omit } from 'lodash';
import { MODE } from '@app/shared/constants/mode';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-admin-document-subcategory-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AdminDocumentSubCategoryFormModalComponent implements OnInit, OnDestroy {

  documentSubCategory: DocumentSubCategory;
  documentCategories: DocumentCategory[];
  selectedDocumentCategoryId: string;

  alive = true;

  form: FormGroup;
  isUpdate: boolean;
  submitted = false;

  mode = '';
  modeDelete = MODE.DELETE;
  
  constructor(private formBuilder: FormBuilder,
    private activeModal: NgbActiveModal,
    private documentSubCategoriesService: DocumentSubCategoriesService) {

    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: [''],
      active: [''],
      enabled: [''],
      documentCategoryId: ['', Validators.required],
      documentCategory: ['']
    });


    this.form.patchValue({
      id: '',
      name: '',
      description: '',
      enabled: true,
      active: true,
      documentCategoryId: null,
      documentCategory: null
    });

    this.isUpdate = false;
  }

  ngOnInit() {
    if (this.documentSubCategory) {
      this.form.patchValue({
        id: this.documentSubCategory.id,
        name: this.documentSubCategory.name,
        enabled: this.documentSubCategory.enabled,
        active: this.documentSubCategory.active,
        documentCategoryId: this.documentSubCategory.documentCategoryId,
        documentCategory: this.documentSubCategory.documentCategory
      });

      this.selectedDocumentCategoryId = this.documentSubCategory.documentCategoryId;
      // console.log(this.documentSubCategory);

      this.isUpdate = true;
    } else {
      this.documentSubCategory = new DocumentSubCategory;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    let formData = this.form.getRawValue();

    if (!this.isUpdate) { // CREATE
      formData = omit(formData, ['id']);  // Just a patch to remove key=id
      formData = omit(formData, ['documentCategory']);

      this.documentSubCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });          
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });           
        }
      );
    } else { // UPDATE
      this.documentSubCategoriesService.save(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });          
        }
      );

    }

  }

  delete(event) {
    const payload = { id: this.documentSubCategory.id };

    this.documentSubCategoriesService.delete(payload).subscribe(
      res => {
        this.closeModal({ response: RESPONSE.SUCCESS, message: res });        
      },
      err => {
        this.closeModal({ response: RESPONSE.ERROR, message: err });        
      }
    );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  closeModal(result?) {
    this.activeModal.close(result);
  }

}
