import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {  AdminDocumentSubCategoryComponent } from './document-subcategory.component';

describe('AdminDocumentSubCategoryComponent', () => {
  let component: AdminDocumentSubCategoryComponent;
  let fixture: ComponentFixture<AdminDocumentSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDocumentSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDocumentSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
