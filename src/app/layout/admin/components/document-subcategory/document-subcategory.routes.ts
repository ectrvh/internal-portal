import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDocumentSubCategoryComponent } from './document-subcategory.component';

const routes: Routes = [
    {
        path: '',
        component: AdminDocumentSubCategoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdminDocumentSubCategoryRoutingModule {}
