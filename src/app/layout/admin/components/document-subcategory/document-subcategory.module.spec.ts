import { AdminDocumentSubCategoryModule } from './document-subcategory.module';

describe('AdminDocumentSubCategoryModule', () => {
  let adminDocumentSubCategoryModule: AdminDocumentSubCategoryModule;

  beforeEach(() => {
    adminDocumentSubCategoryModule = new AdminDocumentSubCategoryModule();
  });

  it('should create an instance', () => {
    expect(adminDocumentSubCategoryModule).toBeTruthy();
  });
});
