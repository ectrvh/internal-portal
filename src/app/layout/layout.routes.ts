import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { AuthGuard } from '@app/shared';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ROLE } from '@app/shared/constants/roles';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'prefix'
            },
            {
                path: 'dashboard',
                loadChildren: './main/dashboard/dashboard.module#DashboardModule',
                pathMatch: 'full'
            },
            {
                path: 'employees',
                loadChildren: './main/employees/employees.module#EmployeesModule',
                pathMatch: 'full'
            },
            {
                path: 'projects',
                loadChildren: './main/projects/projects.module#ProjectsModule',
                pathMatch: 'full'
            },
            {
                path: 'documents',
                loadChildren: './main/documents/documents.module#DocumentsModule',
                pathMatch: 'full'
            },
            {
                path: 'news',
                loadChildren: './main/news/news.module#NewsModule',
                pathMatch: 'full'
            },
            {
                path: 'newsletters',
                loadChildren: './main/newsletters/newsletters.module#NewslettersModule',
                pathMatch: 'full'
            },
            {
                path: 'calendar',
                loadChildren: './main/calendar/calendar.module#CalendarModule',
                pathMatch: 'full'
            },
            {
                path: 'user-profile',
                loadChildren: './main/user-profile/user-profile.module#UserProfileModule',
                pathMatch: 'full'
            },
            {
                path: 'admin',
                loadChildren: './admin/container/admin.module#AdminModule',
                pathMatch: 'prefix',
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                        only: [ROLE.SUPERADMIN],
                        redirectTo: 'dashboard'
                    }
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
