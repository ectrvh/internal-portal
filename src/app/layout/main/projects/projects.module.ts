import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsRoutingModule } from './projects.routes';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectsComponent } from './projects.component';
import { ProjectService } from '@app/shared/services/project.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IwProjectPreviewModule } from '@app/shared/modules/iw-project-preview/iw-project-preview.module';
import { IwProjectPreviewComponent } from '@app/shared/modules/iw-project-preview/iw-project-preview.component';
import { SharedPipesModule } from '@app/shared';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
    NbActionsModule,
    NbUserModule,
    NbContextMenuModule,
    NbCardModule,
    NbRouteTabsetModule,
    NbSpinnerModule,
    NbInputModule,
    NbButtonModule
} from '@nebular/theme';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
    NbActionsModule,
    NbUserModule,
    NbContextMenuModule,
    NgbModule,
    NbCardModule,
    NbRouteTabsetModule,
    NbSpinnerModule,
    NbInputModule,
    NbButtonModule,
    NgbModule.forRoot()
];

const EXTRA_MODULES = [IwProjectPreviewModule, InfiniteScrollModule, SharedPipesModule];

const ROUTE_MODULES = [ProjectsRoutingModule];

const COMPONENTS = [ProjectsComponent];

@NgModule({
    imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
    declarations: [...COMPONENTS],
    entryComponents: [
        IwProjectPreviewComponent
    ],
    providers: [
        ProjectService
    ]
})

export class ProjectsModule { }
