import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { MenuService, ProjectService, DASHBOARD_MENU_ITEMS, ProjectCategoriesService, FileUploadService } from '@app/shared/services';
import { Project, ProjectCategory } from '@app/shared/models';
import { first } from 'rxjs/operators';
import { toMenuItems } from '@shared/util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { takeWhile } from 'rxjs/operators';
import { LayoutModel } from '@app/layout/layout-model';
import { NbMenuService } from '@nebular/theme';
import { NbMenuBag } from '@nebular/theme/components/menu/menu.service';
import { IwProjectPreviewComponent } from '@app/shared/modules/iw-project-preview/iw-project-preview.component';
import { forkJoin } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ProjectsComponent implements OnInit, OnDestroy {

  projects: Project[] = [];
  projectCategories: ProjectCategory[] = [];

  searchForm: FormGroup;
  selectedMenu: NbMenuBag;

  payload = {
    page: 1,
    limit: 12,
    categoryId: '',
    searchName: ''
  };

  currentPageName = '';
  alive = true;
  loading = false;
  allProjects = 'allProjects';

  headerHeight = 100;
  windowHeight: any;
  windowWidth: any;

  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    if (this.projects && this.projects.length < 4) {
      this.windowHeight = 'inherit';
    } else {
      this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    }
    this.windowWidth = window.innerWidth;
  }

  constructor(public layoutModel: LayoutModel,
    private projectService: ProjectService,
    private projectCategoriesService: ProjectCategoriesService,
    private modalService: NgbModal,
    public menuService: MenuService,
    private nbMenuService: NbMenuService,
    private fileUploadService: FileUploadService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

    this.searchForm = this.formBuilder.group({
      searchText: ['', Validators.required]
    });

    this.searchForm.patchValue({ searchText: '' });

    this.getScreenSize();

    this.translate.get('allProjects').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      this.allProjects = res;
      this.currentPageName = res;
    }
    );

    this.nbMenuService.onItemClick()
      .pipe(
        takeWhile(() => this.alive)
      )
      .subscribe(res => {
        if (this.selectedMenu && this.selectedMenu.item && this.selectedMenu.item.data && this.selectedMenu.item.data === res.item.data) {
          return;
        }
        this.selectedMenu = res;
        if (this.selectedMenu && this.selectedMenu.item.data) {
          this.updateProjects(this.selectedMenu.item);
        }
      });

  }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.initPage();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  initPage() {
    this.loading = true;
    const projectCategorys = this.projectCategoriesService.getProjectsCategories();
    const projects = this.projectService.getProjects(this.payload);

    forkJoin([projectCategorys, projects])
      .subscribe(results => {
        this.projectCategories = results[0];
        this.toSidebarMenuItems(this.projectCategories);
        this.updateProjectCategory(results[1]);
        this.updateProjectImage(results[1]);
        this.loading = false;
      });
  }

  getProjects() {
    this.loading = true;
    this.projectService.getProjects(this.payload)
      .subscribe(projects => {
        this.updateProjectCategory(projects);
        this.updateProjectImage(projects);
      });
  }

  updateProjectCategory(projects) {
    if (projects) {
      projects.map(project => {
        if (project.details != null) {
          // if project details has over 300 characters add dots
          project.detailsLimited =
            (project.details.length >= 300)
              ?
              project.details.replace(/<[^>]+>/g, '').slice(0, 300) + ' ...'
              :
              project.details;
        }
        project.image = 'assets/images/logo-hexagone-iw-v3.jpg';
        this.projects.push(project);
        // console.log(project);
      });
    }
    if (this.projects && this.projects.length < 4) {
      this.windowHeight = 'inherit';
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  toSidebarMenuItems(projectCategories) {
    const sidebarMenuItems = toMenuItems(projectCategories, 'projects', true, true, this.allProjects);
    this.layoutModel.updateMenuItems(sidebarMenuItems);
  }

  updateProjectImage(projects) {
    if (projects) {
      projects.map(project => {
        if (project.fileUploads && project.fileUploads.length > 0) {
          const payload = { id: project.id };
          this.fileUploadService.getFile(payload).subscribe(
            data => {
              this.getProjectImage(project, data);
            },
            error => {
            }
          );
        }
      });
    }
    this.loading = false;
    this.cdr.markForCheck();
  }


  showProjectDetails(project: Project, event?: Event) {
    const activeModal = this.modalService.open(
      IwProjectPreviewComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );
    activeModal.componentInstance.project = project;
    activeModal.componentInstance.projectCategories = this.projectCategories;
  }

  updateProjects(selectedMenuItem) {
    this.currentPageName = selectedMenuItem.title;

    this.payload.searchName = '';
    this.payload.page = 1;
    this.payload.categoryId = (selectedMenuItem.data.id === 'all') ? '' : selectedMenuItem.data.id;
    this.projects = [];

    this.getProjects();
  }

  onScroll() {
    this.payload.page++;
    this.getProjects();
  }

  search(event) {
    const searchFormData = this.searchForm.getRawValue();
    const searchText = searchFormData ? searchFormData.searchText : '';

    this.payload.searchName = searchText;
    this.payload.page = 1;

    this.projects = [];
    this.getProjects();
  }

  getScreenSize(event?) {
    this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    this.windowWidth = window.innerWidth;
  }

  getProjectImage(project, image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      project.image = reader.result;
      this.cdr.markForCheck();
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
