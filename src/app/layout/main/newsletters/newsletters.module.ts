import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NewslettersRoutingModule } from './newsletters.routes';
import { NewslettersComponent } from './newsletters.component';
import { DocumentService } from '@app/shared/services/document.service';
import { MessageService } from 'primeng/api';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule
} from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { HeaderModule } from '@app/shared/components/header/header.module';
import { SidebarMenuModule } from '@app/shared/components/sidebar-menu/sidebar-menu.module';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';
import { IwNewsletterPreviewModule } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.module';

const BASE_MODULES = [CommonModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NgbModule.forRoot()
];

const EXTRA_MODULES = [
  HeaderModule,
  SidebarMenuModule,
  PdfViewerModule,
  IwNewsletterPreviewModule
];

const ROUTE_MODULES = [NewslettersRoutingModule];

const COMPONENTS = [
  NewslettersComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  providers: [
    DocumentService,
    MessageService
  ],
  entryComponents: [
    IwNewsletterPreviewComponent
  ]
})

export class NewslettersModule { }
