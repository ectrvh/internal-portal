import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { MenuService, DocumentService, NewsletterService, DASHBOARD_MENU_ITEMS, FileUploadService } from '@app/shared/services';
import { Router } from '@angular/router';
import { LayoutModel } from '@app/layout/layout-model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';

@Component({
  selector: 'app-newsletters',
  templateUrl: './newsletters.component.html',
  styleUrls: ['./newsletters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class NewslettersComponent implements OnInit {

  newsletters: any;

  constructor(public documentService: DocumentService,
    public newsletterService: NewsletterService,
    public menuService: MenuService,
    private router: Router,
    public layoutModel: LayoutModel,
    private fileUploadService: FileUploadService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.getNewsletters();
  }

  selectMenuItem(selectedMenuItem) {
    if (selectedMenuItem.id === 'dashboard') {
      this.router.navigate(['dashboard']);
    } else if (selectedMenuItem.id === 'all') {
      this.getNewsletters();
    }
  }

  getNewsletters() {

    // this.newsletters = newsletters;

    this.newsletterService.getNewsletters()
      .subscribe(
        newsletters => {

          if (newsletters) {
            newsletters.map(newsletter => {

              newsletter.url = '';
              newsletter.loading = true;
              this.newsletters = newsletters;
              this.cdr.markForCheck();

              // Get Images for each Project
              if (newsletter.fileUploads && newsletter.fileUploads.length > 0) {
                const payload = { id: newsletter.id };
                this.fileUploadService.getFile(payload).subscribe(
                  data => {
                    this.createFileFromBlob(newsletter, data);
                  },
                  error => {
                  }
                );
              } else {
                newsletter.loading = false;
              }

            });
          }
        }
      )
  }


  createFileFromBlob(newsletter, file: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      newsletter.url = reader.result;
      this.cdr.markForCheck();
      newsletter.loading = false;
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  // function readBase64(file): Promise<any> {
  //   const reader = new FileReader();
  //   const future = new Promise((resolve, reject) => {
  //     reader.addEventListener('load', function () {
  //       resolve(reader.result);
  //     }, false);

  //     reader.addEventListener('error', function (event) {
  //       reject(event);
  //     }, false);

  //     reader.readAsDataURL(file);
  //   });
  //   return future;
  // }

  previewNewsletter(newsletter) {
    const activeModal = this.modalService.open(
      IwNewsletterPreviewComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.newsletter = newsletter;
    activeModal.componentInstance.fileData = newsletter.url;

    activeModal.result.then((res) => {
      // on close
    }, (reason) => {
      // on dismiss
    });
  }
}
