import {NewslettersModule } from './newsletters.module';

describe('NewslettersModule', () => {
  let newslettersModule: NewslettersModule;

  beforeEach(() => {
    newslettersModule = new NewslettersModule();
  });

  it('should create an instance', () => {
    expect(newslettersModule).toBeTruthy();
  });
});
