import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbSpinnerModule, NbCardModule, NbButtonModule } from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserProfileRoutingModule } from './user-profile.routes';
import { UserProfileComponent } from './user-profile.component';
import { TranslateModule } from '@ngx-translate/core';
import { UserProfileDescriptionComponent } from './user-profile-description/user-profile-description.component';
import { EditorModule } from 'primeng/editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedPipesModule } from '@app/shared';

const BASE_MODULES = [
  CommonModule,
  TranslateModule,
  FormsModule,
  ReactiveFormsModule,
  EditorModule,
  SharedPipesModule
];

const NB_MODULES = [
  NbSpinnerModule,
  NbCardModule,
  NbButtonModule,
  NgbModule.forRoot()
];

const ROUTE_MODULES = [
  UserProfileRoutingModule
];

const COMPONENTS = [
  UserProfileComponent,
  UserProfileDescriptionComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...NB_MODULES],
  entryComponents: [
    UserProfileDescriptionComponent,
  ],
  declarations: [...COMPONENTS],
  providers: [    
  ]
})

export class UserProfileModule { }
