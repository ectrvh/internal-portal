import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { LayoutModel } from '@app/layout/layout-model';
import { DASHBOARD_MENU_ITEMS, TeamService, FileUploadService, AccountService } from '@app/shared/services';
import { UserService } from '@app/shared/services';
import { User, Team, Project } from '@app/shared/models';
import { UserProfileDescriptionComponent } from './user-profile-description/user-profile-description.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { RESPONSE } from '@app/shared/constants/response';
import { MODE } from '@app/shared/constants/mode';
import { NotifierService } from 'angular-notifier';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class UserProfileComponent implements OnInit, OnDestroy {

  alive = true;
  user: any;
  teams: Team[] = [];
  projects: Project[] = [];
  loggedUser = null;
  loading = false;

  notifier: NotifierService;
  textSaveSuccess = '';

  // [TO DO] Take photo from database. Now is static.
  image = 'assets/images/user-nobody.jpg';

  constructor(
    public layoutModel: LayoutModel,
    private userService: UserService,
    public translate: TranslateService,
    private teamService: TeamService,
    private modalService: NgbModal,
    private fileUploadService: FileUploadService,
    private cdr: ChangeDetectorRef,
    private accountService: AccountService,
    private notifierService: NotifierService) {

    this.notifier = this.notifierService;

    this.translate.get('Information successfuly saved').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => this.textSaveSuccess = res);
    
    this.loggedUser = this.userService.getUserFromLocalStorage();
  }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.initPage();
  }

  initPage() {
    this.loading = true;
    const payload = {
      userId: this.loggedUser.id
    };
    const teams = this.teamService.getTeams();
    const user = this.userService.getUsers(payload);
    const projects = this.accountService.getUserProjects(payload);
    forkJoin([teams, user, projects])
      .subscribe(results => {
        this.teams = results[0];
        this.projects = results[2];
        this.updateUserTeams(results[1]);
        this.updateUserProjects(results[1]);
        this.updateUserImage(results[1]);
        this.loading = false;
      });

  }

  updateUserTeams(userTeams) {
    if (userTeams) {
      userTeams.image = 'assets/images/user-nobody.jpg';
      const teamsName = [];
      userTeams.teamsNameList = '';
      const departmentsName = [];
      userTeams.departmentsNameList = '';
      if (userTeams.teamsLink && userTeams.teamsLink.length > 0) {
        for (const i of userTeams.teamsLink) {
          const team = this.teams.find(result => result.id === i.teamId);
          if (team) {
            teamsName.push(team.name);
            userTeams.teamsNameList = teamsName.join(', ');
            departmentsName.push(team.department.name);
            userTeams.departmentsNameList = departmentsName.join(', ');
          }
        }
      }
      this.user = userTeams;
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  updateUserProjects(userProjects) {
    if (userProjects) {
      const projectsName = [];
      userProjects.projectsNameList = '';
      if (userProjects.projectsLink && userProjects.projectsLink.length > 0) {
        for (const i of userProjects.projectsLink) {
          const project = this.projects.find(result => result.id === i.projectId);
          if (project) {
            projectsName.push(project.title);
            userProjects.projectsNameList = projectsName.join(', ');
          }
        }
      }
      this.user = userProjects;
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  updateUserImage(user) {
    if (user) {
      if (user.fileUploads && user.fileUploads.length > 0) {
        const payload = { id: user.id };
        this.fileUploadService.getFile(payload).subscribe(
          data => {
            this.getUserImage(user, data);
          },
          error => {
          }
        );
      }
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  getUserImage(user, image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      user.image = reader.result;
      this.cdr.markForCheck();
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  showDialog(user: User) {
    const activeModal = this.modalService.open(
      UserProfileDescriptionComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );

    activeModal.componentInstance.mode = MODE.UPDATE;
    activeModal.componentInstance.user = user;

    activeModal.result.then((res) => {
      // on close
      if (res && res.response) {

        if (res.response === RESPONSE.ERROR) {
          this.notifier.notify('error', res.message);
        } else {
          this.notifier.notify('success', this.textSaveSuccess);
          this.initPage();
        }

      }
    }, (reason) => {
      // on dismiss
    });
    // console.log(userDescription, 'user description show dialog');
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
