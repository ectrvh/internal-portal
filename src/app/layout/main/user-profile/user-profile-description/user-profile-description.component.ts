import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MenuService, AccountService } from '@app/shared/services';
import { User } from '@app/shared/models';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RESPONSE } from '@app/shared/constants/response';

@Component({
  selector: 'app-user-profile-description',
  templateUrl: './user-profile-description.component.html',
  styleUrls: ['./user-profile-description.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class UserProfileDescriptionComponent implements OnInit {

  modalHeader: string;
  user: User;
  form: FormGroup;
  submitted = false;
  mode = '';

  constructor(
    private activeModal: NgbActiveModal,
    public menuService: MenuService,
    private accountService: AccountService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      id: [''],
      description: ['', [Validators.required, Validators.maxLength(4000)]]
    });

    this.form.patchValue({
      id: this.user ? this.user.id : '',
      description: this.user ? this.user.description : ''
    });

  }

  ngOnInit() { }

  closeModal(result?) {
    this.activeModal.close(result);
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  saveForm(details) {
    this.submitted = true;    
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }
    const formData = this.form.getRawValue();
    if (formData) {
      formData.id = this.user.id;
      formData.description = formData.description.trim();
      this.accountService.saveUserDescription(formData).subscribe(
        res => {
          this.closeModal({ response: RESPONSE.SUCCESS, message: res });                    
        },
        err => {
          this.closeModal({ response: RESPONSE.ERROR, message: err });          
        }
      );
    } else {
      this.closeModal();
    }
  }

}
