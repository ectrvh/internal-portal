import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule
} from '@nebular/theme';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderModule } from '@app/shared/components/header/header.module';
import { SidebarMenuModule } from '@app/shared/components/sidebar-menu/sidebar-menu.module';
import { IwCalendarModule } from '@app/shared/components/iw-calendar/iw-calendar.module';
import { CalendarComponent } from './calendar.component';
import { CalendarRoutingModule } from './calendar.routes';

const BASE_MODULES = [CommonModule, TranslateModule];

const NB_MODULES = [
  NbLayoutModule,
  NbSidebarModule,
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,  
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NgbModule.forRoot()
];

const EXTRA_MODULES = [
  HeaderModule,
  SidebarMenuModule,
  IwCalendarModule
];

const ROUTE_MODULES = [CalendarRoutingModule];

const COMPONENTS = [
  CalendarComponent
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  providers: [        
  ]
})

export class CalendarModule { }
