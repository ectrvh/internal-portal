import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DASHBOARD_MENU_ITEMS } from '@app/shared/services';
import { LayoutModel } from '@app/layout/layout-model';


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CalendarComponent implements OnInit, OnDestroy {

  alive = true;
  loading = false;
  
  constructor(public layoutModel: LayoutModel) {
  }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
