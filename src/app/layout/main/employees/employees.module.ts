import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { EmployeesRoutingModule } from './employees.routes';
import { EmployeesComponent } from './employees.component';
import { UserService } from '@app/shared/services/user.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SafeHtmlPipe } from '@app/theme/pipes/safehtml.pipe';
import { IwUserPreviewComponent } from '@app/shared/modules/iw-user-preview/iw-user-preview.component';
import { IwUserPreviewModule } from '@app/shared/modules/iw-user-preview/iw-user-preview.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,  
  NbInputModule,
  NbButtonModule
} from '@nebular/theme';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NbInputModule,
  NbButtonModule,
  NgbModule.forRoot()
];

const EXTRA_MODULES = [InfiniteScrollModule, IwUserPreviewModule];

const ROUTE_MODULES = [EmployeesRoutingModule];

const COMPONENTS = [EmployeesComponent];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    IwUserPreviewComponent
  ],
  providers: [    
    UserService
  ]
})

export class EmployeesModule { }
