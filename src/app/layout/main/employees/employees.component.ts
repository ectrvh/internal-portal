import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { MenuService, UserService, TeamService, DASHBOARD_MENU_ITEMS, FileUploadService, ProjectService } from '@app/shared/services';
import { User, Team, Project } from '@app/shared/models';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutModel } from '@app/layout/layout-model';
import { omit, find } from 'lodash';
import { isNullOrUndefined } from 'util';
import { toMenuItems } from '@shared/util';
import { IwUserPreviewComponent } from '@app/shared/modules/iw-user-preview/iw-user-preview.component';
import { forkJoin } from 'rxjs';
import { HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NbMenuService } from '@nebular/theme';
import { NbMenuBag } from '@nebular/theme/components/menu/menu.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class EmployeesComponent implements OnInit, OnDestroy {

  users: User[] = [];
  teams: Team[] = [];
  projects: Project[] = [];

  searchForm: FormGroup;
  selectedMenu: NbMenuBag;

  payload = {
    page: 1,
    limit: 12,
    searchName: '',
    teamId: ''
  };

  projectUsersNameList = '';
  currentPageName = '';
  alive = true;
  loading = false;
  allDepartments = 'allDepartments';

  headerHeight = 100;
  windowHeight: any;
  windowWidth: any;

  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    if (this.users && this.users.length < 4) {
      this.windowHeight = 'inherit';
    } else {
      this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    }
    this.windowWidth = window.innerWidth;
  }

  constructor(public layoutModel: LayoutModel,
    private teamService: TeamService,
    private userService: UserService,
    public menuService: MenuService,
    private modalService: NgbModal,
    private fileUploadService: FileUploadService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private projectService: ProjectService,
    private nbMenuService: NbMenuService,
    private cdr: ChangeDetectorRef) {

    this.searchForm = this.formBuilder.group({
      searchText: ['', Validators.required]
    });

    this.searchForm.patchValue({ searchText: '' });

    this.getScreenSize();

    this.translate.get('allDepartments').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      this.allDepartments = res;
      this.currentPageName = res;
    });

    this.nbMenuService.onItemClick()
      .pipe(
        takeWhile(() => this.alive)
      )
      .subscribe(res => {
        if (this.selectedMenu && this.selectedMenu.item && this.selectedMenu.item.data && this.selectedMenu.item.data === res.item.data) {
          return;
        }
        this.selectedMenu = res;
        if (this.selectedMenu && this.selectedMenu.item.data) {
          this.updateUsers(this.selectedMenu.item);
        }
      });
  }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.initPage();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  showDialog(event?, user?: User) {
    const activeModal = this.modalService.open(IwUserPreviewComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );
    activeModal.componentInstance.user = user;
  }

  initPage() {
    this.loading = true;
    const teams = this.teamService.getTeams();
    const users = this.userService.getUsers(this.payload);
    const projects = this.projectService.getAllProjects();
    forkJoin([teams, users, projects])
      .subscribe(results => {
        this.teams = results[0];
        this.projects = results[2];
        this.toSidebarMenuItems(this.teams);
        this.updateUserTeam_Project_Department(results[1]);
        this.updateUserImage(results[1]);
        this.loading = false;
      });
  }

  getUsers() {
    this.loading = true;
    this.userService.getUsers(this.payload)
      .subscribe(users => {
        this.updateUserTeam_Project_Department(users);
        this.updateUserImage(users);
      });
  }

  toSidebarMenuItems(teams) {
    const departments = [];
    if (teams) {
      // Create the Meniu Level 1 : Departments
      teams.forEach(resource => {
        if (resource.hasOwnProperty('department') && resource.department.enabled) {
          const checkDepartment = find(departments, result => result.id === resource.department.id);
          if (isNullOrUndefined(checkDepartment)) {
            resource.department.items = [];
            departments.push(resource.department);
          }
        }
      });

      // Inject Teams in Departments
      teams.forEach(resource => {
        const department = find(departments, result => result.id === resource.department.id);
        if (department) {
          resource = omit(resource, ['department', 'departmentId']);
          department.items.push(resource);
        }
      });

      const sidebarMenuItems = toMenuItems(departments, 'employees', true, true, this.allDepartments);
      this.layoutModel.updateMenuItems(sidebarMenuItems);
    }

  }

  updateUserTeam_Project_Department(users) {
    if (users) {
      users.map(user => {
        user.image = 'assets/images/user-nobody.jpg';
        const teamsName = [];
        user.teamsNameList = '';
        const departmentsName = [];
        user.departmentsNameList = '';
        const projectsName = [];
        user.projectsNameList = '';

        if (user.projectsLink && user.projectsLink.length > 0) {
          for (const i of user.projectsLink) {
            const project = this.projects.find(result => result.id === i.projectId);
            if (project) {
              projectsName.push(project.title);
              user.projectsNameList = projectsName.join(', ');
            }
          }
        }

        if (user.teamsLink && user.teamsLink.length > 0) {
          for (const i of user.teamsLink) {
            const team = this.teams.find(result => result.id === i.teamId);
            if (team) {
              teamsName.push(team.name);
              user.teamsNameList = teamsName.join(', ');
              departmentsName.push(team.department.name);
              user.departmentsNameList = departmentsName.join(', ');
            }
          }
        }
        // console.log(this.users);
        this.users.push(user);
      });
    }

    if (this.users && this.users.length < 4) {
      this.windowHeight = 'inherit';
    } else {
      this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    }

    this.loading = false;
    this.cdr.markForCheck();
  }

  updateUserImage(users) {
    if (users) {
      users.map(user => {
        if (user.fileUploads && user.fileUploads.length > 0) {
          const payload = { id: user.id };
          this.fileUploadService.getFile(payload).subscribe(
            data => {
              this.getUserImage(user, data);
            },
            error => {
            }
          );
        }
      });
    }
    if (this.users && this.users.length < 4) {
      this.windowHeight = 'inherit';
    }
    this.loading = false;
    this.cdr.markForCheck();
  }

  getUserImage(user, image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      user.image = reader.result;
      this.cdr.markForCheck();
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  onScroll() {
    this.payload.page++;
    this.getUsers();
  }

  getScreenSize(event?) {
    this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    this.windowWidth = window.innerWidth;
  }

  search(event) {
    const searchFormData = this.searchForm.getRawValue();
    const searchText = searchFormData ? searchFormData.searchText : '';
    this.payload.searchName = searchText;
    this.payload.page = 1;
    this.users = [];
    this.getUsers();
  }

  updateUsers(selectedMenuItem) {
    this.currentPageName = selectedMenuItem.title;

    this.payload.searchName = '';
    this.payload.page = 1;
    this.payload.teamId = (selectedMenuItem.data.id === 'all') ? '' : selectedMenuItem.data.id;

    this.users = [];

    this.getUsers();
  }

}
