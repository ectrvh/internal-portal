import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardWidgetDocumentsComponent } from './documents.component';

describe('DashboardWidgetDocumentsComponent', () => {
  let component: DashboardWidgetDocumentsComponent;
  let fixture: ComponentFixture<DashboardWidgetDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
