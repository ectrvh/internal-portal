import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { DocumentService } from '@app/shared/services/document.service';
import { Document } from '@app/shared/models/document.ts';
import { FileUploadService } from '@app/shared/services';

@Component({
    selector: 'app-dashboard-widget-documents',
    templateUrl: './documents.component.html',
    styleUrls: ['./documents.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class DashboardWidgetDocumentsComponent implements OnInit, OnDestroy {

    loading = false;
    alive = true;
    documentsDashboard: Document[] = [];

    constructor(
        private documentService: DocumentService,
        private cdr: ChangeDetectorRef,
        private fileUploadService: FileUploadService
    ) { }
    ngOnInit() {
        this.getDocumentsForDashboard();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    getDocumentsForDashboard() {
        this.loading = true;
        const payload = { limit: 9, page: 1 };
        this.documentService.getDocumentsForDashboard(payload)
            .subscribe(documentDashboard => {
                this.documentsDashboard = documentDashboard;
                this.loading = false;
                this.cdr.markForCheck();
            });
    }

    downloadFile(document:Document) {
        if (document && document.fileUploads && document.fileUploads.length > 0) {
            const payload = { id: document.id };
            this.fileUploadService.downloadFile(payload, document.fileUploads[0]);
        }
    }
}
