import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NewsletterService, FileUploadService } from '@app/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';
import { Newsletter } from '@app/shared/models';

// https://www.npmjs.com/package/ng2-pdf-viewer#external-link-target

@Component({
    selector: 'app-dashboard-widget-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class DashboardWidgetNewsletterComponent implements OnInit, OnDestroy {

    newsletter: Newsletter;
    fileData: any;

    loading = false;
    alive = true;

    constructor(public newsletterService: NewsletterService,
        private modalService: NgbModal,
        private fileUploadService: FileUploadService,
        private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.getNewsletters();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    getNewsletters() {
        this.loading = true;
        const payload = { limit: 10, page: 1 };

        this.newsletterService.getLastNewsletter(payload)
            .subscribe(
                newsletter => {
                    if (newsletter) {
                        this.newsletter = newsletter;                        

                        if (newsletter.fileUploads && newsletter.fileUploads.length > 0) {
                            const payloadNewsletterId = { id: newsletter.id };
                            this.fileUploadService.getFile(payloadNewsletterId).subscribe(
                                data => {
                                    this.createFileFromBlob(newsletter, data);
                                },
                                error => {
                                }
                            );
                        } else {
                            this.loading = false;
                            this.cdr.markForCheck();
                        }
                    } else {
                        this.loading = false;
                        this.cdr.markForCheck();
                    }
                }
            );
    }

    previewNewsletter(newsletter) {
        const activeModal = this.modalService.open(
            IwNewsletterPreviewComponent,
            { size: 'lg', centered: true, container: 'nb-layout' }
        );

        activeModal.componentInstance.newsletter = newsletter;
        activeModal.componentInstance.fileData = this.fileData;

        activeModal.result.then((res) => {
            // on close
        }, (reason) => {
            // on dismiss
        });
    }

    createFileFromBlob(newsletter, file: Blob) {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
            this.fileData = reader.result;
            this.cdr.markForCheck();
            this.loading = false;
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

}
