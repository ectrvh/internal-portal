import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardWidgetNewsletterComponent } from './newsletter.component';

describe('DashboardWidgetNotificationsComponent', () => {
  let component: DashboardWidgetNewsletterComponent;
  let fixture: ComponentFixture<DashboardWidgetNewsletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetNewsletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetNewsletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
