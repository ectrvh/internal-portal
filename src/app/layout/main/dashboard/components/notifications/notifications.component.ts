import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { NewsService } from '@app/shared/services';
import { News } from '@app/shared/models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IwNewsPreviewComponent } from '@app/shared/modules/iw-news-preview/iw-news-preview.component';

@Component({
    selector: 'app-dashboard-widget-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardWidgetNotificationsComponent implements OnInit, OnDestroy {

    loading = false;
    dasboardNews: News[] = [];
    alive = true;

    constructor(
        private modalService: NgbModal,
        private newsService: NewsService,
        private cdr: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.getNewsForDashboard();
    }

    ngOnDestroy() {
        this.alive = false;
    }

    getNewsForDashboard() {
        this.loading = true;
        const payload = { limit: 9, page: 1 };
        this.newsService.getNewsForDashboard(payload)
            .subscribe(newsForDashboard => {
                this.dasboardNews = newsForDashboard;
                this.loading = false;
                this.cdr.markForCheck();
                // console.log(newsForDashboard);
            });
    }

    showNotificationDetails(news) {
        // console.log(news);
        const activeModal = this.modalService.open(
            IwNewsPreviewComponent,
            { size: 'lg', centered: true, container: 'nb-layout' }
        );
        activeModal.componentInstance.news = news;
        activeModal.result.then((res) => {
            // on close
        }, (reason) => {
            // on dismiss
        });
    }
}
