import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardWidgetWeatherComponent } from './weather.component';

describe('DashboardWidgetWeatherComponent', () => {
  let component: DashboardWidgetWeatherComponent;
  let fixture: ComponentFixture<DashboardWidgetWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetWeatherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
