import { Component, OnInit, ChangeDetectorRef, OnDestroy, Input } from '@angular/core';
import { WeatherService } from '@app/shared/services/weather.service';
import * as moment from 'moment';
import { weatherLocations } from '@app/shared/constants/weather-locations';

@Component({
    selector: 'app-dashboard-widget-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.scss']
})

export class DashboardWidgetWeatherComponent implements OnInit, OnDestroy {

    locationName = '';
    weatherForecastData: any[];
    alive = true;
    currentDate: number;
    now = new Date();
    loading = false;


    locations = weatherLocations;
    @Input() selectedLocation = this.locations[0];

    constructor(private weatherService: WeatherService,
        private cdr: ChangeDetectorRef) { }

    ngOnInit() {
        this.currentDate = Date.now();
        this.selectedLocation.date = moment(this.now).format('MM/DD/YYYY');
        this.getWeather(this.selectedLocation);
    }

    ngOnDestroy() {
        this.alive = false;
    }

    getWeather(location) {

        this.loading = true;

        this.weatherService.getWeather(location)
            .subscribe(
                res => {
                    let weatherData = null;
                    let weatherHeadline = '';
                    let weatherDailyForecasts = [];

                    if (res.response && res.message && res.message === 'UPDATE') {
                        let payload = res.response;
                        payload.data = JSON.stringify(payload.data);
                        this.weatherService.save(payload).subscribe(
                            res => {

                            }
                        );

                        this.locationName = res.response.locationName;
                        weatherData = JSON.parse(res.response.data);
                        weatherHeadline = weatherData.Headline;
                        weatherDailyForecasts = weatherData.DailyForecasts;
                    } else {
                        this.locationName = res.response.locationName;
                        weatherData = JSON.parse(res.response.data);
                        weatherHeadline = weatherData.Headline;
                        weatherDailyForecasts = weatherData.DailyForecasts;
                    }

                    // console.log(headline, 'headline');
                    // console.log(dailyForecasts, 'dailyForecasts');

                    this.weatherForecastData = weatherDailyForecasts
                        .map(result => {
                            const forecastWeather = {
                                data: result.Date,
                                tempMin: Number(result.Temperature.Minimum.Value),
                                tempMax: Number(result.Temperature.Maximum.Value),
                                iconImg: 'assets/weather/' + result.Day.Icon + '.svg',
                                iconNb: result.Day.Icon,
                                wind: result.Day.Wind.Speed.Value,
                                rainProbability: result.Day.RainProbability,
                                snowProbability: result.Day.SnowProbability,
                                hintText: result.Day.LongPhrase,
                            };
                            // console.log(forecastWeather, 'forecastWeather');
                            return forecastWeather;
                        });

                    // console.log(this.weatherForecastData, 'weatherForecastData');
                    this.loading = false;
                    this.cdr.markForCheck();
                }
            );
    }

    changeLocation(location) {        
        if (this.selectedLocation.locationKey !== location.locationKey) {
            this.selectedLocation = location;
            this.selectedLocation.date = moment(this.now).format('MM/DD/YYYY');            
            this.getWeather(location);
        }
    }

}
