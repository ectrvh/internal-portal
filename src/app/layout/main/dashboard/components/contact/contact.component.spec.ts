import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardWidgetContactComponent } from './contact.component';

describe('DashboardWidgetContactComponent', () => {
  let component: DashboardWidgetContactComponent;
  let fixture: ComponentFixture<DashboardWidgetContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
