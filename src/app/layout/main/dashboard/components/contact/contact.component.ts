import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Contact, Team, Project, User } from '@app/shared/models';
import { ContactService, UserService, TeamService, ProjectService, FileUploadService } from '@app/shared/services';
import { sortBy } from 'lodash';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IwUserPreviewComponent } from '@app/shared/modules/iw-user-preview/iw-user-preview.component';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-dashboard-widget-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DashboardWidgetContactComponent implements OnInit, OnDestroy {

  contacts: Contact[];
  alive = true;
  loading = false;

  teams: Team[];
  projects: Project[];

  allTeams: Team[] = [];
  allProjects: Project[] = [];
  allUsers: User[] = [];
  allContacts: Contact[] = [];

  constructor(
    private modalService: NgbModal,
    private contactService: ContactService,
    private teamService: TeamService,
    private projectService: ProjectService,
    private fileUploadService: FileUploadService,
    private userService: UserService,
    private cdr: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.getContacts();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  getContacts() {
    this.loading = true;
    const payloadLimit = { limit: 5, page: 1 };
    const fork_allTeams = this.teamService.getAllTeams();
    const fork_allProjects = this.projectService.getProjects();
    const fork_allContacts = this.contactService.getContactsForDashboard(payloadLimit);
    forkJoin([fork_allTeams, fork_allProjects, fork_allContacts])
      .subscribe(results => {
        this.allTeams = results[0];
        this.allProjects = results[1];
        this.allContacts = results[2];
        this.contacts = results[2].map(res => {
          if (res.user != null) {
            res.user.image = 'assets/images/user-nobody.jpg';
            const teamsName = [];
            res.user.teamsNameList = '';
            const departmentsName = [];
            res.user.departmentsNameList = '';
            const projectsName = [];
            res.user.projectsNameList = '';
            if (res.user.teamsLink && res.user.teamsLink.length > 0) {
              for (const i of res.user.teamsLink) {
                const team = this.allTeams.find(result => result.id === i.teamId);
                if (team) {
                  // res.user.team = team;
                  teamsName.push(team.name);
                  res.user.teamsNameList = teamsName.join(', ');
                  departmentsName.push(team.department.name);
                  res.user.departmentsNameList = departmentsName.join(', ');
                }
              }
            }
            if (res.user.projectsLink && res.user.projectsLink.length > 0) {
              for (const i of res.user.projectsLink) {
                const project = this.allProjects.find(result => result.id === i.projectId);
                if (project) {
                  // res.user.project = project;
                  projectsName.push(project.title);
                  res.user.projectsNameList = projectsName.join(', ');
                }
              }
            }
            if (res.user.fileUploads && res.user.fileUploads.length > 0) {
              const payload = { id: res.user.id };
              this.fileUploadService.getFile(payload).subscribe(
                data => {
                  this.getUserImage(res.user, data);
                },
                error => {
                }
              );
            }
          }
          return res;
        });
        this.contacts = sortBy(this.contacts, ['position']); // Sort ASC by position
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  getUserImage(user, image: Blob) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      user.image = reader.result;
      this.cdr.markForCheck();
    }, false);
    if (image) {
      reader.readAsDataURL(image);
    }
  }

  showContactDetails(contact) {
    if (contact.user) {
      // console.log(contact);
      const activeModal = this.modalService.open(
        IwUserPreviewComponent,
        { size: 'lg', centered: true, container: 'nb-layout' }
      );
      activeModal.componentInstance.contact = contact;
      activeModal.componentInstance.user = contact.user;
      activeModal.result.then((res) => {
        // on close
      }, (reason) => {
        // on dismiss
      });
    }
  }

}
