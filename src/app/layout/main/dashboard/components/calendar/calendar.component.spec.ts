import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DashboardWidgetCalendarComponent } from './calendar.component';

describe('DashboardWidgetCalendarComponent', () => {
  let component: DashboardWidgetCalendarComponent;
  let fixture: ComponentFixture<DashboardWidgetCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardWidgetCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardWidgetCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
