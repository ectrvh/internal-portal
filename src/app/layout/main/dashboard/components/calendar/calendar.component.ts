import { Component, OnInit, ViewChild, ChangeDetectionStrategy, Output, ChangeDetectorRef } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { MenuService, DocumentService, NewsletterService } from '@app/shared/services';
import { Options } from 'fullcalendar';
import { CalendarService } from '@app/shared/services/calendar.service';
import { IwCalendarComponent } from '@app/shared/components/iw-calendar/components/iw-calendar.component';

@Component({
    selector: 'app-dashboard-widget-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class DashboardWidgetCalendarComponent implements OnInit {

    calendarOptions: Options;
    displayEvent: any;

    // @ViewChild(IwCalendarComponent)
    // iwCalendarComponent: IwCalendarComponent;
    // events: any = [];

    constructor(public documentService: DocumentService,
        public newsletterService: NewsletterService,
        public menuService: MenuService,
        public calendarService: CalendarService) {
    }

    ngOnInit() {
    }

}
