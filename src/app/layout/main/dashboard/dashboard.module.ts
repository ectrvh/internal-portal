import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbAlertModule, NgbModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardRoutingModule } from './dashboard.routes';
import { DashboardComponent } from './containers/dashboard.component';
import {
    DashboardWidgetDocumentsComponent,
    DashboardWidgetNotificationsComponent,
    DashboardWidgetContactComponent,
    DashboardWidgetNewsletterComponent,
    DashboardWidgetCalendarComponent,
    DashboardWidgetWeatherComponent
} from './components';
import { TranslateModule } from '@ngx-translate/core';
import { IwCalendarModule } from '@app/shared/components/iw-calendar/iw-calendar.module';
import {
    NbUserModule,
    NbContextMenuModule,
    NbSearchModule,
    NbRouteTabsetModule,
    NbCardModule,
    NbPopoverModule,
    NbSpinnerModule,
    NbActionsModule
} from '@nebular/theme';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { WeatherService } from '@app/shared/services/weather.service';
import { APP_CONFIG, AppConfig } from '@app/app.config';
import { IwNewsletterPreviewModule } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.module';
import { IwNewsletterPreviewComponent } from '@app/shared/modules/iw-newsletter-preview/iw-newsletter-preview.component';
import { IwNewsPreviewComponent } from '@app/shared/modules/iw-news-preview/iw-news-preview.component';
import { IwNewsPreviewModule } from '@app/shared/modules/iw-news-preview/iw-news-preview.module';
import { IwUserPreviewComponent } from '@app/shared/modules/iw-user-preview/iw-user-preview.component';
import { IwUserPreviewModule } from '@app/shared/modules/iw-user-preview/iw-user-preview.module';


const BASE_MODULES = [CommonModule, TranslateModule];

const NB_MODULES = [
    NbActionsModule,
    NbUserModule,
    NbContextMenuModule,
    NbSearchModule,
    NbCardModule,
    NbRouteTabsetModule,
    NbPopoverModule,
    NbSpinnerModule,
    NgbDropdownModule,
    NgbModule.forRoot()
];

const EXTRA_MODULES = [
    IwCalendarModule,
    PdfViewerModule,
    IwNewsletterPreviewModule,
    IwNewsPreviewModule,
    IwUserPreviewModule,
    NgbAlertModule.forRoot()
];

const ROUTE_MODULES = [DashboardRoutingModule];

const COMPONENTS = [
    DashboardComponent,
    DashboardWidgetNotificationsComponent,
    DashboardWidgetDocumentsComponent,
    DashboardWidgetContactComponent,
    DashboardWidgetNewsletterComponent,
    DashboardWidgetCalendarComponent,
    DashboardWidgetWeatherComponent
];

@NgModule({
    imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
    declarations: [...COMPONENTS],
    providers: [WeatherService, { provide: APP_CONFIG, useValue: AppConfig }],
    entryComponents: [
        IwNewsletterPreviewComponent,
        IwNewsPreviewComponent,
        IwUserPreviewComponent
    ]
})

export class DashboardModule { }
