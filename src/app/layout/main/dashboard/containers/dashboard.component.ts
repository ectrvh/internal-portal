import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { LayoutModel } from '@app/layout/layout-model';
import { DASHBOARD_MENU_ITEMS } from '@app/shared/services';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class DashboardComponent implements OnInit, OnDestroy {

    alive = true;

    constructor(public layoutModel: LayoutModel) {
    }

    ngOnInit() {
        this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    }


    ngOnDestroy() {
        this.alive = false;
    }

}
