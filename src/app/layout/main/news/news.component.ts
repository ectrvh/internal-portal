import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { News } from '@app/shared/models';
import { NewsService, DASHBOARD_MENU_ITEMS, NewsCategoriesService } from '@app/shared/services';
import { takeWhile } from 'rxjs/operators';
import { toMenuItems } from '@shared/util';
import { NbMenuService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LayoutModel } from '@app/layout/layout-model';
import { NbMenuBag } from '@nebular/theme/components/menu/menu.service';
import { IwNewsPreviewComponent } from '@app/shared/modules/iw-news-preview/iw-news-preview.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { find } from 'lodash';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class NewsComponent implements OnInit, OnDestroy {

  categories = [];
  subcategories = [];
  news = [];

  searchForm: FormGroup;
  selectedMenu: NbMenuBag;

  payload = {
    page: 1,
    limit: 20,
    searchName: '',
    categoryId: '',
    subcategoryId: ''
  }

  currentPageName = '';
  alive = true;
  loading = false;
  allNews = 'allNews';

  headerHeight = 100;
  windowHeight: any;
  windowWidth: any;

  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    if (this.subcategories && this.subcategories.length < 4) {
      this.windowHeight = 'inherit';
    } else {
      this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    }
    this.windowWidth = window.innerWidth;
  }


  constructor(public layoutModel: LayoutModel,
    private formBuilder: FormBuilder,
    private newsService: NewsService,
    public translate: TranslateService,
    private newsCategoriesService: NewsCategoriesService,
    private nbMenuService: NbMenuService,
    private modalService: NgbModal,
    private cdr: ChangeDetectorRef) {

    this.searchForm = this.formBuilder.group({
      searchText: ['', Validators.required]
    });

    this.searchForm.patchValue({ searchText: '' });

    this.getScreenSize();

    this.translate.get('allNews').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => { 
      this.allNews = res;
      this.currentPageName = this.allNews;
    });


    this.nbMenuService.onItemClick()
      .pipe(
        takeWhile(() => this.alive)
      )
      .subscribe(res => {
        if (this.selectedMenu && this.selectedMenu.item && this.selectedMenu.item.data && this.selectedMenu.item.data === res.item.data) {
          return;
        }
        this.selectedMenu = res;
        if (this.selectedMenu && this.selectedMenu.item.data) {
          this.updateNews(this.selectedMenu.item);
        }
      });

  }


  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.initPage();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  initPage() {
    this.loading = true;
    const categories = this.newsCategoriesService.getNewsCategories();
    const news = this.newsService.getNews(this.payload);

    forkJoin([categories, news])
      .subscribe(result => {
        this.categories = result[0];
        this.news = result[1];
        this.toSidebarMenuItems(this.categories);
        this.toSubCategoriesNews(this.news);
        this.cdr.markForCheck();
        this.loading = false;
      });

  }


  showNewsDetails(news: News, event: Event) {
    const activeModal = this.modalService.open(
      IwNewsPreviewComponent,
      { size: 'lg', centered: true, container: 'nb-layout' }
    );
    // console.log(news, 'NEWS');
    activeModal.componentInstance.news = news;
  }

  toSidebarMenuItems(categories) {
    if (categories) {
      const sidebarMenuItems = toMenuItems(categories, 'news', true, true, this.allNews);
      this.layoutModel.updateMenuItems(sidebarMenuItems);
    }

  }


  toSubCategoriesNews(news) {
    const subCategories = [];
    if (news) {

      // Step 1. Create SubCategories
      news.forEach(resource => {
        if (resource.hasOwnProperty('newsSubcategories')) {
          const checkSubCategory = find(subCategories, result => result.id === resource.newsSubcategories.id);
          if (isNullOrUndefined(checkSubCategory)) {
            resource.newsSubcategories.news = [];
            subCategories.push(resource.newsSubcategories);
          }
        }
      });

      // Step 2. Inject News in each SubCategory
      news.forEach(resource => {
        const subCategory = find(subCategories, result => result.id === resource.newsSubcategories.id);
        if (subCategory) {
          // resource = omit(resource, ['department', 'departmentId']);
          subCategory.news.push(resource);
        }
      });

      this.subcategories = subCategories;
      if (this.subcategories && this.subcategories.length < 4) {
        this.windowHeight = 'inherit';
      }
      this.loading = false;
      this.cdr.markForCheck();
    }
  }

  getNews(payload) {
    this.loading = true;

    this.newsService.getNews(payload).subscribe(news => {
      this.toSubCategoriesNews(news);
      this.loading = false;
    })
  }

  updateNews(selectedMenuItem) {
    this.currentPageName = selectedMenuItem.title;

    this.payload.searchName = '';
    this.payload.page = 1;
    this.payload.categoryId = (selectedMenuItem.data.id === 'all') ? '' : selectedMenuItem.data.id;

    this.subcategories = [];

    this.getNews(this.payload);
  }


  search(event) {
    const searchFormData = this.searchForm.getRawValue();
    const searchText = searchFormData ? searchFormData.searchText : '';

    this.payload.searchName = searchText;
    this.payload.page = 1;

    this.subcategories = [];
    this.getNews(this.payload);
  }


  getScreenSize(event?) {
    this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    this.windowWidth = window.innerWidth;
  }
}
