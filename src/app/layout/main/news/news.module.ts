import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NewsRoutingModule } from './news.routes';
import { NewsComponent } from './news.component';
import { NewsService } from '@app/shared/services/news.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IwNewsPreviewComponent } from '@app/shared/modules/iw-news-preview/iw-news-preview.component';
import { IwNewsPreviewModule } from '@app/shared/modules/iw-news-preview/iw-news-preview.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NbButtonModule,
  NbInputModule
} from '@nebular/theme';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NbButtonModule,
  NbInputModule,
  NgbModule.forRoot()
];

const EXTRA_MODULES = [InfiniteScrollModule, IwNewsPreviewModule];

const ROUTE_MODULES = [NewsRoutingModule];

const COMPONENTS = [NewsComponent];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  entryComponents: [
    IwNewsPreviewComponent
  ],
  providers: [
    NewsService
  ]
})

export class NewsModule { }
