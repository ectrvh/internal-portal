import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { DocumentsRoutingModule } from './documents.routes';
import { DocumentsComponent } from './documents.component';
import { DocumentService } from '@app/shared/services/document.service';
import { MessageService } from 'primeng/api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NbButtonModule,
  NbInputModule
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const NB_MODULES = [
  NbActionsModule,
  NbUserModule,
  NbContextMenuModule,
  NbSearchModule,
  NgbModule,
  NbCardModule,
  NbRouteTabsetModule,
  NbSpinnerModule,
  NbButtonModule,
  NbInputModule
];

const EXTRA_MODULES = [InfiniteScrollModule];

const ROUTE_MODULES = [DocumentsRoutingModule];

const COMPONENTS = [DocumentsComponent];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...EXTRA_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  providers: [
    DocumentService,
    MessageService
  ]
})

export class DocumentsModule { }
