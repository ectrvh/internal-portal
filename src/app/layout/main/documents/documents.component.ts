import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, OnDestroy, HostListener } from '@angular/core';
import { DocumentService, DASHBOARD_MENU_ITEMS, DocumentCategoriesService, FileUploadService, DocumentSubCategoriesService } from '@app/shared/services';
import { toMenuItems } from '@shared/util';
import { NbMenuService } from '@nebular/theme';
import { takeWhile, map } from 'rxjs/operators';
import { LayoutModel } from '@app/layout/layout-model';
import { NbMenuBag } from '@nebular/theme/components/menu/menu.service';
import { Document } from '@app/shared/models/document.ts';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { map as _map, find } from 'lodash';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DocumentsComponent implements OnInit, OnDestroy {

  categories = [];
  subcategories = [];
  documents = [];

  searchForm: FormGroup;
  selectedMenu: NbMenuBag;

  payload = {
    page: 1,
    limit: 20,
    searchName: '',
    categoryId: '',
    subcategoryId: ''
  }

  currentPageName = '';
  alive = true;
  loading = false;
  allDocuments = 'allDocuments';

  headerHeight = 100;
  windowHeight: any;
  windowWidth: any;

  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    if (this.subcategories && this.subcategories.length < 4) {
      this.windowHeight = 'inherit';
    } else {
      this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    }
    this.windowWidth = window.innerWidth;
  }


  constructor(public layoutModel: LayoutModel,
    private documentService: DocumentService,
    private documentCategoriesService: DocumentCategoriesService,
    private documentSubCategoriesService: DocumentSubCategoriesService,
    private nbMenuService: NbMenuService,
    private fileUploadService: FileUploadService,
    private formBuilder: FormBuilder,
    public translate: TranslateService,
    private cdr: ChangeDetectorRef) {

    this.searchForm = this.formBuilder.group({
      searchText: ['', Validators.required]
    });

    this.searchForm.patchValue({ searchText: '' });

    this.getScreenSize();

    this.translate.get('allDocuments').pipe(
      takeWhile(() => this.alive)
    ).subscribe(res => {
      this.allDocuments = res;
      this.currentPageName = res;      
    });


    this.nbMenuService.onItemClick()
      .pipe(
        takeWhile(() => this.alive)
      )
      .subscribe(res => {
        if (this.selectedMenu && this.selectedMenu.item && this.selectedMenu.item.data && this.selectedMenu.item.data === res.item.data) {
          return;
        }
        this.selectedMenu = res;
        if (this.selectedMenu && this.selectedMenu.item.data) {
          this.updateDocuments(this.selectedMenu.item);
        }
      });

  }

  ngOnInit() {
    this.layoutModel.updateMenuItems(DASHBOARD_MENU_ITEMS);
    this.initPage();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  initPage() {
    this.loading = true;
    const categories = this.documentCategoriesService.getDocumentsCategories();
    const documents = this.documentService.getDocuments(this.payload);

    // const subcategories = this.documentSubCategoriesService.getDocumentSubCategories()
    //   .pipe(
    //     map((result: any) => {
    //       const documentSubCategories = _map(result, documentSubCategory => {
    //         documentSubCategory.loading = true;
    //         documentSubCategory.documents = [];
    //         return documentSubCategory;
    //       });
    //       return documentSubCategories;
    //     })
    //   );

    forkJoin([categories, documents])
      .subscribe(result => {
        this.categories = result[0];
        this.documents = result[1];
        this.toSidebarMenuItems(this.categories);
        this.toSubCategoriesDocuments(this.documents);
        // this.getSubCategoriesDocuments(result[1]);
        this.cdr.markForCheck();
        this.loading = false;
      });

  }

  toSubCategoriesDocuments(documents) {
    const subCategories = [];
    if (documents) {

      // Step 1. Create SubCategories
      documents.forEach(resource => {
        if (resource.hasOwnProperty('documentSubcategory')) {
          const checkSubCategory = find(subCategories, result => result.id === resource.documentSubcategory.id);
          if (isNullOrUndefined(checkSubCategory)) {
            resource.documentSubcategory.documents = [];
            subCategories.push(resource.documentSubcategory);
          }
        }
      });

      // Step 2. Inject Documents in each SubCategory
      documents.forEach(resource => {
        const subCategory = find(subCategories, result => result.id === resource.documentSubcategory.id);
        if (subCategory) {
          // resource = omit(resource, ['department', 'departmentId']);
          subCategory.documents.push(resource);
        }
      });

      this.subcategories = subCategories;
      if (this.subcategories && this.subcategories.length < 4) {
        this.windowHeight = 'inherit';
      }
      this.loading = false;
      this.cdr.markForCheck();
    }
  }

  getDocuments(payload) {
    this.loading = true;

    this.documentService.getDocuments(payload).subscribe(documents => {
      this.toSubCategoriesDocuments(documents);
      this.loading = false;
    });
  }


  toSidebarMenuItems(categories) {
    if (categories) {
      const sidebarMenuItems = toMenuItems(categories, 'documents', true, true, this.allDocuments);
      this.layoutModel.updateMenuItems(sidebarMenuItems);
    }
  }


  // TODO: NOT USED. Use this in future
  getSubCategoriesDocuments(subcategories) {
    if (subcategories) {
      subcategories.forEach(subcategory => {
        this.documentSubCategoriesService.getCategoryDocuments({ subcategoryId: subcategory.id }).subscribe(document => {
          subcategory.documents.push(document);
          this.cdr.markForCheck();
        });
        subcategory.loading = false;
      });
      this.subcategories = subcategories;
    }
    this.loading = false;
  }


  updateDocuments(selectedMenuItem) {
    this.currentPageName = selectedMenuItem.title;

    this.payload.searchName = '';
    this.payload.page = 1;
    this.payload.categoryId = (selectedMenuItem.data.id === 'all') ? '' : selectedMenuItem.data.id;

    this.subcategories = [];
    this.getDocuments(this.payload);
  }


  downloadFile(document: Document) {
    if (document && document.fileUploads && document.fileUploads.length > 0) {
      const payload = { id: document.id };
      this.fileUploadService.downloadFile(payload, document.fileUploads[0]);
    }
  }


  search(event) {
    const searchFormData = this.searchForm.getRawValue();
    const searchText = searchFormData ? searchFormData.searchText : '';

    this.payload.searchName = searchText;
    this.payload.page = 1;

    this.subcategories = [];
    this.getDocuments(this.payload);
  }


  getScreenSize(event?) {
    this.windowHeight = (window.innerHeight - this.headerHeight) + 'px';
    this.windowWidth = window.innerWidth;
  }

}
