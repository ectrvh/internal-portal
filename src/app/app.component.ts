import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AuthenticationService } from '@app/shared/services';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

    title = 'IW Internal Portal';    

    constructor(private auth: AuthenticationService,
                private router: Router) {
    }

    ngOnInit() {
    }

    logout() {
        this.auth.logout();
        this.router.navigate(['login']);
    }
}
