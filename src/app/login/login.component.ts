import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService, UserService } from '@app/shared/services';
import { first, takeWhile } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class LoginComponent implements OnInit, OnDestroy {

    loginForm: FormGroup;
    loading = false;
    returnUrl: string;

    notifier: NotifierService;

    alive = true;

    textUserUndefined = '';

    constructor(private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        public router: Router,
        private userService: UserService,
        private authService: AuthenticationService,        
        private notifierService: NotifierService,
        public translate: TranslateService,
        private cdr: ChangeDetectorRef) {

        this.notifier = this.notifierService;
    }

    ngOnInit() {

        this.loginForm = this.formBuilder.group({
            name: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.authService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    public ngOnDestroy() {
        this.alive = false;
    }

    login() {
        if (this.loginForm.invalid) {
            return;
        }

        const username: string = this.loginForm.get('name').value.trim();
        const password: string = this.loginForm.get('password').value.trim();

        this.loading = true;

        this.authService.login(username, password)
            .subscribe(
                loggedUser => {
                    if (loggedUser) {

                        const payload = { userId: loggedUser.id };

                        this.userService.getUsers(payload).subscribe(user => {
                            if (user && user.id === loggedUser.id) {
                                loggedUser.fullName = user.fullName;
                                loggedUser.email = user.email;
                                loggedUser.havePhoto = (user.fileUploads && user.fileUploads.length > 0) ? true : false;

                                this.userService.saveUserInLocalStorage(loggedUser);

                                this.router.navigate([this.returnUrl]);
                            }

                        });

                    }

                },
                error => {
                    // console.log(error, 'ERROR');
                    if (error.status === 504) {
                        if (error.hasOwnProperty('statusText')) {
                            this.notifier.notify('error', error.statusText);                            
                        }
                    } else if (error.status === 400) {
                        if (error.hasOwnProperty('_body')) {
                            const error_body = JSON.parse(error._body);                            
                            this.notifier.notify('warning', error_body.message);
                        }
                    } else {
                        this.notifier.notify('error', error.status);                         
                    }

                    this.cdr.markForCheck();
                    this.loading = false;
                });

    }

}
