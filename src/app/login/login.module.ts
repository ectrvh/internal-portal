import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { NbCardModule, NbLayoutModule, NbActionsModule, NbPopoverModule, NbSpinnerModule, NbButtonModule, NbThemeModule, NbInputModule, NbAlertModule, NbToastrModule } from '@nebular/theme';
import { DEFAULT_THEME } from '@app/theme/styles/theme.default';
import { ToastService } from '@app/shared/services';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule, TranslateModule];

const ROUTE_MODULES = [RouterModule, LoginRoutingModule];

const NB_MODULES = [
  NbCardModule,
  NbLayoutModule,
  NbActionsModule,
  NbInputModule,
  NbSpinnerModule,
  NbButtonModule
];

const COMPONENTS = [
  LoginComponent
];

const PROVIDERS = [
];

const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'default',
    },
    [DEFAULT_THEME],
  ).providers
];

@NgModule({
  imports: [...BASE_MODULES, ...ROUTE_MODULES, ...NB_MODULES],
  declarations: [...COMPONENTS],
  providers: [...PROVIDERS, ...NB_THEME_PROVIDERS]
})

export class LoginModule { }
